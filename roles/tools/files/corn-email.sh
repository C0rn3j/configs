#!/usr/bin/env bash
set -euo pipefail

Red='\033[0;31m'
Green='\033[0;32m'
Yellow='\033[0;33m'
NoColor='\033[0m'

Help() {
	echo -e "${Yellow}Sends email output about a systemd unit"
	echo -e "Usage example: ${0} --email me@site.com --type ansible --service-name updater-daily.service"
	echo -e "Required flags:"
	echo -e "  --email        - Email output to this address"
	echo -e "  --type         - Type of email, available methods are: generic, tail, ansible"
	echo -e "  --service-name - Name of systemd unit to get logs from"
	echo -e ""
	echo -e "Optional flags:"
	echo -e "  --output-type  - Name of journalctl --output type, default is 'cat', unlike the upstream default 'short'"
	echo -e "  --help | -h    - Writes out this help screen${NoColor}"
}

email=''
type=''
serviceName=''
journalctlOutputType="cat"
while [[ -n ${1-} ]]; do
	case ${1} in
		--email )
			shift
			email=${1};;
		--type )
			shift
			type=${1};;
		--service-name )
			shift
			serviceName=${1};;
		--output-type )
			shift
			journalctlOutputType=${1};;
		-h | --help )
			Help
			exit 0;;
		* )
			echo -e "${Red}Invalid parameter ${1}${NoColor}"
			Help
			exit 1;;
	esac
	shift
done

if [[ ${email} == '' ]] || [[ ${type} == '' ]] || [[ ${serviceName} == '' ]]; then
	Help
	exit 1
fi
serviceLog=$(journalctl --output "${journalctlOutputType}" _SYSTEMD_INVOCATION_ID="$(systemctl show -p InvocationID --value "${serviceName}")")
serviceLogProcessed=''
appendBody=''
if [[ ${type} == 'generic' ]]; then
	serviceLogProcessed=${serviceLog}
	appendBody="Sending full service log from last run."
elif [[ ${type} == 'tail' ]]; then
	serviceLogProcessed=$(echo "${serviceLog}" | tail -n 30)
	appendBody="Sending last 30 lines of output from last run."
elif [[ ${type} == 'ansible' ]]; then
	if serviceLogProcessed=$(echo "${serviceLog}" | grep -Pzo "fatal:.*(?:\n\s+.*)*" | tr '\0' '\n'); then
		failedServers=$(echo "${serviceLogProcessed}" | grep -c "fatal:")
		appendBody="${failedServers} servers failed to run the playbook in the last run."
	else
		serviceLogProcessed=${serviceLog}
		appendBody="All servers ran the playbook successfully!"
	fi
else
	echo -e "${Red}Invalid type: ${type}${NoColor}"
	Help
	exit 1
fi

if ! sendmailBin=$(command -v sendmail); then
	echo -e "${Red}sendmail binary could not be found, exiting!${NoColor}"
	exit 1
fi

# Sendmail probably autocorrects it, but we should not exceed 1000 chars per line
# In addition we shouldn't exceed 78 including CLRF to prevent ancient mailservers breaking down
serviceLogBase64=$(echo "${serviceLogProcessed}" | base64 -w76)
BOUNDARY="20231130ABCDEF"
echo -e "${Yellow}Sending email...${NoColor}"
${sendmailBin} -t <<ERRMAIL
From: systemd <root@${HOSTNAME}>
To: ${email}
Subject: ${serviceName}
Content-Type: multipart/mixed; boundary="${BOUNDARY}"

--${BOUNDARY}
Content-Transfer-Encoding: 8bit
Content-Type: text/plain; charset=UTF-8

${appendBody}

Email sent by corn-email

--${BOUNDARY}
Content-Type: application;
Content-Transfer-Encoding: base64
Content-Disposition: attachment; filename="log.txt"

${serviceLogBase64}

--${BOUNDARY}--
ERRMAIL
echo -e "${Yellow}Sent!...${NoColor}"

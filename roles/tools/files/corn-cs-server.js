#!/usr/bin/node

// On Windows just install node with Chocolatey:
//   choco install -y nodejs.install

function convertToReadableTime(seconds) {
	const MINUTE = 60
	const HOUR = MINUTE * 60
	const DAY = HOUR * 24
	const MONTH = DAY * 30 // approximate
	const YEAR = DAY * 365 // approximate

	const years = Math.floor(seconds / YEAR)
	seconds -= years * YEAR
	const months = Math.floor(seconds / MONTH)
	seconds -= months * MONTH
	const days = Math.floor(seconds / DAY)
	seconds -= days * DAY
	const hours = Math.floor(seconds / HOUR)
	seconds -= hours * HOUR
	const minutes = Math.floor(seconds / MINUTE)
	seconds -= minutes * MINUTE

	let result = []

//	if (years === 1) result.push('1 year')
//	else if (years > 1) result.push(`${years} years`)
//	if (months === 1) result.push('1 month')
//	else if (months > 1) result.push(`${months} months`)
//	if (days === 1) result.push('1 day')
//	else if (days > 1) result.push(`${days} days`)
//	if (hours === 1) result.push('1 hour')
//	else if (hours > 1) result.push(`${hours} hours`)
//	if (minutes === 1) result.push('1 minute')
//	else if (minutes > 1) result.push(`${minutes} minutes`)
//	if (seconds === 1) result.push('1 second')
//	else if (seconds > 1) result.push(`${seconds} seconds`)

if (years > 0) result.push(`${years}y`)
if (months > 0) result.push(`${months}m`)
if (days > 0) result.push(`${days}d`)
if (hours > 0) result.push(`${hours}h`)
if (minutes > 0) result.push(`${minutes}m`)
if (seconds > 0) result.push(`${seconds}s`)

return result.join('')
//	return result.join(', ')
}


function getWeaponName(name, paintKit) {
	if (name === 'weapon_fists') return 'Fists'
	if (name === 'weapon_hammer') return 'Hammer time'
	if (name === 'weapon_axe') return 'Here\'s Johnny!'
	if (name === 'weapon_tablet') return 'Galaxy Note 7'
	if (name === 'weapon_healthshot') return 'Healing'
	if (name === 'weapon_bumpmine') return 'Jumpy Jump'
	if (name === 'weapon_breachcharge') return 'Breach charge'
	if (name === 'weapon_shield') return 'Anti-protester'
	if (name === 'weapon_c4') return 'BOOOOOM'
	if (name === 'weapon_decoy') return 'Fakie Noise Makie'
	if (name === 'weapon_flashbang') return 'Blinder'
	if (name === 'weapon_incgrenade') return 'IDF Fire Makie'
	if (name === 'weapon_molotov') return 'Ruskie fryer'
	if (name === 'weapon_smokegrenade') return 'Fog machine'
	if (name === 'weapon_bayonet') return 'Fix bayonets!'
	if (name === 'weapon_knife_m9_bayonet') return 'Fix M9 bayonets!'
	if (name === 'weapon_knife_butterfly') return 'Butterfry'
	if (name === 'weapon_knife_canis') return 'Sovereign\'s knife' // Survival knife
	if (name === 'weapon_knife_cord') return 'Paracord knife'
	if (name === 'weapon_knife_css') return 'Classic knife'
	if (name === 'weapon_knife_falchion') return 'Falchion'
	if (name === 'weapon_knife_flip') return 'Flippy'
	if (name === 'weapon_knife_ghost') return 'Ghost Knife' // Hidden knife, spectral flip
	if (name === 'weapon_knife_gut') return 'Gut Knife'
	if (name === 'weapon_knife_gypsy_jackknife') return 'Gypsy Jackknife'
	if (name === 'weapon_knife_karambit') return 'Karambit'
	if (name === 'weapon_knife_kukri') return 'Cuckri'
	if (name === 'weapon_knife_outdoor') return 'Nomad knife'
	if (name === 'weapon_knife_stiletto') return 'Stiletto'
	if (name === 'weapon_knife_skeleton') return 'Skelly'
	if (name === 'weapon_knife_survival_bowie') return 'David\'s knife'
	if (name === 'weapon_knife_tactical') return 'Huntsman'
	if (name === 'weapon_knife_ursus') return 'U R SUS'
	if (name === 'weapon_knife_push') return 'Buttplugs'
	if (name === 'weapon_knife_widowmaker') return 'Talon'
	if (name === 'weapon_knife') return 'Knife (no skin)'
	if (name === 'weapon_knife_t') return 'Knife (no skin)'
	if (name === 'weapon_taser') {
		if (paintKit === 'default') return 'SUS (no skin)'
		return 'SUS'
	}
	if (name === 'weapon_hegrenade') return 'Boomstick'
	if (name === 'weapon_usp_silencer') return 'USP'
	if (name === 'weapon_hkp2000') return 'P2K'
	if (name === 'weapon_cz75a') return 'CZ'
	if (name === 'weapon_elite') return 'Dual Berettas'
	if (name === 'weapon_p250') return 'P250'
	if (name === 'weapon_fiveseven') return 'Five-Seven'
	if (name === 'weapon_revolver') return 'I R8'
	if (name === 'weapon_deagle') return 'Desert Beagle'
	if (name === 'weapon_glock') return 'Glock'
	if (name === 'weapon_tec9') return 'Tec-9'
	if (name === 'weapon_mp7') return 'MP7'
	if (name === 'weapon_mp9') return 'MP9'
	if (name === 'weapon_mac10') return 'Mac and cheese'
	if (name === 'weapon_mp5sd') return 'MP5'
	if (name === 'weapon_ump45') return 'UMP'
	if (name === 'weapon_p90') return 'P90'
	if (name === 'weapon_bizon') return 'Smol PP'
	if (name === 'weapon_nova') return 'Nova, chooom'
	if (name === 'weapon_xm1014') return 'XM'
	if (name === 'weapon_sawedoff') return 'Sawed-Off'
	if (name === 'weapon_mag7') return 'Swag-7'
	if (name === 'weapon_m249') return 'Shitty Negev'
	if (name === 'weapon_negev') return 'Negev'
	if (name === 'weapon_galilar') return 'Galil'
	if (name === 'weapon_ak47') return 'AK47'
	if (name === 'weapon_famas') return 'Famas'
	if (name === 'weapon_m4a1') return 'M4A1'
	if (name === 'weapon_m4a1_silencer') return 'M4A1-S'
	if (name === 'weapon_sg556') return 'SG'
	if (name === 'weapon_aug') return 'AUG'
	if (name === 'weapon_ssg08') return 'SSG'
	if (name === 'weapon_awp') return 'AWP'
	if (name === 'weapon_g3sg1') return 'ღPuppyღ'
	if (name === 'weapon_scar20') return '🐱Kitty🐱'
	console.log('Unknown weapon!')
	return name
}

async function getHeartRateData() {
	try {
		const response = await fetch(urlHeartRate);
		if (!response.ok) {
			throw new Error(`HTTP error! Status: ${response.status}`);
		}
		const textData = await response.text(); // Get the response as text
//		console.log("Received raw text data:", textData);

		// Parse the JSON string into an object
		const data = JSON.parse(textData);
//		console.log("data:", data);

		// Extract and format the heart rate
		const heartRateValue = `${data["Heart Rate"].toFixed(0)}|`; // Remove decimal places and add the "|"
	//	console.log("Formatted Heart Rate:", heartRateValue);
		heartRate = heartRateValue
		return data;
	} catch (error) {
		console.error("Error fetching heart rate data:", error);
	}
}


// https://steamcommunity.com/sharedfiles/filedetails/?id=1265752398
// ღ - does not show on Linux in Flatpak for some reason - https://github.com/flathub/com.valvesoftware.Steam/issues/1119

// ღ • ⁂ € ™ ↑ → ↓ ⇝ √ ∞ ░ ▲ ▶ ◀ ● ☀ ☁ ☂ ☃ ☄ ★ ☆ ☉ ☐ ☑ ☎
// ☚ ☛ ☜ ☝ ☞ ☟ ☠ ☢ ☣ ☪ ☮ ☯ ☸ ☹ ☺ ☻ ☼ ☾☽ ♔ ♕ ♖ ♗ ♘ ♚ ♛ ♜ ♝ ♞ ♟
// ♡ ♨ ♩ ♪ ♫ ♬ ✈ ✉ ✍ ✎ ✓ ✔ ✘ ✚ ✝ ✞ ✟ ✠ ✡ ✦ ✧ ✩ ✪ ✮ ✯ ✹ ✿ ❀ ❁ ❂ ❄ ❅ ❆
// ❝ ❞ ❣ ❤ ❥ ❦ ❧ ➤ ツ ㋡ ♥ 웃 Ⓐ Ⓑ Ⓒ Ⓓ Ⓔ Ⓕ Ⓖ Ⓗ Ⓘ Ⓙ Ⓚ Ⓛ Ⓜ Ⓝ Ⓞ Ⓟ Ⓠ Ⓡ Ⓢ Ⓣ Ⓤ Ⓥ Ⓦ Ⓧ Ⓨ Ⓩ
// ⚠ ⛏ 😈 🐵 ⛵ ⚕ 😶‍ 😷 ✋ 😇 ♂ ♀ ✳ ✏ ❔ ⚙ ♏ ⛪ 😌 🐱 ⚓ ⚖
// TODO there's also a car symbol!

http = require('http')
fs = require('fs')
const os = require('os')
const usedOS = os.type()

const port = 3000
const host = '127.0.0.1'

let heartRate = ''
const portHeartRate = '8080'
const hostHeartRate = '192.168.1.55'
const endpointHeartRate = '/'
const urlHeartRate = `http://${hostHeartRate}:${portHeartRate}${endpointHeartRate}`
setInterval(getHeartRateData, 1000)


let bombPlantedAt = ''
let usedMem = ''
let kernelVer = ''
let uptime = ''
let steamDirectory = ''
let whisperString = ''

// We should really just parse Steam's library config file for this, but eh, it'll do
// Only support Windows and Linux as macOS doesn't even have a CS2 build
if (usedOS === 'Windows_NT') {
	steamDirectory = 'C:\\Program Files (x86)\\Steam'
} else {
	steamDirectory = '/home/c0rn3j/.var/app/com.valvesoftware.Steam/.local/share/Steam'
}

fs.readFile('/proc/version', 'utf8', (err, data) => {
	if (err) {
		console.log('Error reading /proc/version')
	} else {
		let match = data.split(' ')
		kernelVer = match ? match[2] : null
	}
})

server = http.createServer( function(req, res) {
	if (req.method == 'POST') {
		fs.readFile('/proc/uptime', 'utf8', (err, data) => {
			if (err) {
				console.log('Error reading /proc/uptime')
			} else {
				let match = data.match(/(\d+\.\d+)/)
				if (match) {
					let uptimeSeconds = parseInt(parseFloat(match[1]))
					uptime = convertToReadableTime(uptimeSeconds)
				} else {
					console.log('Error parsing /proc/uptime')
				}
			}
		})

		console.log("Handling POST request...")
		res.writeHead(200, {'Content-Type': 'text/html'})

		let body = ''
		req.on('data', function (data) {
			body += data
		})
		req.on('end', function () {
			console.log("POST payload: " + body)

			let data = JSON.parse(body)
			let player = data['player']
			let provider = data['provider']
			let outputHOME = ''
			let outputEND = ''
			let outputPGUP = ''
			let outputPGDN = ''
			let HP = 0
			let armor = 0
			let helmet = false
			let roundKills = 0
			let roundKillsHS = 0
			let timeRemaining = 0
			let sayString = ''
			if (player !== undefined)
				console.log("Player Name: " + player['name'])
			if (player !== undefined && player['state'] !== undefined && player['state']['health'] !== undefined) {
				console.log("Health: " + player['state']['health'])
				console.log("Armor: " + player['state']['armor'])
				HP = player['state']['health']
				armor = player['state']['armor']
				helmet = player['state']['helmet']
			}

			let bombString = ''
			if (data['round'] !== undefined) {
				console.log(data['round'])
				if (data['round']['phase'] === 'freezetime') bombPlantedAt = ''
				if (data['round']['bomb'] === 'planted' && (bombPlantedAt === undefined || bombPlantedAt === null || bombPlantedAt === '')) {
					bombPlantedAt = new Date()
					console.log('<SET> Bomb planted at: '+ bombPlantedAt)
				}
				if (bombPlantedAt !== undefined && bombPlantedAt !== null && bombPlantedAt !== '') {
					if (data['round']['bomb'] === 'defused') {
						bombString = ' ☢ DEFUSED'
					} else {
						let now = new Date()
						let timeElapsedInMs = now - bombPlantedAt
						let timeElapsed = (timeElapsedInMs / 1000).toFixed(2)
						// Bomb time is 40 but the game adds a randomized delay, so we do 39 for a semi-accurate countdown
						timeRemaining = (39 - timeElapsed).toFixed(2)
						console.log(`Time til BOOM: ${timeRemaining}`)
						bombString = ` ☢${timeRemaining}`
					}
				}
			}
			let weapons = {}
			if (player !== undefined)
				weapons = player['weapons']
			let activeWeapon
			let reloadString = ''
			for (let weapon in weapons) {
					if (weapons[weapon]['state'] == 'active' || weapons[weapon]['state'] == 'reloading') {
						if (weapons[weapon]['state'] == 'reloading') reloadString = 'Ⓡ'
						activeWeapon = weapons[weapon]
						break
					}
			}
			if (activeWeapon !== undefined) {
				//console.log("Active Weapon: " + getWeaponName(activeWeapon['name']))
				let ammoString = ''
				if (activeWeapon['ammo_clip'] !== undefined) {
					ammoString = '|' + activeWeapon['ammo_clip'] + '/' + activeWeapon['ammo_reserve'] + ''
				}
				let armorString = ' '
					let armorSymbol = '☆'
					if (helmet) armorSymbol = '★'
					if (armor > 0) {
						armorString = ' ' + armorSymbol + armor
					}
				let playerNameString = player['name'] + '| '
				if (provider['steamid'] === player['steamid']) {
					playerNameString = ''
				}
				roundKills = player['state']['round_kills']
				let killsString = ''
				if (roundKills > 0) {
					killsString = ` ☠${roundKills}`
//					if (roundKills === 1) killsString = '<☠>'
//					if (roundKills === 2) killsString = '<☠☠>'
//					if (roundKills === 3) killsString = '<☠☠☠>'
//					if (roundKills === 4) killsString = '<☠☠☠☠>'
//					if (roundKills === 5) killsString = '<☠☠☠☠☠>'
//					if (roundKills > 5) killsString = '<☠'+roundKills + '>'
				}
				let weaponString = reloadString + getWeaponName(activeWeapon['name'], activeWeapon['paintkit']) + ammoString
				sayString = `say "${heartRate}${playerNameString}♥${HP}${armorString} ${weaponString}${killsString}${bombString}";`
			} else {
				sayString = `say "${heartRate}${bombString}"`
				console.log("No active weapon.")
			}
			fs.readFile('/tmp/whisper.txt', 'utf8', (err, data) => {
				if (err) {
					console.log('Error reading /tmp/whisper.txt')
				} else {
					whisperString = data.replace(/\r?\n/g, " ")
					whisperString = `say "${whisperString}"`
				}
			})
			ramString = `say "${usedMem}";`
			outputHOME += `bind home "exec csgo_home"; ${sayString}`
			outputEND  += `bind end "exec csgo_end"; ${whisperString}`
//			outputPGUP += `bind pgup "exec csgo_pgup"; ${sayString}`
			outputPGDN += `bind pgdn "exec csgo_pgdn"; ${ramString}`
			console.log("Spamline: " + sayString)
			//			if (player['state'] !== undefined) {
//				console.log(player['state'])
//			}
			fs.readFile('/proc/meminfo', 'utf8', (err, data) => {
				if (err) {
					console.log('Error reading /proc/meminfo')
				} else {
					const memTotalMatch = data.match(/MemTotal:\s+(\d+) kB/)
//					const memAvailMatch = data.match(/MemAvailable:\s+(\d+) kB/)
					const memFreeMatch = data.match(/MemFree:\s+(\d+) kB/)
					const buffersMatch = data.match(/Buffers:\s+(\d+) kB/)
					const cachedMatch = data.match(/Cached:\s+(\d+) kB/)
					if (memTotalMatch) {
						const memTotalKB = parseInt(memTotalMatch[1])
						const memAvailKB = parseInt(memTotalMatch[1]-memFreeMatch[1]-buffersMatch[1]-cachedMatch[1])
						const memTotalMB = (memTotalKB / 1024 / 1024).toFixed(0)
						const memAvailMB = (memAvailKB / 1024 / 1024).toFixed(2)
//						console.log(`${memAvailMB}/${memTotalMB} GB`)
// Player AiMW4R3Z left the game (Disconnected)

// We can also force overflowing of the text by inserting multiple " " unicode spaces at the end
						usedMem = `!status Aimware kernel: ${kernelVer} Memory usage: ${memAvailMB}/${memTotalMB} GB Uptime: ${uptime} Last console command: map de_nuke`
//						console.log(output)
					} else {
						console.log('MemTotal not found in /proc/meminfo')
					}
				}
			})
			console.log(outputHOME)
			console.log(outputEND)
//			console.log(outputPGUP)
			console.log(outputPGDN)
			console.log(`♥${heartRate}`)
			fs.writeFile(`${steamDirectory}/steamapps/common/Counter-Strike Global Offensive/game/csgo/cfg/csgo_home.cfg`, outputHOME, function(err) {
				if (err) {
					return console.log(err)
				}
			})
			fs.writeFile(`${steamDirectory}/steamapps/common/Counter-Strike Global Offensive/game/csgo/cfg/csgo_end.cfg`, outputEND, function(err) {
				if (err) {
					return console.log(err)
				}
			})
//			fs.writeFile(`${steamDirectory}/steamapps/common/Counter-Strike Global Offensive/game/csgo/cfg/csgo_pgup.cfg`, outputPGUP, function(err) {
//				if (err) {
//					return console.log(err)
//				}
//			})
			fs.writeFile(`${steamDirectory}/steamapps/common/Counter-Strike Global Offensive/game/csgo/cfg/csgo_pgdn.cfg`, outputPGDN, function(err) {
				if (err) {
					return console.log(err)
				}
			})

			res.end( '' )
		})
	} else {
		console.log("Not expecting other request types...")
		res.writeHead(200, {'Content-Type': 'text/html'})
		let html = '<html><body>HTTP Server at http://' + host + ':' + port + '</body></html>'
		res.end(html)
	}
})

server.listen(port, host)
console.log('Listening at http://' + host + ':' + port)

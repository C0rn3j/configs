#!/usr/bin/env python

import sys
from pathlib import Path

from lynxlynx import printCommandOutput, shellExec
from termcolor import colored

versionCheck, errors = shellExec(args=['pkgctl', 'version', 'check'])
printCommandOutput(commandOutput=versionCheck)
if versionCheck.returnCode == 1:
	print(colored(f'Failed to check version via pkgctl, RC: {versionCheck.returnCode}, maybe run "pkgctl version setup"?', 'red'))
	sys.exit(1)
elif versionCheck.returnCode == 2:
	# TODO(Martin): Automate this if it turns out to be reliable
	print(colored(f'Failed to check version via pkgctl, RC: {versionCheck.returnCode}, maybe run "pkgctl version upgrade"?', 'red'))
	sys.exit(1)
elif versionCheck.returnCode != 0:
	print(colored(f'Failed to check version via pkgctl, RC: {versionCheck.returnCode}, unknown Return Code.', 'red'))
	sys.exit(1)

# Use updpkgsums from pacman-contrib instead of redirecting stdout from `makepkg -g`
# One should probably implement an --inplace flag in makepkg so this script has no more reason to exist, but we can't fight every battle
# This updates checksums inplace so we don't have to do it manually, and saves us from having to generate initial .SRCINFO by `makepkg --printsrcinfo > .SRCINFO`
updPkgsums, errors = shellExec(args=['updpkgsums'])
printCommandOutput(commandOutput=updPkgsums)

# Try building the package in a clean chroot with paru instead of running plain old boring `makepkg -sri``
# This avoids building with makepkg with system libraries which would make us miss dependencies, and produce nonsensical warnings in namcap in some cases
paruUpgrade, errors = shellExec(args=['paru', '-U', '--chroot', '--noconfirm'])
printCommandOutput(commandOutput=paruUpgrade)
if paruUpgrade.returnCode != 0:
	print(colored('Error trying to run paru makepkgbuild in a clean chroot!', 'red'))
	sys.exit(1)

# Rebuild SRCINFO in case PKGBUILD changed while building, for example in vcs(-git,...) packages
makepkg, errors = shellExec(args=['makepkg', '--printsrcinfo'])
printCommandOutput(commandOutput=makepkg)
if makepkg.returnCode != 0:
	print(colored('Error trying to run makepkg to generate .SRCINFO!', 'red'))
	sys.exit(1)
else:
	with open(file='.SRCINFO', mode='w', newline='', encoding='utf-8') as srcFile:
		print(makepkg.stdOut, file=srcFile)

# See if there are any easily detectable mistakes with the PKGBUILD and the final package
# Sort and uniq it as otherwise we risk getting dupe idetical errors for PKGBUILD and package
# Run it through `script`` so it keeps colors despite being piped, should probably be reported and a --color flag should be added, but something something battle
files = Path().glob('*.pkg.tar.zst')
filesStr = list(map(str, files))
namcap, errors = shellExec(args=['namcap', 'PKGBUILD', *filesStr])
#print(files) # ['file1.bc', 'file2.bc']
printCommandOutput(commandOutput=namcap)
if namcap.returnCode != 0:
	print(colored('Error trying to run namcap against PKGBUILD and packages!', 'red'))
	sys.exit(1)
else:
	pass
	# TODO(Martin): | sort | uniq

# https://wiki.archlinux.org/title/Reproducible_builds
# https://wiki.archlinux.org/title/Arch_package_guidelines#Reproducible_builds
# https://tests.reproducible-builds.org/archlinux/archlinux.html
print('Remember to run "makerepropkg -d *.pkg.tar.zst" to verify reproducibility')

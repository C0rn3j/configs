#!/bin/bash
set -euo pipefail

# Very WIP script

secondTimeout=3
# loop through the list of proxies and test them with curl
while read line; do
	ip=$(echo "${line}" | awk '{print $1}')
	port=$(echo "${line}" | awk '{print $2}')
	protocol=$(echo "${line}" | awk '{print $3}')

	# test the proxy with curl and get the response time
	echo "Testing $protocol proxy at $ip:$port"
	curl_output=$(curl -x $protocol://$ip:$port -s -w "%{json}" -o /dev/null --max-time ${secondTimeout} https://www.google.com)

	# parse the required JSON data
	status=$(echo $curl_output | jq -r '.errormsg')
	time=$(echo $curl_output | jq -r '.time_total')

	# check if the proxy connection works
	if [[ ${status} == "Could not resolve host"* ]]; then
		echo "$ip:$port $protocol Proxy connection failed: ${status}"
	elif [[ ${status} == "Failed to connect to"* ]]; then
		echo "$ip:$port $protocol Proxy connection failed: ${status}"
	elif [[ ${status} == "Connection timed out"* ]]; then
		echo "$ip:$port $protocol Proxy connection failed: ${status}"
	else
		# print the latency
		echo "$ip:$port $protocol Latency: $time seconds"
	fi
done < "proxy-list.txt"

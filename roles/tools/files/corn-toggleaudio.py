#!/usr/bin/env python3
import getpass
import subprocess
import sys
import time
import json
from ruamel.yaml import YAML
from colorama import init
from dataclasses import dataclass

class Colors:
	"Class for coloring output"
	ENDC = '\033[0m'
	BOLD = '\033[1m'
	UNDERLINE = '\033[4m'
	GREY = '\033[90m'
	RED = '\033[91m'
	GREEN = '\033[92m'
	YELLOW = '\033[93m'
	PURPLE = '\033[35m'

@dataclass
class commandOutput:
	"Class for shell command output"
	returnCode: int
	stdOut: str
	stdErr: str

def shellExec(args: list) -> commandOutput:
	process = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	if debug:
		print(Colors.YELLOW, end = "")
		for i in range(len(args)):
			print(args[i], end = " ")
		print(Colors.ENDC)
	stdout, stderr = process.communicate()
	if stderr != b'':
		print(Colors.RED + str(stderr.rstrip(), "utf-8") + Colors.ENDC)
	return commandOutput(process.returncode, stdout, stderr)

def getModuleList() -> dict:
	commandOutput = shellExec(['pactl', '--format', 'json', 'list', 'modules'])
	return json.loads(commandOutput.stdOut.decode("utf-8"))

def getPulseInfo() -> dict:
	commandOutput = shellExec(['pactl', '--format', 'json', 'info'])
	return json.loads(commandOutput.stdOut.decode("utf-8"))

def getCardList() -> dict:
	commandOutput = shellExec(['pactl', '--format', 'json', 'list', 'cards'])
	return json.loads(commandOutput.stdOut.decode("utf-8"))

def setLastID(*, id: int) -> None:
	with open('/tmp/corn-toggleaudio_ID', 'w') as f:
		f.write(str(id))

def getLastID() -> int:
	try:
		with open('/tmp/corn-toggleaudio_ID', 'r') as f:
			return int(f.read())
	except FileNotFoundError:
		return 0

def getSinkList() -> dict:
	commandOutput = shellExec(['pactl', '--format', 'json', 'list', 'sinks'])
	return json.loads(commandOutput.stdOut.decode("utf-8"))

def loadConfig() -> dict:
	"""
 To get the required values:
 * Run 'pactl list cards short' to get the string
 * Set your device to the correct output and run 'pactl list cards | grep Active' to get output and input
 * If device has device.product.name defined, that name will be used as a fallback if output cannot be found
   for example if audio card connects and disconnects due to powersaving and ends up having a different ID
 Example config file:
debug: True
devices:
  PC_Speakers:
    output_device.product.name: Family 17h/19h HD Audio Controller
    output: pci-0000_0a_00.4
    outputMode: analog-stereo
    index: 1
  TV:
    output: pci-0000_08_00.1
    outputMode: hdmi-stereo
    index: 2
  Valve_Index:
    virtualSink: True
    output: pci-0000_08_00.1
    outputMode: hdmi-stereo-extra2
    input: usb-Valve_Corporation_Valve_VR_Radio___HMD_Mic_4A8C29B0F1-LYM-01
    inputMode: mono-fallback
    index: 3
  Headset:
    virtualSink: True
    output_device.product.name: HyperX 7.1 Audio
    output: usb-Kingston_HyperX_Virtual_Surround_Sound_00000000-00
    input_device.product.name: HyperX 7.1 Audio
    input: usb-Kingston_HyperX_Virtual_Surround_Sound_00000000-00
    outputMode: analog-stereo
    inputMode: iec958-stereo
    index: 4
	"""
	username=getpass.getuser()
	configFile="/home/"+username+"/audioDevices.yaml"
	with open(configFile, 'r') as streamConfig:
		yaml = YAML(typ='safe')
		devices = yaml.load(streamConfig)

	# Disable debug if unset
	if 'debug' not in devices:
		devices['debug'] = False

	return devices

# https://wiki.archlinux.org/index.php/PulseAudio/Examples#Mixing_additional_audio_into_the_microphone's_audio
def virtualSink(*, inputCard: str, inputIO: str, outputCard: str, outputIO: str, deviceName: str) -> None:
	microphone = "alsa_input."+inputCard+"."+inputIO
	speakers = "alsa_output."+outputCard+"."+outputIO # +".monitor" # .monitor only on pipewire-pulse
	micName = deviceName+"_mic_ec"
	spkName = deviceName+"_spk_ec"
	backendName = getPulseInfo()['server_name']
	print("Setting up virtual sinks...")
	# Setting up echo cancellation
	# If check because of https://gitlab.freedesktop.org/pipewire/pipewire/-/issues/3071
	if backendName == 'pulseaudio':
		shellExec(['pactl', 'load-module', 'module-echo-cancel', 'use_master_format=1', 'aec_method=webrtc',
			'aec_args="analog_gain_control=0\\ digital_gain_control=1\\ experimental_agc=1\\ extended_filter=1"', 'source_master="'+microphone+'"',
			'source_name='+micName, 'source_properties=device.description='+micName+' sink_master="'+speakers+'"', 'sink_name='+spkName, 'sink_properties=device.description='+spkName])
	else: # PulseAudio (on PipeWire 0.3.66)
		shellExec(['pactl', 'load-module', 'module-echo-cancel', 'use_master_format=1', 'library_name=aec/libspa-aec-webrtc',
			'aec_args="analog_gain_control=0 digital_gain_control=1 experimental_agc=1 extended_filter=1"', 'source_master="'+microphone+'"',
			'source_name='+micName, 'source_properties=device.description='+micName+' sink_master="'+speakers+'"', 'sink_name='+spkName, 'sink_properties=device.description='+spkName])
	# Creating virtual output devices
	shellExec(['pactl', 'load-module', 'module-null-sink', 'sink_name=vsink_fx', 'sink_properties=device.description=vsink_fx'])
	shellExec(['pactl', 'load-module', 'module-null-sink', 'sink_name=vsink_fx_mic', 'sink_properties=device.description=vsink_fx_mic'])
	# Creating loopbacks
	shellExec(['pactl', 'load-module', 'module-loopback', 'latency_msec=30', 'adjust_time=3', 'source='+micName, 'sink=vsink_fx_mic'])
	shellExec(['pactl', 'load-module', 'module-loopback', 'latency_msec=30', 'adjust_time=3', 'source=vsink_fx.monitor', 'sink=vsink_fx_mic'])
	shellExec(['pactl', 'load-module', 'module-loopback', 'latency_msec=30', 'adjust_time=3', 'source=vsink_fx.monitor', 'sink='+spkName])
	# Setting default devices
	shellExec(['pactl', 'set-default-source', micName])
	shellExec(['pactl', 'set-default-sink', spkName])

def unloadSinks() -> None:
	""" In case sinks were loaded for virtual audio, unload them, else PipeWire/PulseAudio screws up """
	moduleList = getModuleList()
	if any(item['name'] == 'module-null-sink' for item in moduleList):
		shellExec(['pactl', 'unload-module', 'module-null-sink'])
	if any(item['name'] == 'module-loopback' for item in moduleList):
		shellExec(['pactl', 'unload-module', 'module-loopback'])
	if any(item['name'] == 'module-echo-cancel' for item in moduleList):
		shellExec(['pactl', 'unload-module', 'module-echo-cancel'])

def setCardMode(*, cardString: str, ioString: str, cardList: dict) -> None:
	""" Sets the card to the input/+output mode """
	cardPulseID=""
	cardFound=False
	for card in cardList:
		if card['name'] == cardString:
			cardPulseID=str(card['index'])
			cardFound=True
	if cardFound:
		shellExec(['pactl', 'set-card-profile', cardPulseID, ioString])
	else:
		print(Colors.RED + "Could not find card", cardString, "with IO string", ioString + Colors.ENDC)


def setActive(*, deviceList: dict, index: int) -> None:
	if debug:
		print("Setting active index for: " + str(index))
	unloadSinks()
	# Turn off all cards
	for card in cardList:
		shellExec(['pactl', 'set-card-profile', str(card['index']), 'off'])
	# Turn on all devices matching the index
	for device in deviceList:
		if deviceList[device]['index'] == index:
			deviceName=device
			# Determine if we're setting input and output, or just input or just output
			if 'input' in deviceList[device] and 'output' in deviceList[device]:
				# If the input and output is on the same device, we need to set one mode for one card, as opposed to two modes for two different card
				if deviceList[device]['input'] == deviceList[device]['output']:
					ioString="output:"+deviceList[device]['outputMode']+"+input:"+deviceList[device]['inputMode']
					setCardMode(cardString="alsa_card."+deviceList[device]['output'], ioString=ioString, cardList=cardList)
				else:
					ioString="input:"+deviceList[device]['inputMode']
					setCardMode(cardString="alsa_card."+deviceList[device]['input'],  ioString=ioString, cardList=cardList)
					ioString="output:"+deviceList[device]['outputMode']
					setCardMode(cardString="alsa_card."+deviceList[device]['output'], ioString=ioString, cardList=cardList)
			elif 'input' in deviceList[device]:
				ioString="input:"+deviceList[device]['inputMode']
				setCardMode(cardString="alsa_card."+deviceList[device]['input'],    ioString=ioString, cardList=cardList)
			elif 'output' in deviceList[device]:
				ioString="output:"+deviceList[device]['outputMode']
				setCardMode(cardString="alsa_card."+deviceList[device]['output'],   ioString=ioString, cardList=cardList)
			else:
				print("Input nor output defined for device "+deviceList[device])
				exit(1)
			if 'virtualSink' in deviceList[device]:
				if deviceList[device]['virtualSink']:
					virtualSink(inputCard=deviceList[device]['input'], inputIO=deviceList[device]['inputMode'], outputCard=deviceList[device]['output'], outputIO=deviceList[device]['outputMode'], deviceName=deviceName)
			print("Switched to ID " + str(index) + " - " + deviceName)
			setLastID(id=index)

def getNextDeviceToSwitchTo(*, deviceList: dict, lastIndex: int) -> int:
	""" Gets the index of the next device to switch to, or rolls back to the first one in the list if there's no next"""
	lowestIndex = sys.maxsize
	highestIndex = 0

	for device in deviceList:
		if deviceList[device]['index'] > highestIndex:
			highestIndex = deviceList[device]['index']
		if deviceList[device]['index'] < lowestIndex:
			lowestIndex = deviceList[device]['index']

	nextIndex = lastIndex +1
	while True:
		if nextIndex not in [deviceList[device]['index'] for device in deviceList]:
			if nextIndex > highestIndex:
				return lowestIndex
			nextIndex = nextIndex + 1
		else:
			return nextIndex

def unmuteSink(*, sinkID: str) -> None:
	shellExec(['pactl', 'set-sink-mute', sinkID, '0'])

def pruneDevice(*, IOType: str, device: str) -> None:
	""" IOType is either input or output"""
	if IOType in deviceList[device]:
		fullCardName="alsa_card."+deviceList[device][IOType]
		if not any(item['name'] == fullCardName for item in cardList):
			fallbackFound=False
			# Allow fallback if device.product.name is defined
			for item in cardList:
				if IOType in deviceList[device] and IOType+'_device.product.name' in deviceList[device] and item['properties']['device.product.name'] == deviceList[device][IOType+'_device.product.name']:
					replacedName = item['name'].replace("alsa_card.", "")
					print(Colors.PURPLE + "Replaced", IOType, "of", device, "index", deviceList[device]['index'], "name", item['properties']['device.product.name'], "from", deviceList[device][IOType], "to", replacedName)
					deviceList[device][IOType] = replacedName
					fallbackFound=True
					break
			if not fallbackFound:
				print(Colors.RED + device+' (' + fullCardName + ")'s", IOType, "was removed from the list because it was not found to be connected! See 'pactl list cards short' for list of available devices." + Colors.ENDC)
				del deviceList[device][IOType]


def pruneDeviceList() -> None:
	for device in list(deviceList):
		pruneDevice(IOType='output', device=device)
		pruneDevice(IOType='input', device=device)
		if 'input' not in deviceList[device] and 'output' not in deviceList[device]:
			if debug:
				print(Colors.RED + device + " was removed from the list completely as it has no input or output" + Colors.ENDC)
			del deviceList[device]

loadedConfig=loadConfig()
debug=loadedConfig['debug']
cardList=getCardList()
sinkList=getSinkList()
deviceList=loadedConfig['devices']
lastID=getLastID()
initialDevicesAmount=1
# Get biggest index this way instead of len() since we can have multiple devices as one index
for device in deviceList:
	if deviceList[device]['index'] > initialDevicesAmount: initialDevicesAmount = deviceList[device]['index']
# Prune outputs/inputs/devices that are not connected
pruneDeviceList()
# Print available devices after pruning
if debug:
	for device in list(deviceList):
		print('Available device ' + str(deviceList[device]['index']) + ': ' + device)
if len(deviceList) == 0:
	print(Colors.RED + "There's no available devices, exiting!" + Colors.ENDC)
	exit(1)
if "auto_null" in sinkList:
	print(Colors.RED + "No sources active, defaulting to the first index" + Colors.ENDC)
	setActive(index=1, deviceList=deviceList)
else:
	nextDeviceIndex = getNextDeviceToSwitchTo(deviceList=deviceList, lastIndex=lastID)
#		print(Colors.RED + "Current source unknown, defaulting to the first index" + Colors.ENDC)
	setActive(index=nextDeviceIndex, deviceList=deviceList)

# Hopeful workaround for a race condition on HDMI devices
time.sleep(0.100)
sinkList=getSinkList()
# Unmute all sinks, for some reason, probably a PA or a KDE update, the sinks start muted after a switch since around KDE 5.20
for sink in sinkList:
	unmuteSink(sinkID=str(sink['index']))

#if debug:
#	print("DEVICE LIST:", deviceList)
#	print("CARD LIST:", cardList)
#	print("SINK LIST:", sinkList)

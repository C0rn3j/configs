#!/usr/bin/env python
import logging
import os
import shutil
import subprocess
import sys
from dataclasses import dataclass
from termcolor import colored
from pathlib import Path
from pylxd import Client
from pylxd.models import Instance, Snapshot

logging.basicConfig(
	level=logging.INFO,
	format="%(asctime)s [%(levelname)s] %(message)s",
	handlers=[
		logging.StreamHandler()
	]
)
@dataclass
class CommandOutput:
	"""Class for shell command output."""

	def __init__(self, returnCode: int, stdOut: str, stdErr: str):
		self.returnCode = returnCode
		self.stdOut = stdOut
		self.stdErr = stdErr

def shell_exec(*, args:list[str]|str, timeout: int = 150) -> tuple[CommandOutput, list[str]|None]:
	"""Execute a shell command with subprocess.

	Args:
	----
		args (list[str] or str): Command and arguments to execute.
		timeout (int): Maximum time in seconds to wait for the command to complete.
		debug (bool): If True, print debugging information.

	Returns:
	-------
	CommandOutput: An object containing the returncode, stdout, and stderr.

	Raises:
	------
	ShellExecError: If an error occurs during command execution.

	"""
	logging.debug(f"Executing command: {' '.join(args) if isinstance(args, list) else args}")
	# Make sure env is available so xdg-open has access to XDG_CURRENT_DESKTOP
	# TODO it doesn't work -> we don't use xdg-open anymore, just forget about this?
#	logging.info(colored(text=os.environ, color='dark_grey'))
#	process = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True, env=os.environ.copy())
	process = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
	errorList = []
	try:
		stdout, stderr = process.communicate(timeout=timeout)
#		print(vars(process))
	except subprocess.TimeoutExpired as timedOutProcess:
		logging.warning(f"timedoutproctimeout is {timedOutProcess.stdout}; stderr is {timedOutProcess.stderr}")
		process.kill()
		stdout, stderr = timedOutProcess.stdout, timedOutProcess.stderr
#		stdout, stderr = process.communicate()
#		logging.warning(f"timedoutproctimeout postkill is {stdout}; stderr is {stderr}")
		logging.error(f"Command '{' '.join(args) if isinstance(args, list) else args}' timed out after {timeout} seconds, user probably ignored notification.")
		errorList.append('Timeout')
	except subprocess.SubprocessError:
		process.kill()
		stdout, stderr = process.communicate() # Is this useful? Needs testing
		logging.critical(f"An error occurred while executing command '{' '.join(args) if isinstance(args, list) else args}'.")
		errorList.append('Subprocess')

	# We have byte and str and None
	if stdout is None:
		stdout = ''
	elif isinstance(stdout, bytes):
		stdout = stdout.strip().decode('utf-8')
	elif isinstance(stdout, str):
		stdout = stdout.strip()
	else:
		logging.critical('Uknown command error!')
		return (CommandOutput(123, '', 'UKNOWN ERROR'), list('Unknown'))

	if stderr is None:
		stderr = ''
	elif isinstance(stderr, bytes):
		stderr = stderr.strip().decode('utf-8')
	elif isinstance(stderr, str):
		stderr = stderr.strip()
	else:
		logging.critical('Uknown command error!')
		return (CommandOutput(123, '', 'UKNOWN ERROR'), list('Unknown'))

	if stderr != '':
		logging.warning(colored(stderr, 'red'))

	return (CommandOutput(process.returncode, stdout, stderr), errorList)

def load_os_release() -> list:
	os_release_info = []
	try:
		with open('/etc/os-release', 'r') as file:
			os_release_dict = {}
			for line in file:
				if '=' in line:
					key, value = line.strip().split('=', 1)
					value = value.strip('"')  # Remove surrounding quotes if present
					os_release_dict[key] = value
			os_release_info.append(os_release_dict)
	except FileNotFoundError:
		print("The /etc/os-release file does not exist.")
	return os_release_info[0]

def pretty_size(*, bytes: int) -> str:
	if bytes < 1024:
		size = f"{bytes} bytes"
	elif bytes < 1048576:
		size = f"{bytes / 1024:.2f} KB"
	elif bytes < 1073741824:
		size = f"{bytes / 1048576:.2f} MB"
	else:
		size = f"{bytes / 1073741824:.2f} GB"
	return size

def boardInfo():
	if not os.path.isdir('/sys/class/dmi'):
		print(colored(text='Device: ', color='yellow'), colored(text='No DMI', color='magenta'))
		return
#	if ! chassis_serial=$(${sudoCommand} cat /sys/class/dmi/id/chassis_serial 2>/dev/null)
#		chassis_serial="N/A"
#
#	if [[ ${virtualization} == "none" ]]
#		if ! chassis_vendor=$(${sudoCommand} cat /sys/class/dmi/id/chassis_vendor 2>/dev/null)
#			chassis_vendor="N/A"
#
#		if [[ "${chassis_vendor}" == "Dell Inc." ]]
#			# Example URL: https://www.dell.com/support/home/us/en/04/product-support/servicetag/crzghv1/drivers
#			echo -e "${Yellow}Firmware update:${NoColor} https://www.dell.com/support/home/us/en/04/product-support/servicetag/${chassis_serial}/drivers"
#		elif [[ "${chassis_vendor}" == "LENOVO" ]]
#			if [[ -e /tmp/corn-info_motherboard ]]
#				LenovoURL=$(cat /tmp/corn-info_motherboard)
#			else
#				# Example URL: https://pcsupport.lenovo.com/en/us/products/laptops-and-netbooks/300-series/330-15ikb-type-81de/81de/81de012qin/pf170m4p/downloads
#				if ! LenovoURL=$(curl -s -L "https://pcsupport.lenovo.com/en/us/api/mse/getproducts?productId=${chassis_serial}" | grep -o -P '(?<=Id":").*(?=","Guid)')
#					echo -e "${Red}Failed running curl for pcsupport.lenovo.com${NoColor}"
#				else
#					echo "${LenovoURL}" > /tmp/corn-info_motherboard
#
#
#			echo -e "${Yellow}Firmware update:${NoColor} https://pcsupport.lenovo.com/en/us/products/${LenovoURL}/downloads"
#		elif [[ "${chassis_vendor}" == "HP" ]]
#			if [[ -e /tmp/corn-info_motherboard ]]
#				HPXML=$(cat /tmp/corn-info_motherboard)
#			else
#				if ! HPXML=$(curl -s -L "https://ftp.hp.com/pub/pcbios/${board_name}/${board_name}.xml")
#					echo -e "${Red}Failed running curl for pcsupport.lenovo.com${NoColor}"
#				else
#					echo "${HPXML}" > /tmp/corn-info_motherboard
#
#
#			HPLastVersion=$(echo "${HPXML}" | grep -oPm1 "(?<=Ver=\")[^<a-zA-Z\"]+")
#			HPLastVersionDate=$(echo "${HPXML}" | grep -oPm1 "(?<=Date=\")[^<a-zA-Z\"]+")
#			# Example URL: https://ftp.hp.com/pub/pcbios/1909/1909.xml
#			echo -e "${Yellow}Latest UEFI Version:${NoColor} ${HPLastVersion}"
#			echo -e "${Yellow}Latest UEFI Release Date:${NoColor} ${HPLastVersionDate}"
#
#		# Couldn't figure GIGABYTE and ASUS out, their search sucks to automate
#
#	if ! product_name=$(${sudoCommand} cat /sys/class/dmi/id/product_name 2>/dev/null)
#		product_name="N/A"
#
#	if ! board_name=$(${sudoCommand} cat /sys/class/dmi/id/board_name 2>/dev/null)
#		board_name="N/A"
#
#	echo -e "${Yellow}Device:${NoColor} ${product_name}"
#	if [[ ${board_name} != "N/A" ]]
#		echo -e "${Yellow}Motherboard:${NoColor} ${board_name}"
#		echo -e "${Yellow}  Vendor:${NoColor} $(cat /sys/class/dmi/id/board_vendor)"
#	else
#		echo -e "${Yellow}Motherboard:${NoColor} ${board_name}"
#
#	if [[ ${BootMode} == "UEFI" ]]
#		echo -e "${Yellow}  UEFI Date:${NoColor} $(cat /sys/class/dmi/id/bios_date)"
#	else
#		echo -e "${Yellow}  UEFI/BIOS Date:${NoColor} $(cat /sys/class/dmi/id/bios_date)"
#
#	echo -e "${Yellow}    Version:${NoColor} $(cat /sys/class/dmi/id/bios_version)"
#	echo -e "${Yellow}    Vendor:${NoColor} $(cat /sys/class/dmi/id/bios_vendor)"


def btrfsFeatures():
	# Btrfs has features that are optional or introduced after creation of existing volumes
	# This def checks all volumes and their featureset for specific features
	# You can run 'mkfs.btrfs -O list-all' to list all available features, or read /sys/fs/btrfs/features for it
	if not os.path.isfile('/sys/fs/btrfs'):
		return

	if os.path.isfile('/tmp/corn-info_BTRFS_FEATURES_OK'):
		return


#	readarray -t btrfsVolumes < <(find /sys/fs/btrfs -maxdepth 1 -type d | grep -E '/[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$')
#
#	btrfsWarnings=0
#	# Loop through the array of UUID directories
#	for uuid_dir in "${btrfsVolumes[@]}"; do
#		# Check if the 'blah' directory exists within the current UUID directory
#		if ! [[ -e "${uuid_dir}/features/no_holes" && $(cat "${uuid_dir}/features/no_holes") -eq 1 ]]
#			btrfsWarnings=1
#			echo -e "${Red}${uuid_dir} does not have 'no_holes' enabled, this is default since kernel 5.15, make sure volume is unmounted and run 'sudo btrfstune -n /dev/disk/by-uuid/$(basename "${uuid_dir}")'${NoColor}"
#
#		if ! [[ -e "${uuid_dir}/features/free_space_tree" && $(cat "${uuid_dir}/features/free_space_tree") -eq 1 ]]
#			btrfsWarnings=1
#			echo -e "${Red}${uuid_dir} does not have 'free_space_tree' enabled, this is default since kernel 5.15, make sure volume is unmounted and run 'sudo btrfstune --convert-to-free-space-tree /dev/disk/by-uuid/$(basename "${uuid_dir}")'${NoColor}"
#
#		# Only warn once per boot, it's not important
#		if ! [[ -e /tmp/corn-info_BTRFS_BLOCKGROUPTREE_WARNED ]]
#			if ! [[ -e "${uuid_dir}/features/block_group_tree" && $(cat "${uuid_dir}/features/block_group_tree") -eq 1 ]]
#				btrfsWarnings=1
#				echo -e "${Purple}${uuid_dir} does not have 'block_group_tree' enabled, this is supported(not safe though) since kernel 6.1, make sure volume is unmounted and run 'sudo btrfstune --convert-to-block-group-tree /dev/disk/by-uuid/$(basename "${uuid_dir}")'${NoColor}"
#				touch /tmp/corn-info_BTRFS_BLOCKGROUPTREE_WARNED
#
#
#	if [[ $btrfsWarnings -eq 0 ]]
#		touch /tmp/corn-info_BTRFS_FEATURES_OK


def accessTimestamps():
	if os.path.isfile('/tmp/corn-info_noatime'):
		return

	# filter out everything that's not a normal fs (ext4, btrfs, ...) and also everything that already has noatime set
#	if mounts=$(grep -v -e 'noatime' -e ' autofs ' -e ' binfmt_misc ' -e ' bpf ' -e ' cgroup2 ' -e ' configfs ' -e ' debugfs ' -e ' devpts ' -e ' devtmpfs ' -e ' efivarfs ' -e ' fuse.* ' -e ' hugetlbfs ' -e ' mqueue ' -e ' nsfs ' -e ' overlay ' -e ' proc ' -e ' pstore ' -e ' ramfs ' -e ' securityfs ' -e ' sysfs ' -e ' tmpfs ' -e ' tracefs ' -e ' rpc_pipefs ' /proc/mounts)
#		echo -e "${Red}Following mounts are missing 'noatime' to prevent reading files from writing data (access times): \n$(echo "${mounts}" | awk '{print "  " $2}' )${NoColor}"
#	else:
#		Path('/tmp/corn-info_noatime').touch()


def LUKSCheck():
	# Check that every LUKS2 device uses modern default argon2id
	if os.path.isfile('/tmp/corn-info_LUKS_OK'):
		return


#	if ! ENCRYPTED_DEVICES=$(blkid --match-token TYPE=crypto_LUKS -o device)
#		# No devices found
#		touch /tmp/corn-info_LUKS_OK
#		return
#
#
#	luksWarnings=0
#	for device in ${ENCRYPTED_DEVICES}; do
#		LUKS_INFO_JSON=$(${sudoCommand} cryptsetup luksDump --dump-json-metadata "${device}")
#		PBKDF_TYPES=$(echo "${LUKS_INFO_JSON}" | jq -r '.keyslots[] | select(.type == "luks2") | .kdf.type')
#		SLOT_ID=0
#		for PBKDF_TYPE in ${PBKDF_TYPES}; do
#			if [[ ${PBKDF_TYPE} != 'argon2id' ]]
#				luksWarnings=1
#				echo -e "${Red}Found ${PBKDF_TYPE} in slot ${SLOT_ID} instead of argon2id for PBKDF! Run 'sudo cryptsetup luksConvertKey --verbose ${device} --pbkdf argon2id'${NoColor}"
#
#			SLOT_ID=$((SLOT_ID + 1))
#		done
#	done
#	if [[ $luksWarnings -eq 0 ]]
#		touch /tmp/corn-info_LUKS_OK


def pacdiffStatus():
	print("WIP")
#	if ! command -v pacdiff &>/dev/null; then
#		echo -e "${Red}Could not run pacdiff from pacman-contrib as it was not found.${NoColor}"
#		return
#	fi
#	if ! ${sudoCommand} test -e /var/lib/plocate/plocate.db
#		if command -v updatedb &>/dev/null; then
#			${sudoCommand} updatedb
#		else
#			echo -e "${Red}Could not run pacdiff as updatedb from plocate not found.${NoColor}"
#			return
#		fi
#
#	if os.path.isfile('/tmp/pacdiffstatus'):
#		# Delete cached pacdiff output if older than 4 hours
#		find /tmp/pacdiffstatus -mmin +240 -exec rm /tmp/pacdiffstatus \;
#
#	if os.path.isfile('/tmp/pacdiffstatus'):
#		pacnews=$(cat /tmp/pacdiffstatus)
#		echo -e "${Yellow}Run pacdiff or otherwise take care of the following pacman files (cached):${Purple}\n${pacnews}${NoColor}"
#	else
#		if pacnews=$(pacdiff --output --locate)
#			if [[ ${pacnews} != "" ]]
#				echo "${pacnews}" > /tmp/pacdiffstatus
#				echo -e "${Yellow}Run pacdiff or otherwise take care of the following pacman files:${Purple}\n${pacnews}${NoColor}"
#
#		else
#			echo -e "${Red}Pacdiff error!${NoColor}"



def mirrorListStatus():
	print("WIP")
#	if os.path.isfile('/tmp/mirrorliststatus'):
#		if [ ! -s /tmp/mirrorliststatus ]
#			# Delete mirror list status file if empty
#			rm /tmp/mirrorliststatus
#		else
#			# Delete mirror list status file if older than 24 hours
#			find /tmp/mirrorliststatus -mmin +1440 -exec rm /tmp/mirrorliststatus \;
#
#
#	if not os.path.isfile('/tmp/mirrorliststatus'):
#		echo -e "${Yellow}Downloading mirror list status...${NoColor}"
#		if ! wget -q -O /tmp/mirrorliststatus https://www.archlinux.org/mirrors/status/json/
#			echo -e "${Red}Couldn't download mirrorlist status${NoColor}"
#
#
#	firstMirror=$(grep -m1 ^Server /etc/pacman.d/mirrorlist)
#	firstMirrorFiltered=${firstMirror%%\$*}
#	firstMirrorFiltered=${firstMirrorFiltered##*\ }
#	if ! mirrorSyncCompletion=$(jq ".urls[] | select(.url==\"${firstMirrorFiltered}\") | .completion_pct" /tmp/mirrorliststatus)
#		echo -e "${Red}Jq failed for ${firstMirrorFiltered}(${firstMirror})!${NoColor}"
#
#	if [[ ${mirrorSyncCompletion} != "1" ]]
#		echo -e "${Red}Your mirror ${firstMirrorFiltered} is out of sync - sync completion at ${mirrorSyncCompletion}!${NoColor}"


def nvidiaMismatchCheck():
	if not os.path.isfile('/usr/bin/nvidia-smi'):
		return

	if 'DISPLAY' not in os.environ:
		# Fallback in case the check is ran through tty
		os.environ['DISPLAY'] = ':0'

	if 'SUDO_USER' in os.environ:
		# Fallback in case the check is ran through ssh(?)
		# nvidia-settings will just exit if HOME isn't set how it likes it
		os.environ['HOME'] = f'/home/{os.environ['SUDO_USER']}'

	# TODO LANG=C
	commandOut, errList = shell_exec(args=['/usr/bin/nvidia-smi'])
	smiOutput = commandOut.stdOut
	if commandOut.returnCode != 0:
		if 'Failed to initialize NVML: Driver/library version mismatch' in commandOut.stdOut:
			commandOut2, errList2 = shell_exec(args=['/usr/bin/nvidia-smi', '--help'])
			first_line = commandOut2.stdOut.split('\n')[0]
			# Split the first line by whitespace and get the 6th element (index 5)
			installedDriverVer = first_line.split()[5]

			commandOut3, errList3 = shell_exec(args=['/usr/bin/nvidia-settings', '-q', 'NvidiaDriverVersion'])
			if commandOut3.returnCode == 0:
				filtered_lines = [line for line in commandOut3.stdOut.split('\n') if 'NvidiaDriverVersion' in line]
				if filtered_lines:
					# Take the first filtered line
					first_line = filtered_lines[0]
					# Split the first line by whitespace and get the 4th element (index 3)
					runningDriverVer = first_line.split()[3]
				else:
					runningDriverVer = None
			else:
				print(colored(f"Installed Nvidia driver {installedDriverVer}, but can't detect running driver through 'nvidia-settings -q NvidiaDriverVersion'!", 'red'))
				print(colored("  Reboot ASAP to fix!", 'red'))
				return
			print(colored(f"Installed Nvidia driver {installedDriverVer} does not match the running driver {runningDriverVer}!", 'red'))
			print(colored("  Reboot ASAP to fix!", 'red'))
		else:
			print(colored("Nvidia: Unknown smiOutput {smiOutput}", 'red'))
	else:
		splitSmi = smiOutput.split('\n')[2]
		print(f'{colored("Nvidia driver:", 'yellow')} {splitSmi.split()[2]}')


def Containers(instances:list[Instance]):
	print(colored(text='Containers:', color='yellow'))
	for instance in instances:
		if type(instance) is not Instance:
			print("Error, instance is not an Instance?!")
			continue
		instanceConfig:dict = instance.config
		instanceName = instance.name
		instanceState = instance.state()
		instanceStatus = instanceState.status
		instanceMemoryUsage = instanceState.memory['usage']
		maxMemString=''

		featureLetters=''
		if 'limits.memory' in instanceConfig:
			maxMem=instanceConfig['limits.memory']
			maxMemString=f' {colored('/','yellow')} {maxMem}'

		spacesToAdd=19-len(instanceName)
		if 'security.nesting' in instanceConfig and instanceConfig['security.nesting']:
			featureLetters=f'{colored('N', 'black')}'
			spacesToAdd=spacesToAdd - 1

		if 'security.privileged' in instanceConfig and instanceConfig['security.privileged']:
			featureLetters+=f'{colored('P', 'red')}'
			spacesToAdd=spacesToAdd - 1

		print(f'  {colored(instanceName, 'blue')} {featureLetters}{spacesToAdd * ' '}{pretty_size(bytes=instanceMemoryUsage)}{maxMemString}')
		snapshots:list[Snapshot] = instance.snapshots.all()
		for snapshot in snapshots:
			if not snapshot.stateful:
				print(f'{colored(f'    Manual snapshot: {snapshot.name}', 'red')}')

def problemUnits ():
	print("WIP")
#	if ! failingUnits=$(systemctl list-units --failed | awk '/failed/ { printf "%s ", $2 } END { printf "\n" }'); then
#		echo -e "${Red}Failed running systemctl to list units!${NoColor}"
#		return
#	fi
#	if [[ ${failingUnits} != '' ]]
#		echo -e "${Red}Problematic units: ${Purple}${failingUnits}${NoColor}"


def postInstall():
	print("""
Plasma taskbar:
* Move up and change the Panel height to 60
* Add Widgets - Memory Usage, Individual Core Usage, Network Speed
#* Add Widgets - Event calendar(plasma5-applets-eventcalendar)
#* Remove Digital Clock Widget - replaced by Event Calendar
#* Event calendar - set first line to HH:mm:ss
#* Event calendar - set second line to ddd  yyyy-MM-dd
#* Event calendar - Calendar - Style - Show Week Numbers: on
#* Event calendar - Calendar - Style - Event Badge: Bottom Bar (Event Color)
#* Event calendar - Google Calendar - login to personal acc and enable all calendars
#* Event calendar - Timezones - check UTC +0
* Network Speed - Display style: Text Only
* Memory Usage - Display style: Digital Gauge

# Seems not needed anymore on a fresh install
#System Settings -> Personalization -> Applications -> Default applications -> Set default apps
# Trying without
#Display configuration -> Global Scale 125%
Konsole -> Settings -> Configure Konsole... -> General -> Run all Konsole windows in a single process: On
Keyboard -> Advanced -> Configure keyboard options: On
Keyboard -> Advanced -> Position of Compose key: Right alt

KDEConnect -> Configure

Pin:
* Tauon music box
* Mumble
* Jitsi
* virt-manager
* Steam (runtime)
* Deemix
* Dolphin
* Pavucontrol
* Code
* Chromium
* Firefox
* KeepassXC
* qbittorrent
* Telegram
""")

def dkmsCheck():
	# Only run once per boot (assume /tmp is tmpfs) if successful, takes 0.25s to run dkms status
	if not os.path.isfile('/tmp/corn-info_dkmspassed'):
		if shutil.which('dkms'):
			commandOut, errList = shell_exec(args=['dkms', 'status'])
			# Need to check stderr too until this is fixed: https://github.com/dell/dkms/issues/352
			if commandOut.returnCode != 0 or 'error' in commandOut.stdErr:
				print(f'{colored(text="dkms error, check 'dkms status', fix the issue and run 'dkms autoinstall'!${NoColor}", color='red')}')
			else:
				Path('/tmp/corn-info_dkmspassed').touch()
		else:
			Path('/tmp/corn-info_dkmspassed').touch()

#sudoCommand='sudo'
#if [[ ${UID} == '0' ]]
#	sudoCommand=''
#
if os.path.isdir('/sys/firmware/efi'):
	BootMode='UEFI'
else:
	BootMode='BIOS'
osRelease = load_os_release()
osName = osRelease['NAME']
if osName != 'Arch Linux' and osName != 'Arch Linux ARM':
	print(colored('UNSUPPORTED OS','red'))
if not shutil.which('systemd-detect-virt'):
	print(colored('Error getting virtualization, no systemd?', 'red'))
	virtualization=''
else:
	commandOut, errList = shell_exec(args=['systemd-detect-virt'])
	virtualization=commandOut.stdOut
#echo -e "${Yellow}LAN IPv4 addresses:${NoColor} $(hostname -i)"
print(f'{colored('Hostname:','yellow')} {os.uname().nodename}')
print(f'{colored('Kernel:','yellow')} {os.uname().release}')
if virtualization != 'lxc' and osName == 'Arch Linux' or osName == 'Arch Linux ARM':
	if not shutil.which('corn-rebootcheck'):
		print(colored("corn-rebootcheck not found, can't check if reboot into a newer kernel is needed!", 'red'))

	commandOut, errList = shell_exec(args='corn-rebootcheck')
	if commandOut.returnCode == 0:
		if 'does not match' in commandOut.stdOut:
			print(commandOut.stdOut)
			print(colored('  Reboot at your convenience to fix!', 'grey'))
	elif commandOut.returnCode == 2:
			print(colored('corn-rebootcheck was unable to run due to Pacman lock being engaged!', 'purple'))
	else:
			print(colored('corn-rebootcheck broke', 'red'))

def pythonCheck():
	# Arch Linux only keeps latest Python version available, but due to reasons, older dependencies can survive and either clutter the system up or break it because they were not rebuilt
	# Keep last 3 previous versions in check, as AUR packages are not always tagged for rebuilding, so Python packages can be out of date
	print('TODO pythonCheck')
	return
#	localPythonVer=$(python -V)
#	expectedPythonVer='Python 3.13.'
#	deadVersions=('3.10' '3.11' '3.12')
#	for deadVersion in "${deadVersions[@]}":
#		if [[ -e /usr/lib/python${deadVersion} ]]:
#			echo -e "${Red}Found ${Purple}/usr/lib/python${deadVersion}${Red} which is a dead Python version on Arch - maybe packages need rebuilding?${NoColor}"
#			if [[ -e /usr/lib/python${deadVersion}/site-packages ]]:
#				echo -e "${Purple}$(ls -lah "/usr/lib/python${deadVersion}/site-packages")${NoColor}"
#		if [[ -e ${HOME}/.local/lib/python${deadVersion} ]]:
#			echo -e "${Red}Found ${Purple}${HOME}/.local/lib/python${deadVersion}${Red} which is a dead Python version on Arch - maybe packages need rebuilding?${NoColor}"
#			if [[ -e ${HOME}/.local/lib/python${deadVersion}/site-packages ]]:
#				echo -e "${Purple}$(ls -lah "${HOME}/.local/lib/python${deadVersion}/site-packages")${NoColor}"
#	if [[ ${localPythonVer} != *${expectedPythonVer}* ]]:
#			echo -e "${Red}Local python version ${localPythonVer} did not match the expected ${expectedPythonVer}. Did Python update to a newer minor version and a new string needs to be added while removing the oldest one?${NoColor}"

def mailQueue():
	if not shutil.which('mailq'):
		print(colored('mailq from postfix not found!', 'red'))
		return
	commandOut, errList = shell_exec(args=['mailq'])
	mailqOut=commandOut.stdOut
	if mailqOut != 'Mail queue is empty':
		print(f'{colored('Mail stuck in queue! Fix the issue then force flush with "postqueue -f"!\n { mailqOut }','red')}')

commandOut, errList = shell_exec(args=['uptime', '-p'])
print(f'{colored('Uptime:','yellow')} {commandOut.stdOut}')
print(f'{colored('Virtualization:','yellow')} {virtualization}')
#if ! timedatectl status | grep "synchronized: yes" >/dev/null
#	echo -e "${Red}NTP: No${NoColor}"
print(f'{colored('Boot mode:','yellow')} {BootMode}')
if osName == 'Arch Linux' or osName == 'Arch Linux ARM':
	nvidiaMismatchCheck()
	boardInfo()
	# This isn't a 100% detection for a broken mirror plus it is currently made in an annoying fashion which blocks the terminal
	#mirrorListStatus()
dkmsCheck()

# Needs AUR/python-pylxd and AUR/python-requests-unixsocket2
# Workaround otherwise it defaults to LXD
os.environ['LXD_DIR'] = '/var/lib/incus'
incusClient = Client()
instances:list[Instance] = incusClient.instances.all()
if len(instances) > 0:
	Containers(instances)

pacdiffStatus()
pythonCheck()
problemUnits()
LUKSCheck()
btrfsFeatures()
accessTimestamps()
mailQueue()
if len(sys.argv) > 1: # Hack, but eh
	postInstall()

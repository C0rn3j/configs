#!/usr/bin/env bash
set -euo pipefail
#[B]old, [U]nderline, [I]ntense, [BG_]Background
#Black='\033[0;30m'
BBlack='\033[1;30m'
#IBlack='\033[0;90m'
#UBlack='\033[4;30m'
#BIBlack='\033[1;90m'
#BG_Black='\033[40m'
#BG_IBlack='\033[0;100m'

Red='\033[0;31m'
#BRed='\033[1;31m'
#IRed='\033[0;91m'
#URed='\033[4;31m'
#BIRed='\033[1;91m'
#BG_Red='\033[41m'
#BG_IRed='\033[0;101m'

#Green='\033[0;32m'
#BGreen='\033[1;32m'
#IGreen='\033[0;92m'
#UGreen='\033[4;32m'
#BIGreen='\033[1;92m'
#BG_Green='\033[42m'
#BG_IGreen='\033[0;102m'

Yellow='\033[0;33m'
#BYellow='\033[1;33m'
#IYellow='\033[0;93m'
#UYellow='\033[4;33m'
#BIYellow='\033[1;93m'
#BG_Yellow='\033[43m'
#BG_IYellow='\033[0;103m'

Blue='\033[0;34m'
#BBlue='\033[1;34m'
#IBlue='\033[0;94m'
#UBlue='\033[4;34m'
#BIBlue='\033[1;94m'
#BG_Blue='\033[44m'
#BG_IBlue='\033[0;104m'

Purple='\033[0;35m'
#BPurple='\033[1;35m'
#IPurple='\033[0;95m'
#UPurple='\033[4;35m'
#BIPurple='\033[1;95m'
#BG_Purple='\033[45m'
#BG_IPurple='\033[0;105m'

#Cyan='\033[0;36m'
#BCyan='\033[1;36m'
#ICyan='\033[0;96m'
#UCyan='\033[4;36m'
#BICyan='\033[1;96m'
#BG_Cyan='\033[46m'
#BG_ICyan='\033[0;106m'

#White='\033[0;37m'
#BWhite='\033[1;37m'
#IWhite='\033[0;97m'
#UWhite='\033[4;37m'
#BIWhite='\033[1;97m'
#BG_White='\033[47m'
#BG_IWhite='\033[0;107m'

NoColor='\033[0m'

boardInfo() {
	if [[ ! -e /sys/class/dmi ]]; then
		echo -e "${Yellow}Device: ${Purple}No DMI${NoColor}"
		return
	fi
	if ! chassis_serial=$(${sudoCommand} cat /sys/class/dmi/id/chassis_serial 2>/dev/null); then
		chassis_serial="N/A"
	fi
	if [[ ${virtualization} == "none" ]]; then
		if ! chassis_vendor=$(${sudoCommand} cat /sys/class/dmi/id/chassis_vendor 2>/dev/null); then
			chassis_vendor="N/A"
		fi
		if [[ "${chassis_vendor}" == "Dell Inc." ]]; then
			# Example URL: https://www.dell.com/support/home/us/en/04/product-support/servicetag/crzghv1/drivers
			echo -e "${Yellow}Firmware update:${NoColor} https://www.dell.com/support/home/us/en/04/product-support/servicetag/${chassis_serial}/drivers"
		elif [[ "${chassis_vendor}" == "LENOVO" ]]; then
			if [[ -e /tmp/corn-info_motherboard ]]; then
				LenovoURL=$(cat /tmp/corn-info_motherboard)
			else
				# Example URL: https://pcsupport.lenovo.com/en/us/products/laptops-and-netbooks/300-series/330-15ikb-type-81de/81de/81de012qin/pf170m4p/downloads
				if ! LenovoURL=$(curl -s -L "https://pcsupport.lenovo.com/en/us/api/mse/getproducts?productId=${chassis_serial}" | grep -o -P '(?<=Id":").*(?=","Guid)'); then
					echo -e "${Red}Failed running curl for pcsupport.lenovo.com${NoColor}"
				else
					echo "${LenovoURL}" > /tmp/corn-info_motherboard
				fi
			fi
			echo -e "${Yellow}Firmware update:${NoColor} https://pcsupport.lenovo.com/en/us/products/${LenovoURL}/downloads"
		elif [[ "${chassis_vendor}" == "HP" ]]; then
			if [[ -e /tmp/corn-info_motherboard ]]; then
				HPXML=$(cat /tmp/corn-info_motherboard)
			else
				if ! HPXML=$(curl -s -L "https://ftp.hp.com/pub/pcbios/${board_name}/${board_name}.xml"); then
					echo -e "${Red}Failed running curl for pcsupport.lenovo.com${NoColor}"
				else
					echo "${HPXML}" > /tmp/corn-info_motherboard
				fi
			fi
			HPLastVersion=$(echo "${HPXML}" | grep -oPm1 "(?<=Ver=\")[^<a-zA-Z\"]+")
			HPLastVersionDate=$(echo "${HPXML}" | grep -oPm1 "(?<=Date=\")[^<a-zA-Z\"]+")
			# Example URL: https://ftp.hp.com/pub/pcbios/1909/1909.xml
			echo -e "${Yellow}Latest UEFI Version:${NoColor} ${HPLastVersion}"
			echo -e "${Yellow}Latest UEFI Release Date:${NoColor} ${HPLastVersionDate}"
		fi
		# Couldn't figure GIGABYTE and ASUS out, their search sucks to automate
	fi
	if ! product_name=$(${sudoCommand} cat /sys/class/dmi/id/product_name 2>/dev/null); then
		product_name="N/A"
	fi
	if ! board_name=$(${sudoCommand} cat /sys/class/dmi/id/board_name 2>/dev/null); then
		board_name="N/A"
	fi
	echo -e "${Yellow}Device:${NoColor} ${product_name}"
	if [[ ${board_name} != "N/A" ]]; then
		echo -e "${Yellow}Motherboard:${NoColor} ${board_name}"
		echo -e "${Yellow}  Vendor:${NoColor} $(cat /sys/class/dmi/id/board_vendor)"
	else
		echo -e "${Yellow}Motherboard:${NoColor} ${board_name}"
	fi
	if [[ ${BootMode} == "UEFI" ]]; then
		echo -e "${Yellow}  UEFI Date:${NoColor} $(cat /sys/class/dmi/id/bios_date)"
	else
		echo -e "${Yellow}  UEFI/BIOS Date:${NoColor} $(cat /sys/class/dmi/id/bios_date)"
	fi
	echo -e "${Yellow}    Version:${NoColor} $(cat /sys/class/dmi/id/bios_version)"
	echo -e "${Yellow}    Vendor:${NoColor} $(cat /sys/class/dmi/id/bios_vendor)"
}

btrfsFeatures() {
	# Btrfs has features that are optional or introduced after creation of existing volumes
	# This function checks all volumes and their featureset for specific features
	# You can run 'mkfs.btrfs -O list-all' to list all available features, or read /sys/fs/btrfs/features for it
	if [[ ! -e /sys/fs/btrfs ]]; then
		return
	fi
	if [[ -e /tmp/corn-info_BTRFS_FEATURES_OK ]]; then
		return
	fi

	readarray -t btrfsVolumes < <(find /sys/fs/btrfs -maxdepth 1 -type d | grep -E '/[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$')

	btrfsWarnings=0
	# Loop through the array of UUID directories
	for uuid_dir in "${btrfsVolumes[@]}"; do
		# Check if the 'blah' directory exists within the current UUID directory
		if ! [[ -e "${uuid_dir}/features/no_holes" && $(cat "${uuid_dir}/features/no_holes") -eq 1 ]]; then
			btrfsWarnings=1
			echo -e "${Red}${uuid_dir} does not have 'no_holes' enabled, this is default since kernel 5.15, make sure volume is unmounted and run 'sudo btrfstune -n /dev/disk/by-uuid/$(basename "${uuid_dir}")'${NoColor}"
		fi
		if ! [[ -e "${uuid_dir}/features/free_space_tree" && $(cat "${uuid_dir}/features/free_space_tree") -eq 1 ]]; then
			btrfsWarnings=1
			echo -e "${Red}${uuid_dir} does not have 'free_space_tree' enabled, this is default since kernel 5.15, make sure volume is unmounted and run 'sudo btrfstune --convert-to-free-space-tree /dev/disk/by-uuid/$(basename "${uuid_dir}")'${NoColor}"
		fi
		# Only warn once per boot, it's not important
		if ! [[ -e /tmp/corn-info_BTRFS_BLOCKGROUPTREE_WARNED ]]; then
			if ! [[ -e "${uuid_dir}/features/block_group_tree" && $(cat "${uuid_dir}/features/block_group_tree") -eq 1 ]]; then
				btrfsWarnings=1
				echo -e "${Purple}${uuid_dir} does not have 'block_group_tree' enabled, this is supported(not safe though) since kernel 6.1, make sure volume is unmounted and run 'sudo btrfstune --convert-to-block-group-tree /dev/disk/by-uuid/$(basename "${uuid_dir}")'${NoColor}"
				touch /tmp/corn-info_BTRFS_BLOCKGROUPTREE_WARNED
			fi
		fi
	done
	if [[ $btrfsWarnings -eq 0 ]]; then
		touch /tmp/corn-info_BTRFS_FEATURES_OK
	fi

}

accessTimestamps() {
	if [[ -e /tmp/corn-info_noatime ]]; then
		return
	fi
	# filter out everything that's not a normal fs (ext4, btrfs, ...) and also everything that already has noatime set
	if mounts=$(grep -v -e 'noatime' -e ' autofs ' -e ' binfmt_misc ' -e ' bpf ' -e ' cgroup2 ' -e ' configfs ' -e ' debugfs ' -e ' devpts ' -e ' devtmpfs ' -e ' efivarfs ' -e ' fuse.* ' -e ' hugetlbfs ' -e ' mqueue ' -e ' nsfs ' -e ' overlay ' -e ' proc ' -e ' pstore ' -e ' ramfs ' -e ' securityfs ' -e ' sysfs ' -e ' tmpfs ' -e ' tracefs ' -e ' rpc_pipefs ' /proc/mounts); then
		echo -e "${Red}Following mounts are missing 'noatime' to prevent reading files from writing data (access times): \n$(echo "${mounts}" | awk '{print "  " $2}' )${NoColor}"
	else
		touch /tmp/corn-info_noatime
	fi
}

LUKSCheck() {
	# Check that every LUKS2 device uses modern default argon2id
	if [[ -e /tmp/corn-info_LUKS_OK ]]; then
		return
	fi

	if ! ENCRYPTED_DEVICES=$(blkid --match-token TYPE=crypto_LUKS -o device); then
		# No devices found
		touch /tmp/corn-info_LUKS_OK
		return
	fi

	luksWarnings=0
	for device in ${ENCRYPTED_DEVICES}; do
		LUKS_INFO_JSON=$(${sudoCommand} cryptsetup luksDump --dump-json-metadata "${device}")
		PBKDF_TYPES=$(echo "${LUKS_INFO_JSON}" | jq -r '.keyslots[] | select(.type == "luks2") | .kdf.type')
		SLOT_ID=0
		for PBKDF_TYPE in ${PBKDF_TYPES}; do
			if [[ ${PBKDF_TYPE} != 'argon2id' ]]; then
				luksWarnings=1
				echo -e "${Red}Found ${PBKDF_TYPE} in slot ${SLOT_ID} instead of argon2id for PBKDF! Run 'sudo cryptsetup luksConvertKey --verbose ${device} --pbkdf argon2id'${NoColor}"
			fi
			SLOT_ID=$((SLOT_ID + 1))
		done
	done
	if [[ $luksWarnings -eq 0 ]]; then
		touch /tmp/corn-info_LUKS_OK
	fi
}

pacdiffStatus() {
	if ! command -v pacdiff &>/dev/null; then
		echo -e "${Red}Could not run pacdiff from pacman-contrib as it was not found.${NoColor}"
		return
	fi
	if ! ${sudoCommand} test -e /var/lib/plocate/plocate.db; then
		if command -v updatedb &>/dev/null; then
			${sudoCommand} updatedb
		else
			echo -e "${Red}Could not run pacdiff as updatedb from plocate not found.${NoColor}"
			return
		fi
	fi
	if [[ -e /tmp/pacdiffstatus ]]; then
		# Delete cached pacdiff output if older than 4 hours
		find /tmp/pacdiffstatus -mmin +240 -exec rm /tmp/pacdiffstatus \;
	fi
	if [[ -e /tmp/pacdiffstatus ]]; then
		pacnews=$(cat /tmp/pacdiffstatus)
		echo -e "${Yellow}Run pacdiff or otherwise take care of the following pacman files (cached):${Purple}\n${pacnews}${NoColor}"
	else
		if pacnews=$(pacdiff --output --locate); then
			if [[ ${pacnews} != "" ]]; then
				echo "${pacnews}" > /tmp/pacdiffstatus
				echo -e "${Yellow}Run pacdiff or otherwise take care of the following pacman files:${Purple}\n${pacnews}${NoColor}"
			fi
		else
			echo -e "${Red}Pacdiff error!${NoColor}"
		fi
	fi
}

mirrorListStatus() {
	if [[ -e /tmp/mirrorliststatus ]]; then
		if [ ! -s /tmp/mirrorliststatus ] ; then
			# Delete mirror list status file if empty
			rm /tmp/mirrorliststatus
		else
			# Delete mirror list status file if older than 24 hours
			find /tmp/mirrorliststatus -mmin +1440 -exec rm /tmp/mirrorliststatus \;
		fi
	fi
	if [[ ! -e /tmp/mirrorliststatus ]]; then
		echo -e "${Yellow}Downloading mirror list status...${NoColor}"
		if ! wget -q -O /tmp/mirrorliststatus https://www.archlinux.org/mirrors/status/json/; then
			echo -e "${Red}Couldn't download mirrorlist status${NoColor}"
		fi
	fi
	firstMirror=$(grep -m1 ^Server /etc/pacman.d/mirrorlist)
	firstMirrorFiltered=${firstMirror%%\$*}
	firstMirrorFiltered=${firstMirrorFiltered##*\ }
	if ! mirrorSyncCompletion=$(jq ".urls[] | select(.url==\"${firstMirrorFiltered}\") | .completion_pct" /tmp/mirrorliststatus); then
		echo -e "${Red}Jq failed for ${firstMirrorFiltered}(${firstMirror})!${NoColor}"
	fi
	if [[ ${mirrorSyncCompletion} != "1" ]]; then
		echo -e "${Red}Your mirror ${firstMirrorFiltered} is out of sync - sync completion at ${mirrorSyncCompletion}!${NoColor}"
	fi
}

nvidiaMismatchCheck() {
	if [[ ! -e /usr/bin/nvidia-smi ]]; then
		return
	fi
	if [[ -z ${DISPLAY-} ]]; then
		# Fallback in case the check is ran through tty
		export DISPLAY=':0'
	fi
	if [[ -n ${SUDO_USER-} ]]; then
		# Fallback in case the check is ran through ssh(?)
		# nvidia-settings will just exit if HOME isn't set how it likes it
		export HOME="/home/${SUDO_USER}"
	fi
	if ! smiOutput=$(LANG=C /usr/bin/nvidia-smi); then
		if [[ ${smiOutput} == "Failed to initialize NVML: Driver/library version mismatch"* ]]; then
			installedDriverVer=$(nvidia-smi --help | awk 'NR==1{print $6}')
			if ! runningDriverVer=$(nvidia-settings -q NvidiaDriverVersion 2>/dev/null | grep NvidiaDriverVersion | awk 'NR==1{print $4}'); then
				echo -e "${Red}Installed Nvidia driver ${installedDriverVer}, but can't detect running driver through 'nvidia-settings -q NvidiaDriverVersion'!${NoColor}"
				echo -e "${Red}  Reboot ASAP to fix!${NoColor}"
				return
			fi
			echo -e "${Red}Installed Nvidia driver ${installedDriverVer} does not match the running driver ${runningDriverVer}!${NoColor}"
			echo -e "${Red}  Reboot ASAP to fix!${NoColor}"
		else
			echo -e "${Red}Nvidia: Unknown smiOutput ${smiOutput}${NoColor}"
		fi
	else
		echo -e "${Yellow}Nvidia driver:${NoColor} $( echo "${smiOutput}" | awk 'NR==3{ print $3 }')"
	fi
}

Containers() {
	for container in ${incusList}; do
		containerInfo=$(incus info "${container}")
		containerConfig=$(incus config show "${container}")

		currentMemUsage=$(   echo "${containerInfo}"   | grep 'Memory (current):'        | awk '{print $3}' || echo NaN)
		maxMem=$(            echo "${containerConfig}" | grep 'memory:'                  | awk '{print $2}' || echo NaN)
		securityNesting=$(   echo "${containerConfig}" | grep '^  security.nesting: '    | awk '{print $2}' || echo NaN)
		securityPrivileged=$(echo "${containerConfig}" | grep '^  security.privileged: ' | awk '{print $2}' || echo NaN)
		featureLetters=''

		spacesToAdd=$((19-${#container}))
		# If key is set, assume true
		if [[ ${securityNesting} != 'NaN' ]]; then
			featureLetters="${BBlack}N"
			spacesToAdd=$((spacesToAdd - 1))
		fi
		if [[ ${securityPrivileged} != 'NaN' ]]; then
			featureLetters="${featureLetters}${Red}P"
			spacesToAdd=$((spacesToAdd - 1))
		fi

		containerSpaced=''
		for i in $(seq 1 $((spacesToAdd))); do
			containerSpaced="${containerSpaced} "
		done
		echo -e "${Blue}  ${container} ${featureLetters}${containerSpaced}${NoColor} ${currentMemUsage} ${Yellow}/${NoColor} ${maxMem}"
		if snapshotList=$(echo "${containerInfo}" | grep -A99999 ^Snapshots:); then
			if manualSnapshots=$(echo "${snapshotList}" | grep -v autosnapshot); then
				echo -e "${Red}  Manual snapshots detected!\n ${manualSnapshots}${NoColor}" >&2
			fi
		fi
	done
}

problemUnits () {
	if ! failingUnits=$(systemctl list-units --failed | awk '/failed/ { printf "%s ", $2 } END { printf "\n" }'); then
		echo -e "${Red}Failed running systemctl to list units!${NoColor}"
		return
	fi
	if [[ ${failingUnits} != '' ]]; then
		echo -e "${Red}Problematic units: ${Purple}${failingUnits}${NoColor}"
	fi
}

postInstall() {
	cat <<EOF
Plasma taskbar:
* Move up and change the Panel height to 60
* Add Widgets - Memory Usage, Individual Core Usage, Network Speed
#* Add Widgets - Event calendar(plasma5-applets-eventcalendar)
#* Remove Digital Clock Widget - replaced by Event Calendar
#* Event calendar - set first line to HH:mm:ss
#* Event calendar - set second line to ddd  yyyy-MM-dd
#* Event calendar - Calendar - Style - Show Week Numbers: on
#* Event calendar - Calendar - Style - Event Badge: Bottom Bar (Event Color)
#* Event calendar - Google Calendar - login to personal acc and enable all calendars
#* Event calendar - Timezones - check UTC +0
* Network Speed - Display style: Text Only
* Memory Usage - Display style: Digital Gauge

# Seems not needed anymore on a fresh install
#System Settings -> Personalization -> Applications -> Default applications -> Set default apps
# Trying without
#Display configuration -> Global Scale 125%
Konsole -> Settings -> Configure Konsole... -> General -> Run all Konsole windows in a single process: On
Keyboard -> Advanced -> Configure keyboard options: On
Keyboard -> Advanced -> Position of Compose key: Right alt

KDEConnect -> Configure

Pin:
* Tauon music box
* Mumble
* Jitsi
* virt-manager
* Steam (runtime)
* Deemix
* Dolphin
* Pavucontrol
* Code
* Chromium
* Firefox
* KeepassXC
* qbittorrent
* Telegram
EOF
}

dkmsCheck() {
	# Only run once per boot (assume /tmp is tmpfs) if successful, takes 0.25s to run dkms status
	if [[ ! -e /tmp/corn-info_dkmspassed ]]; then
		if command -v dkms &>/dev/null; then
			if dkms status 2>&1 | grep -i error &>/dev/null; then
# We can't do this because error codes are broken https://github.com/dell/dkms/issues/352
#			if ! dkms status &>/dev/null; then
				echo -e "${Red}dkms error, check 'dkms status', fix the issue and run 'dkms autoinstall'!${NoColor}"
			else
				touch /tmp/corn-info_dkmspassed
			fi
		else
			touch /tmp/corn-info_dkmspassed
		fi
	fi
}

pythonCheck() {
	# Arch Linux only keeps latest Python version available, but due to reasons, older dependencies can survive and either clutter the system up or break it because they were not rebuilt
	# Keep last 3 previous versions in check, as AUR packages are not always tagged for rebuilding, so Python packages can be out of date
	localPythonVer=$(python -V)
	expectedPythonVer='Python 3.13.'
	deadVersions=('3.10' '3.11' '3.12')
	for deadVersion in "${deadVersions[@]}"; do
		if [[ -e /usr/lib/python${deadVersion} ]]; then
			echo -e "${Red}Found ${Purple}/usr/lib/python${deadVersion}${Red} which is a dead Python version on Arch - maybe packages need rebuilding?${NoColor}"
			if [[ -e /usr/lib/python${deadVersion}/site-packages ]]; then
				echo -e "${Purple}$(ls -lah "/usr/lib/python${deadVersion}/site-packages")${NoColor}"
			fi
		fi
		if [[ -e ${HOME}/.local/lib/python${deadVersion} ]]; then
			echo -e "${Red}Found ${Purple}${HOME}/.local/lib/python${deadVersion}${Red} which is a dead Python version on Arch - maybe packages need rebuilding?${NoColor}"
			if [[ -e ${HOME}/.local/lib/python${deadVersion}/site-packages ]]; then
				echo -e "${Purple}$(ls -lah "${HOME}/.local/lib/python${deadVersion}/site-packages")${NoColor}"
			fi
		fi
	done
	if [[ ${localPythonVer} != *${expectedPythonVer}* ]]; then
			echo -e "${Red}Local python version ${localPythonVer} did not match the expected ${expectedPythonVer}. Did Python update to a newer minor version and a new string needs to be added while removing the oldest one?${NoColor}"
	fi
}

mailQueue() {
	if ! command -v mailq &>/dev/null; then
		echo -e "${Red}mailq from postfix not found!${NoColor}"
		return
	fi
	mailqOut=$(mailq)
	if [[ ${mailqOut} != 'Mail queue is empty' ]]; then
		echo -e "${Red}Mail stuck in queue! ! Fix the issue then force flush with "postqueue -f"\n${mailqOut}${NoColor}"
	fi
}

sudoCommand='sudo'
if [[ ${UID} == '0' ]]; then
	sudoCommand=''
fi

if [[ -d /sys/firmware/efi ]]; then BootMode="UEFI"; else BootMode="BIOS"; fi
source /etc/os-release
if [[ ${NAME} != "Arch Linux" ]] && [[ ${NAME} != "Arch Linux ARM" ]]; then
	echo -e "${Red}UNSUPPORTED OS${NoColor}"
fi
if ! command -v systemd-detect-virt &>/dev/null; then
	echo -e "${Red}Error getting virtualization, no systemd?${NoColor}"
else
	virtualization=$(systemd-detect-virt) || true
fi
#echo -e "${Yellow}LAN IPv4 addresses:${NoColor} $(hostname -i)"
echo -e "${Yellow}Hostname:${NoColor} $(uname -n)"
echo -e "${Yellow}Kernel:${NoColor} $(uname -r)"
if [[ ${virtualization} != "lxc" ]] && { [[ ${NAME} == "Arch Linux" ]] || [[ ${NAME} == "Arch Linux ARM" ]]; }; then
	if ! cornRebootcheckPath=$(command -v corn-rebootcheck); then
		echo -e "${Red}corn-rebootcheck not found, can't check if reboot into a newer kernel is needed!${NoColor}"
	fi
	if rebootcheck=$(${cornRebootcheckPath}); then
		if echo "${rebootcheck}" | grep 'does not match' > /dev/null; then
			echo -e "${rebootcheck}"
			echo -e "${BBlack}  Reboot at your convenience to fix!${NoColor}"
		fi
	else
		# Code 2, but checking it in Bash is annoying, just check output
		if [[ ${rebootcheck} == *'lockfile detected'* ]]; then
			echo -e "${Purple}corn-rebootcheck was unable to run due to Pacman lock being engaged${NoColor}"
		else
			echo -e "${Red}corn-rebootcheck broke${NoColor}"
		fi
	fi
fi
echo -e "${Yellow}Uptime:${NoColor} $(uptime -p)"
echo -e "${Yellow}Virtualization:${NoColor} ${virtualization}"
if ! timedatectl status | grep "synchronized: yes" >/dev/null; then
	echo -e "${Red}NTP: No${NoColor}"
fi
echo -e "${Yellow}Boot mode:${NoColor} ${BootMode}"
if [[ ${NAME} == "Arch Linux" ]] || [[ ${NAME} == "Arch Linux ARM" ]]; then
	nvidiaMismatchCheck
	boardInfo
	# This isn't a 100% detection for a broken mirror plus it is currently made in an annoying fashion which blocks the terminal
	#mirrorListStatus
fi
dkmsCheck

if incusList=$(incus list --fast 2>/dev/null | grep 'RUNNING' | awk '{print $2}'); then
	echo -e "${Yellow}Containers:${NoColor}"
	Containers
fi

pacdiffStatus
pythonCheck
problemUnits
LUKSCheck
btrfsFeatures
accessTimestamps
mailQueue
if [[ -n ${1-} ]]; then # Hack, but eh
	postInstall
fi

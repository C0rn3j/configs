#!/usr/bin/env bash
set -euo pipefail
Red='\033[0;31m'
Green='\033[0;32m'
Yellow='\033[0;33m'
Purple='\033[0;35m'
NoColor='\033[0m'

if [[ -e /var/lib/pacman/db.lck ]]; then
	echo -e "${Purple}Pacman lockfile detected, not doing a check to prevent false positives.${NoColor}"
	exit 2 # Code 2 reserved only for this error
fi

kernelVersion=''
kernelSuffix=''
# Go through the uname -r string by splitting by "-" and reading into one part until an element that matches a-zA-Z is found. In which case that's the beginning of the second part
splitUname() {
	local version="$1"
	local IFS='-'
	read -ra ADDR <<< "$version" # Read version into an array
	local first_part=""
	local second_part=""
	local found_alpha=0

	for part in "${ADDR[@]}"; do
		if [[ $part =~ ^[a-zA-Z]+$ ]] && [[ $found_alpha -eq 0 ]]; then
			found_alpha=1
			second_part+="$part"
		elif [[ $found_alpha -eq 1 ]]; then
			second_part+="-${part}"
		else
			if [[ -n "$first_part" ]]; then
				first_part+="-"
			fi
			first_part+="$part"
		fi
	done
	kernelVersion="${first_part}"
	kernelSuffix="${second_part}"
	if [[ ${second_part} != '' ]]; then
		kernelSuffix="-${kernelSuffix}"
	fi
}

runTestCases() {
	# Test cases that need to work
	${0} --running "6.5.8-arch1-1"              --installed "linux 6.5.8.arch1-1"
	${0} --running "6.6.10-arch1-1"             --installed "linux 6.6.10.arch1-1"
	${0} --running "6.1.59-1-lts"               --installed "linux-lts 6.1.59-1"
	${0} --running "6.5.8-zen1-1-zen"           --installed "linux-zen 6.5.8.zen1-1"
	${0} --running "6.5.8-1-clear"              --installed "linux-clear 6.5.8-1"
	${0} --running "6.6.0-rc7-1-mainline"       --installed "linux-mainline 6.6rc7-1"
	${0} --running "6.5.2.8.realtime1-1-rt"     --installed "linux-rt 6.5.2.8.realtime1-1"
	${0} --running "6.5.8-hardened1-1-hardened" --installed "linux-hardened 6.5.8.hardened1-1"
	${0} --running "6.6.18-1-rpi-16k"           --installed "linux-rpi-16k 6.6.18-1"
	#echo -e "${Purple}Xanmod is broken, someone should poke maintainer at AUR/linux-xanmod if they want it fixed${NoColor}"
	#${0} --running "6.5.8-x64v2-xanmod1-1"      --installed "linux-xanmod 6.5.8-1"

	# Historical cases
	#  5.2.8-arch1-1-ARCH | linux 5.2.8.arch1-1 # pre-2019-11-11
	#  5.3.10-arch1-1     | linux 5.3.10.1-1    # pre-2019-12-02
}

Help() {
	echo -e "${Yellow}"
	cat << EOF
Compares output of pacman to uname -r, while replacing all dashes for dots. The .0.arch sed is there because .0 kernel releases are named poorly without the .0 when compared to uname -r
uname -r && pacman -Q linux(-version)

Options:
--notify                           Send a notification using notify-send
--running                          Override running kernel, useful for tests
--installed                        Override installed kernel, useful for tests
--test                             Run test cases, flag for development
-h | --help                        Show this help...

Usage examples:
  corn-rebootcheck
  corn-rebootcheck --running "6.5.7-arch1-1" --installed "linux 6.5.8.arch1-1" --notify
EOF
	echo -e "${NoColor}"
}

notify="no"
runningOverride="no"
installedOverride="no"
while [[ -n ${1-} ]]; do
	case ${1} in
		--notify )
			notify="yes";;
		--running )
			shift
			runningOverride=${1};;
		--installed )
			shift
			installedOverride=${1};;
		--test )
			runTestCases
			exit 0;;
		-h | --help )
			Help
			exit 0;;
		* )
			echo -e "${Red}Wrong parameter: ${1} ${NoColor}"
			Help
			exit 1;;
	esac
	shift
done


if ! command -v systemd-detect-virt &>/dev/null; then
	echo "Error getting virtualization, no systemd?"
	exit 1
else
	virtualization=$(systemd-detect-virt) || true
fi
if [[ ${virtualization} == 'lxc' ]]; then
	echo 'System is an LXC container, exiting'
	exit 0
fi
if [[ ${runningOverride} != "no" ]]; then
	runningKernelFull=$runningOverride
else
	runningKernelFull=$(uname -r)
fi

splitUname "${runningKernelFull}"
# Old unreliable check
## Get suffix, on 'linux' it's going to be '-1'(which we remove), on 'linux-zen' it will be '-zen'
#kernelSuffix="$(echo -${runningKernelFull##*-} | sed s/-[1-9]//)"
## "s/\\.0-/-/" check is there because convention is that running kernel X.Y.0 is installed as X.Y
#runningKernel=$(echo ${runningKernelFull} | sed -e s/${kernelSuffix}$// -e s/\\.0-/-/ -e s/-/./g -e s/[.]rc/rc/ )
## Remove potential -1 suffix, this will happen on 'linux'
#kernelSuffix="$(echo ${kernelSuffix})"
runningKernel="${kernelVersion}"
if [[ ${installedOverride} != "no" ]]; then
	installedKernelFull=$installedOverride
	guessedPackageName="linux${kernelSuffix}"
	actualPackageName=$(echo "${installedOverride}" | awk '{print $1}')
	if [[ ${guessedPackageName} != "${actualPackageName}" ]]; then
		echo -e "${Red}We are guessing the wrong package name, ${Purple}${guessedPackageName}${Red} instead of ${Green}${actualPackageName}${Red}!${NoColor}"
	fi
else
	if ! installedKernelFull=$(pacman -Q "linux${kernelSuffix}"); then
		echo -e "${Red}Reboot check script broke. Running kernel ${Green}${runningKernel}${Red}. Can't get output of pacman -Q linux${kernelSuffix}${NoColor}"
		if [[ ${notify} == "yes" ]]; then
			notify-send "Reboot check script broke." "Running kernel ${runningKernel}.\nCan't get output of 'pacman -Q linux${kernelSuffix}'"
		fi
		exit 1
	fi
fi
#installedKernel=$(echo "${installedKernelFull##* }" | sed -e 's/-/./g')
installedKernel=${installedKernelFull##* }
installedKernelCheck=${installedKernel//-/.}
runningKernelCheck=${runningKernel//.0-/}
runningKernelCheck=${runningKernelCheck//-/.}
if [[ ${runningKernelCheck} != "${installedKernelCheck}" ]]; then
#	echo "DEBUG: ${runningKernelCheck} != ${installedKernelCheck}"
	echo -e "${Yellow}Running kernel ${Purple}${runningKernelFull}${Yellow} does not match installed kernel ${Green}${installedKernel}${kernelSuffix}${Yellow}!${NoColor}"
	if [[ ${notify} == "yes" ]]; then
		notify-send "Reboot required!" "Running kernel ${runningKernelFull} is different from installed kernel ${installedKernelFull}! \nReboot now to ensure your system behaves correctly.\n
Stop showing the warning for this boot by executing:\n systemctl stop rebootcheck.timer --user"
	fi
else
	echo -e "${Yellow}Running kernel ${Green}${runningKernel}${Yellow} matches installed kernel${NoColor}"
fi

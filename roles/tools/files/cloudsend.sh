#!/usr/bin/env bash

############################################################
#
# cloudsend.sh
#
# Uses curl to send files to a shared
# Nextcloud/Owncloud folder
#
# Usage: ./cloudsend.sh <file> <folderLink>
# Help:  ./cloudsend.sh -h
#
# Gustavo Arnosti Neves
# https://github.com/tavinus
#
# Contributors:
# @MG2R @gessel @C0rn3j
#
# Get this script to current folder with:
# curl -O 'https://gist.githubusercontent.com/tavinus/93bdbc051728748787dc22a58dfe58d8/raw/cloudsend.sh' && chmod +x cloudsend.sh
#
############################################################


CS_VERSION="0.1.7"

CLOUDURL="https://cloud.rys.rs/s/oJeDzaftDncqJTi"
FOLDERTOKEN=""

PUBSUFFIX="public.php/webdav"
HEADER='X-Requested-With: XMLHttpRequest'
INSECURE=''

# https://cloud.mydomain.net/s/fLDzToZF4MLvG28
# curl -k -T myFile.ext -u "fLDzToZF4MLvG28:" -H 'X-Requested-With: XMLHttpRequest' https://cloud.mydomain.net/public.php/webdav/myFile.ext

log() {
    [ "$VERBOSE" == " -s" ] || printf "%s\n" "$1"
}

printVersion() {
    printf "%s\n" "CloudSender v$CS_VERSION"
}

initError() {
    printVersion >&2
    printf "%s\n" "Init Error! $1" >&2
    printf "%s\n" "Try: $0 --help" >&2
    exit 1
}

usage() {
    printVersion
    printf "\n%s%s\n" "Parameters:" "
  -h | --help      Print this help and exits
  -q | --quiet     Be quiet
  -V | --version   Prints version and exits
  -f | --file      Optionally specify the file via a flag, must be used together with -u
  -u | --url       Optionally specify the URL via a flag, must be used together with -f
  -k | --insecure  Uses curl with -k option (https insecure)
  -p | --password  Uses env var \$CLOUDSEND_PASSWORD as share password
                   You can 'export CLOUDSEND_PASSWORD' at your system, or set it at the call.
                   Please remeber to also call -p to use the password set."
    printf "\n%s\n%s\n%s\n" "Use:" "  $0 <filepath> <folderLink>" "  CLOUDSEND_PASSWORD='MySecretPass' $0 -p <filepath> <folderLink>"
    printf "\n%s\n%s\n%s\n" "Example:" "  $0 './myfile.txt' 'https://cloud.mydomain.net/s/fLDzToZF4MLvG28'"  "   CLOUDSEND_PASSWORD='MySecretPass' $0 -p './myfile.txt' 'https://cloud.mydomain.net/s/fLDzToZF4MLvG28'"
}

##########################
# Process parameters

if [[ -z ${1-} ]]; then
    usage
    exit 1
fi

FULL_ARGS="${BASH_ARGV[*]}"
while [[ -n ${1-} ]]; do
    case ${1} in
        -u | --url )
            shift
            CLOUDURL=${1};;
        -f | --file )
            shift
            FILENAME=${1};;
        -p | --password )
            PASSWORD=${CLOUDSEND_PASSWORD}
            log " > Using password from env";;
        -k | --insecure )
            INSECURE=' -k'
            log " > Insecure mode ON";;
        -q | --quiet )
            VERBOSE=" -s";;
        -V | --version )
            printVersion
            exit 0;;
        -h | --help )
            usage
            exit 0;;
        * )
            # If -f or -u are defined, it's a syntax error. If not, we're using positional arguments instead of those defined by flags.
            if [[ " $FULL_ARGS " =~ [[:space:]](-u|--url|-f|--file)[[:space:]] ]]; then
                usage
                exit 1
            else
                FILENAME="$1"
                CLOUDURL="$2"
                break
            fi;;
    esac
    shift
done

##########################
# Validate input

FOLDERTOKEN="${CLOUDURL##*/s/}"

# if we have index.php in the URL, process accordingly
if [[ $CLOUDURL == *"index.php"* ]]; then
    CLOUDURL="${CLOUDURL%/index.php/s/*}"
else
    CLOUDURL="${CLOUDURL%/s/*}"
fi

if [ ! -f "$FILENAME" ]; then
    initError "Invalid input file: $FILENAME"
fi

if [ -z "$CLOUDURL" ]; then
    initError "Empty URL! Nowhere to send..."
fi

if [ -z "$FOLDERTOKEN" ]; then
    initError "Empty Folder Token! Nowhere to send..."
fi


##########################
# Check for curl

CURLBIN='/usr/bin/curl'
if [ ! -x "$CURLBIN" ]; then
    CURLBIN="$(command -v curl 2>/dev/null)"
    if [ ! -x "$CURLBIN" ]; then
        initError "No curl found on system!"
    fi
fi


##########################
# Extract base filename

BFILENAME=$(/usr/bin/basename $FILENAME)


##########################
# Send file

#echo "$CURLBIN"$INSECURE$VERBOSE -T "$FILENAME" -u "$FOLDERTOKEN":"$PASSWORD" -H "$HEADER" "$CLOUDURL/$PUBSUFFIX/$BFILENAME"
"$CURLBIN"$INSECURE$VERBOSE -T "$FILENAME" -u "$FOLDERTOKEN":"$PASSWORD" -H "$HEADER" "$CLOUDURL/$PUBSUFFIX/$BFILENAME"

#!/usr/bin/env bash
set -euo pipefail

# https://developer.valvesoftware.com/wiki/Counter-Strike:_Global_Offensive_Game_State_Integration
# Also tee into game directory itself? Does not seem to be needed
gameDir='/mnt/1tbADATA/SteamLibrary/steamapps/common/Counter-Strike Global Offensive/game/csgo/cfg'
gameDirFlatpak=~/.var/app/com.valvesoftware.Steam/.local/share/Steam/steamapps/common/Counter-Strike\ Global\ Offensive/game/csgo/cfg
flatpakSteamPrefix=~/.var/app/com.valvesoftware.Steam/.steam/steam
localSteamPrefix=~/.steam/steam
if [[ ! -e "${gameDir}" ]]; then
	echo "Game directory not found in: ${gameDir}"
	exit 1
fi

createConfigFile() {
	local cfgName="${1}"
	echo "Copying config ${cfgName}"
	for userDir in "${userDirs[@]}"; do
		cp /tmp/csgo.conf "${gameDir}/${cfgName}.cfg"
		cp /tmp/csgo.conf "${userDir}/730/local/cfg/${cfgName}.cfg"
	done
	rm /tmp/csgo.conf
}

bindLoop() {
	local keyName="${1}"
	local commandName="${2}"

	echo "bind ${keyName} ${commandName};" >> /tmp/csgo.conf

	#bindArray=($(printf "%s\n" "${bindArray[@]}" | shuf))

	for ((i=0; i<${#bindArray[@]}; i++)); do
		nextIndex=$(( (i+1) % ${#bindArray[@]} )) # Calculate the index of the next element, wrapping around at the end of the array
		echo "alias ${commandName}${i} \"say ${bindArray[i]}; alias ${commandName} ${commandName}${nextIndex}\";" >> /tmp/csgo.conf
	done

	echo "alias ${commandName} ${commandName}0;" >> /tmp/csgo.conf
}

cat > /home/c0rn3j/.var/app/com.valvesoftware.Steam/.local/share/Steam/steamapps/common/Counter-Strike Global Offensive/game/csgo/cfg/gamestate_integration_corn.cfg << EOF
"Console Sample v.1"
{
  "uri" "http://127.0.0.1:3000"
  "timeout" "5.0"
  "buffer"  "0.01"
  "throttle" "0.05"
  "heartbeat" "0.1"
  "output"
  {
    "precision_time" "3"
    "precision_position" "1"
    "precision_vector" "3"
  }
  "data"
  {
   "provider"               "1"      // general info about client being listened to: game name, appid, client steamid, etc.
   "map"                    "1"      // map, gamemode, and current match phase ('warmup', 'intermission', 'gameover', 'live') and current score
   "round"                  "1"      // round phase ('freezetime', 'over', 'live'), bomb state ('planted', 'exploded', 'defused'), and round winner (if any)
   "map_round_wins"         "1"      // history of round wins
   "player_id"              "1"      // player name, clan tag, observer slot (ie key to press to observe this player) and team
   "player_state"           "1"      // player state for this current round such as health, armor, kills this round, etc.
   "player_weapons"         "1"      // output equipped weapons.
   "player_match_stats"     "1"      // player stats this match such as kill, assists, score, deaths and MVPs
   "allgrenades"            "1"      // grenade effecttime, lifetime, owner, position, type, velocity
   "allplayers_id"          "1"      // the steam id of each player
   "allplayers_match_stats" "1"      // the scoreboard info for each player
   "allplayers_position"    "1"      // player_position but for each player
   "allplayers_state"       "1"      // the player_state for each player
   "allplayers_weapons"     "1"      // the player_weapons for each player
   "bomb"                   "1"      // location of the bomb, who's carrying it, dropped or not
   "phase_countdowns"       "1"      // time remaining in tenths of a second, which phase
   "player_position"        "1"      // forward direction, position for currently spectated player
  }
}
EOF

while IFS= read -r -d '' userDir; do
	userDirs+=("${userDir}")
	echo "Found user dir ${userDir}"
done < <(find ${localSteamPrefix}/userdata/ ${flatpakSteamPrefix}/userdata/ -maxdepth 1 -mindepth 1 -type d -print0)

# For each steam user directory create the one with CS GO ID
for userDir in "${userDirs[@]}"; do
	mkdir -p "${userDir}/730/local/cfg"
done

cat > /tmp/csgo.conf << EOF
// Config for server
sv_cheats 1
sv_infinite_ammo 1
ammo_grenade_limit_total 5
mp_warmup_end
mp_freezetime 0
mp_roundtime 60
mp_roundtime_defuse 60
sv_grenade_trajectory 1
sv_grenade_trajectory_time 10
sv_showimpacts 1
enable_skeleton_draw 1
mp_limitteams 0
mp_autoteambalance 0
mp_maxmoney 60000
mp_startmoney 60000
mp_buytime 9999
mp_buy_anywhere 1
mp_restartgame 1

// Press this once to get all grenades
bind "C" "give weapon_hegrenade;give weapon_flashbang;give weapon_smokegrenade;give weapon_molotov"

// Bind a key so you can fly around
bind "V" "noclip"

// Bind a key so you can fly around
bind "F" "bot_place"

mp_friendlyfire 1
ff_damage_reduction_grenade 1
ff_damage_reduction_bullets 1
ff_damage_reduction_other 1
ff_damage_reduction_grenade_self 1

mp_autokick 0 // no kick after 3 team kills

// Bot commands
bot_add_t
bot_add_ct
//bot_kick
bot_stop 1 // Make bots stand still
EOF
createConfigFile "nade"

cat > /tmp/csgo.conf << EOF
// Must be pasted in sections as the console has a char limit, ideally just use autoexec.cfg
// Lines containing // must not be pasted as linebreaks are ignored so the whole thing will be treated as a comment
// Enable Developer Console ~ in Settings Menu -> Game Settings

// To get rid of custom configs - rm -i ~/.local/share/Steam/userdata/*/730/local/cfg/config.cfg
// You can see the configs here - https://store.steampowered.com/account/remotestorageapp/?appid=730

// Max ping allowed when searching for a game, 25 is minimum
// Rest is crosshair settings
mm_dedicated_search_maxping 50;
cl_crosshairalpha 255;
cl_crosshaircolor 5;
cl_crosshaircolor_b 50;
cl_crosshaircolor_r 50;
cl_crosshaircolor_g 250;
cl_crosshairdot 0;
cl_crosshairgap -2;
cl_crosshairsize 1;
cl_crosshairstyle 4;
cl_crosshairusealpha 1;

// 4x misc crosshair settings
// T shaped crosshair
// Draw arrow over teammates heads even through walls
cl_crosshairthickness 0.5;
cl_fixedcrosshairgap -2;
cl_crosshair_outlinethickness 0;
cl_crosshair_drawoutline 1;
cl_crosshair_t 1;
cl_teamid_overhead_mode 2;

// Don't autoswitch weapons on pickup
// Disable autohelp(?)
// Disable showhelp(?)
// Disable instructor messages
// 3000 DPI + 4 game sens is the way to go
// Use V for mic
cl_autowepswitch 0;
cl_autohelp 0;
cl_showhelp 0;
gameinstructor_enable 0;
sensitivity 4;

// Close the buy menu on buying a single item;
// Press Home button during warmup to suicide
// No menu music
// No roundstart music
// No MVP music
// Jump on mouse wheel down for easier bhop
// Don't open buy menu on pressing E
closeonbuy 1;
bind home kill;
snd_menumusic_volume 0;
snd_roundstart_volume 0;
snd_mvp_volume 0;
bind mwheeldown +jump;
cl_use_opens_buy_menu 0;


// Set up inspect because my nade config unbinds it
bind F +lookatweapon
// Set up mic key
bind V +voicerecord
// Set up a permanent mic on key, disabled by using mic normally again
bind C "+voicerecord;+voicerecord";
// Set bomb noise to 50%, in case it was disabled previously
snd_tensecondwarning_volume 0.3;
// Or toogle it instead
//bind C toggle_mic;
//alias toggle_mic tm1;
//alias tm1 "+voicerecord;+voicerecord;alias toggle_mic tm2";
//alias tm2 "-voicerecord;-voicerecord;alias toggle_mic tm1";

// Default 500 is wasteful
fps_max 300
// Do not show build info in bottom right
r_show_build_info 0

bind pgdn "say $(date --iso-8601) Aimware loaded, kernel $(uname -r)| running on $(hostname)| uptime $(uptime -p), welcome back";
// In case you set this up as autoexec.cfg, print out a message in the console on load
echo "========================== Loaded autoexec v2! =========================="
EOF
bindArray=("oWo" "daddy" "uWu" "xoxo")
bindLoop "end" "uWuize"
createConfigFile "autoexec"

# Subtitles for the entire Shrek movie
bindArray=(
	'VILLAGER 1: Think it’s in there?'
	'VILLAGER 2: All right. Let’s get it!'
	'VILLAGER 1: Whoa. Hold on. Do you know what that thing can do to you?'
	'VILLAGER 3: Yeah, it’ll grind your bones for its bread.'
	'SHREK: Yes, well, actually, that would be a giant.'
	'SHREK: Now, ogres, oh they’re much worse. They’ll make a suit from your freshly peeled skin...'
	'VILLAGERS: No!'
	'SHREK: They’ll shave your liver. Squeeze the jelly from your eyes! Actually, it’s quite good on toast.'
	'VILLAGER 1: Back! Back, beast! Back! I warn ya!'
	'VILLAGER 1: Right...'
	'SHREK: (Whispering) This is the part where you run away.'
	'ALL: (Screaming!!!)'
	'SHREK: And stay out!'
	'SHREK: "Wanted. Fairytale creatures"?'
	'GUARD: All right. This one’s full. Take it away! Move it along. Come on! Get up!'
	'THE CAPTAIN: Next!'
	'GUARD: (Taking the witch’s broom) Give me that! Your flying days are over. (breaks the broom in half)'
	'THE CAPTAIN: That’s 20 pieces of silver for the witch. Next!'
	'VILLAGER: Lousy twenty pieces...'
	'GUARD: Get up! Come on!'
	'GUARD: Sit down there! Be quiet!'
	'LITTLE BEAR: (crying) This cage is too small.'
	'DONKEY: (To his owner) Please, don’t turn me in. I’ll never be stubborn again. I can change. Please! Give me another chance!'
	'OLD WOMAN: Oh, shut up. (Smacks Donkey)'
	'DONKEY: Oh!'
	'THE CAPTAIN: Next! What have you got?'
	'GEPPETTO: This little wooden puppet.'
	'PINOCCHIO: I’m not a puppet. I’m a real boy. (his nose grows)'
	'THE CAPTAIN: Five shillings for the possessed toy. Take it away.'
	'PINOCCHIO: Father, please! Don’t let them do this! Help me!'
	'THE CAPTAIN: Next! What have you got?'
	'OLD WOMAN: Well, I’ve got a talking donkey.'
	'THE CAPTAIN: Right. Well, that’s good for ten shillings, if you can prove it.'
	'OLD WOMAN: Oh, go ahead, little fella. (Donkey stays silent).'
	'THE CAPTAIN: Well?..'
	'OLD WOMAN: Oh, oh, he’s just...he’s just a little nervous. He’s really quite a chatterbox. (Smacks Donkey again) Talk, you boneheaded dolt, talk!'
	'THE CAPTAIN: That’s it. I’ve heard enough. Guards!'
	'OLD WOMAN: No, no, he talks! He does. (Moving Donkey’s lips) I can talk. I love to talk. I’m the talking-est damn thing you ever saw.'
	'THE CAPTAIN: Get her out of my sight.'
	'OLD WOMAN: No, no! I swear! Oh! He can talk!'
	'DONKEY: Hey! I can fly!'
	'PETER PAN: He can fly!'
	'THREE LITTLE PIGS: He can fly!'
	'THE CAPTAIN: He can talk?!'
	'DONKEY: Ha, ha! That’s right, fool! Now I’m a flying, talking donkey. You might have seen a housefly, maybe even a superfly but I bet you ain’t never seen a donkey fly. Ha, ha! (The pixie dust’s effects begin to wear off) Uh-oh. (He drops out of the air and hits the ground with a thud.)'
	'THE CAPTAIN: Seize him!'
	'GUARDS: He’s getting away! Get him! This way! Turn!'
	'THE CAPTAIN: You there. Ogre!'
	'SHREK: Aye?'
	'THE CAPTAIN: By the order of Lord Farquaad, I am authorized to place you both under arrest and...(Shrek slowly approaches the group of guards, the guards visibly frightened by him) transport you to... a designated...resettlement...facility?'
	'SHREK: Oh, really? You and what army? (Smiles)'
	'DONKEY: Can I say something to you? Listen, you was really, really, really somethin’ back here. Incredible!'
	'SHREK: Are you talkin’ to...(he turns around and Donkey is gone) me? (he turns back around and Donkey is right in front of him.) Whoa!'
	'DONKEY: Yes. I was talkin’ to you. Can I tell you that you that you was great back there? Man those guards! They thought they was all of that. Then you showed up and bam! They was trippin’ over themselves like babes in the woods. That really made me feel good to see that.'
	'SHREK: (Annoyed) Oh, that’s great. Really.'
	'DONKEY: Man, it’s good to be free.'
	'SHREK: Now, why don’t you go celebrate your freedom with your own friends? Hmm?'
	'DONKEY: But, uh, I don’t have any friends. And I’m not goin’ out there by myself. Hey, wait a minute! I got a great idea! I’ll stick with you. You’re a mean, green, fightin’ machine. Together we’ll scare the spit out of anybody that crosses us.'
	'DONKEY: Oh, wow! That was really scary. If you don’t mind me sayin’, if that don’t work, your breath certainly will get the job done, ’cause you definitely need some Tic Tacs or something, ’cause your breath stinks!'
	'DONKEY: Man, you almost burned the hair outta my nose, just like the time...(Shrek covers his mouth but Donkey continues to talk, so Shrek removes his hand.) ...then I ate some rotten berries. I had strong gases leaking out of my butt that day.'
	'SHREK: Why are you following me?'
	'DONKEY: I’ll tell you why. (Drops from the log. Singing) "’Cause I’m all alone, There’s no one here beside me, My problems have all gone, There’s no one to deride me, But you gotta have friends..."'
	'SHREK: Stop singing! (Picks up Donkey by his ears and tail) It’s no wonder you don’t have any friends (drops him).'
	'DONKEY: Wow. Only a true friend would be that truly honest.'
	'SHREK: Listen, little donkey. Take a look at me. What am I?'
	'DONKEY: (looks all the way up at Shrek) Uh...really tall?'
	'SHREK: No! I’m an ogre! You know, "Grab your torch and pitchforks." Doesn’t that bother you?'
	'DONKEY: (Shakes his head) Nope.'
	'SHREK: (Surprised) Really?'
	'DONKEY: Really, really.'
	'SHREK: Oh.'
	'DONKEY: Man, I like you. What’s your name?'
	'SHREK: Uh, Shrek.'
	'DONKEY: Shrek? Well, you know what I like about you, Shrek? You got that kind of "I-don’t-care-what-nobody-thinks-of-me" thing. I like that. I respect that, Shrek. You’re all right. (They come over a hill overlooking Shrek’s home.) Woo, look at that! Who’d want to live in place like that?'
	'SHREK: (Annoyed) That would be my home.'
	'DONKEY: Oh! And it is lovely! Just beautiful. You know you are quite a decorator. It’s amazing what you’ve done with such a modest budget. I like that boulder. That is a nice boulder. (Looks at Shrek’s "keep out" signs) I guess you don’t entertain much, do you?'
	'SHREK: I like my privacy.'
	'DONKEY: You know, I do too. That’s another thing we have in common. Like I hate it when you got somebody in your face. You’re trying to give them a hint and they won’t leave. And there’s that big awkward silence you know? (awkward silence) Can I stay with you?'
	'SHREK: Uh, what?'
	'DONKEY: Can I stay with you, please?'
	'SHREK: (sarcastically) Of course!'
	'DONKEY: Really?'
	'SHREK: No.'
	'DONKEY: Please! I don’t wanna go back there! You don’t know what it’s like to be considered a freak. (Donkey pushes Shrek up against the door) Well, maybe you do. But that’s why we gotta stick together. You gotta let me stay! Please! Please!'
	'SHREK: Okay! Okay! But one night only.'
	'DONKEY: Ah! Thank you! (he runs inside the hut)'
	'SHREK: Ah! What are you...no! (Donkey hops up onto a chair.) No!'
	'DONKEY: This is gonna be fun! We can stay up late, swappin’ manly stories, and in the mornin’... I’m makin’ waffles.'
	'SHREK: (Groans in frustration)'
	'DONKEY: Where do, uh, I sleep?'
	'SHREK: (irritated) Outside!'
	'DONKEY: Oh, well, I guess that’s cool. I mean, I don’t know you, and you don’t know me, so I guess outside is best, you know. Here I go. Good night. (Shrek slams the door, shutting Donkey outside) I mean, I do like the outdoors. I’m a donkey. I was born outside. I’ll just be sitting by myself outside, I guess, you know. By myself, outside. I’m all alone...there’s no one here beside me...'
	'SHREK: (to Donkey) I thought I told you to stay outside!'
	'DONKEY: (from the window) I am outside!'
	'MOUSE 1: Well, gents, it’s a far cry from the farm, but what choice do we have?'
	'MOUSE 2: It’s not home, but it’ll do just fine.'
	'GORDER: (bouncing on a slug) What a lovely bed.'
	'GORDER: I found some cheese. (bites into Shrek’s ear)'
	'SHREK: Ow! (tries to grab him)'
	'GORDER: Blah! Awful stuff. (jumps down to the table)'
	'BLIND MOUSE: Is that you, Gorder?'
	'GORDER: How did you know?'
	'SHREK: Enough! (he grabs all three mice) What are you doing in my house? (He gets bumped from behind and he drops the mice.) Hey!'
	'SHREK: Oh, no, no, no. Dead broad off the table! (pushes the coffin away)'
	'DWARF: Where are we supposed to put her? The bed’s taken.'
	'SHREK: Huh?'
	'BIG BAD WOLF: What?'
	'SHREK: I live in a swamp. I put up signs. I’m a terrifying ogre! What do I have to do get a little privacy?'
	'SHREK: Oh, no. No! No! (He dodges out the way of a group of witches flying on broomsticks)'
	'SHREK: What are you doing in my swamp?!!'
	'SHREK: All right, get out of here. All of you, move it! Come on! Let’s go! Hapaya! Hapaya! Hey!'
	'DWARVES: Hey! Quickly. Come on!'
	'SHREK: No, no! No, no. Not there! Not there!'
	'DONKEY: Hey, don’t look at me. I didn’t invite them.'
	'PINOCCHIO: Well gosh, no one invited us.'
	'SHREK: What?!'
	'PINOCCHIO: We were forced to come here!'
	'SHREK: By who?!'
	'LITTLE PIG: Lord Farquaad. He huffed and he puffed and he...signed an eviction notice.'
	'SHREK: (Sighs) Alright. Who knows where this... "Farquaad" guy is?'
	'DONKEY: Oh, I do. I know where he is!'
	'SHREK: Does anyone else know where to find him?'
	'SHREK: Anyone at all?'
	'DONKEY: Me! Me!'
	'SHREK: Anyone?'
	'DONKEY: (Jumping up and down) Oh! Oh, pick me! Oh, I know! I know! Me, me!'
	'SHREK: (sigh) Okay, fine. Attention all...fairy tale things. Do not get comfortable! Your welcome is officially worn out! In fact, I’m gonna see this guy Farquaad right now and get you all off my land and back where you came from!'
	'SHREK: Oh! (to Donkey) You! You’re comin’ with me.'
	'DONKEY: All right, that’s what I like to hear, man. Shrek and Donkey, two stalwart friends, off on a whirlwind big-city adventure. I love it!'
	'DONKEY: (singing) "On the road again...", sing it with me, Shrek!'
	'DONKEY: "I can’t wait to get on the road again."'
	'SHREK: What did I say about singing? (yanks the wreath off Donkey’s head)'
	'DONKEY: Can I whistle?'
	'SHREK: No.'
	'DONKEY: Can I hum it?'
	'SHREK: All right, hum it.'
	'FARQUAAD: (stepping forward) That’s enough. He’s ready to talk.'
	'FARQUAAD: (he picks up the Gingy’s severed legs and plays with them) Run, run, run, as fast as you can. You can’t catch me. I’m the gingerbread man!'
	'GINGY: You’re a monster!'
	'FARQUAAD: I’m not the monster here, you are. (throws one leg at Gingy) You and the rest of that fairy tale trash, poisoning my perfect world (crumbles his other leg into dust). Now, tell me! Where are the others?!'
	'GINGY: Eat me! (He spits milk into Farquaad’s eye.)'
	'FARQUAAD: I’ve tried to be fair to you creatures. Now my patience has reached its end! Tell me or I’ll...(he grabs one of Gingy’s gumdrop buttons)'
	'GINGY: No, no, not the buttons. Not my gumdrop buttons!'
	'FARQUAAD: All right then. Who’s hiding them?'
	'GINGY: Okay, I’ll tell you. Do you know the muffin man?'
	'FARQUAAD: The muffin man?'
	'GINGERBREAD MAN: The muffin man.'
	'FARQUAAD: Yes, I know the muffin man, who lives on Drury Lane?'
	'GINGERBREAD MAN: Well, she’s married to the muffin man.'
	'FARQUAAD: (Shocked) The muffin man?'
	'GINGERBREAD MAN: The muffin man!'
	'FARQUAAD: She’s married to the muffin man...'
	'CAPTAIN: My lord! We’ve found it.'
	'FARQUAAD: Then what are you waiting for? Bring it in!'
	'GINGERBREAD MAN: Ohhhh...'
	'FARQUAAD: Magic mirror...'
	'GINGERBREAD MAN: Don’t tell him anything! (Farquaad smacks him off the table and a trash can. ) No!'
	'FARQUAAD: Evening. Mirror, mirror on the wall, is this not the most perfect kingdom of them all?'
	'MIRROR: Well, technically you’re not a king.'
	'FARQUAAD: Uh, Thelonius. (Thelonius holds up a hand mirror and smashes it with his fist.) You were saying?'
	'MIRROR: What I mean is you’re not a king yet. But you can become one. All you have to do is marry a princess.'
	'FARQUAAD: Go on.'
	'MIRROR: (chuckles nervously) So, just sit back and relax, my lord, because it’s time for you to meet today’s eligible bachelorettes. And...here they are!'
	'MIRROR: Bachelorette number one is a mentally abused shut-in from a kingdom far, far away. She likes sushi and hot tubbing anytime! Her hobbies include cooking and cleaning for her two evil sisters. Please welcome...Cinderella!'
	'MIRROR: Bachelorette number two is a cape-wearing girl from the land of fancy. Although she lives with seven other men, she’s not easy.'
	'MIRROR: Just kiss her dead, frozen lips and find out what a live wire she is.! Come on, give it up for Snow White!'
	'MIRROR: And last, but certainly not last, bachelorette number three is a fiery redhead from a dragon-guarded castle surrounded by hot boiling lava!'
	'MIRROR: But don’t let that cool you off. She’s a loaded pistol who likes piña coladas and getting caught in the rain. Yours for the rescuing, Princess Fiona!'
	'MIRROR: So, will it be: bachelorette number one, bachelorette number two, or bachelorette number three?'
	'GUARDS: Two! Two! Three! Three! Two! Two! Three!'
	'FARQUAAD: (To himself) Two? Three? One? Three?'
	'THELONIUS: Three! (holds up 2 fingers) Pick number three, my lord!'
	'FARQUAAD: Okay, okay, uh... number three!'
	'MIRROR: Lord Farquaad, you’ve chosen Princess Fiona.'
	'FARQUAAD: Princess...Fiona...she’s perfect. All I have to do is just find someone who can go...'
	'MIRROR: But I probably should mention the little thing that happens at night.'
	'FARQUAAD: I’ll do it.'
	'MIRROR: Yes, but after sunset...'
	'FARQUAAD: Silence! I will make this Princess Fiona my queen, and Duloc will finally have the perfect king! Captain, assemble your finest men. We’re going to have a tournament! (smiles evilly)'
	'DONKEY: But that’s it. That’s it right there. That’s Duloc. I told ya I’d find it.'
	'SHREK: So, that must be Lord Farquaad’s castle.'
	'DONKEY: Uh-huh. That’s the place.'
	'SHREK: Do you think maybe he’s compensating for something?'
	'DONKEY: Hey, wait. Wait up, Shrek.'
	'MAN: Hurry, darling. We’re late. Hurry!'
	'SHREK: Hey, you!'
	'SHREK: Wait a second. Look, I’m not gonna eat you. I just-- I just --'
	'SHREK: It’s quiet. Too quiet. Where is everybody?'
	'DONKEY: Hey, look at this!'
	'WOODEN PEOPLE: Welcome to Duloc such a perfect town / Here we have some rules let us lay them down / Don’t make waves, stay in line and we’ll get along fine / Duloc is perfect place / Please keep off of the grass / Shine your shoes, wipe your... face / Duloc is, Duloc is / Duloc is perfect place.'
	'DONKEY: Wow! Let’s do that again!'
	'SHREK: No. No. No, no, no!...No.'
	'FARQUAAD: Brave knights! You are the best and brightest in all the land, and today one of you shall prove himself better and brighter than all the rest.'
	'SHREK: All right, you’re going the right way for a smacked bottom.'
	'DONKEY: Sorry about that.'
	'FARQUAAD: That champion shall have the honor-- no, no -- the privilege to go forth and rescue the lovely Princess Fiona from the fiery keep of the dragon. If for any reason the winner is unsuccessful, the first runner-up will take his place. And so on and so forth. Some of you may die, but it’s a sacrifice I am willing to make.'
	'FARQUAAD: Let the tournament begin!'
	'FARQUAAD: Oh! What is that? It’s hideous!'
	'SHREK: Ah, that’s not very nice (Looks at Donkey and then back at Farquaad). It’s just a donkey'
	'FARQUAAD: Indeed. Knights, new plan! The one who kills the ogre will be named champion! Have at him!'
	'CROWD: Get him!'
	'SHREK: Oh, hey! Now come on! Hang on now. (He bumps into a table, noticing mugs of beer)'
	'CROWD: Go ahead! Get him!'
	'SHREK: (holds up a mug of beer) Can’t we just settle this over a pint?'
	'CROWD: Kill the beast!'
	'SHREK: No? All right then. (drinks the mug in one gulp) Come on!'
	'DONKEY: Hey, Shrek, tag me! Tag me!'
	'SHREK: Yeah!'
	'WOMAN: The chair! Give him the chair!'
	'SHREK: Oh, yeah! Ah! Ah! Thank you! Thank you very much! I’m here till Thursday. Try the veal! Ha, ha! (laughs)'
	'GUARD: Shall I give the order, sir?'
	'FARQUAAD: No, I have a better idea. People of Duloc! I give you our champion!'
	'SHREK: What?'
	'FARQUAAD: Congratulations, ogre. You’ve won the honor of embarking on a great and noble quest.'
	'SHREK: Quest? I’m already on a quest. A quest to get my swamp back.'
	'FARQUAAD: Your swamp?'
	'SHREK: Yeah, my swamp! Where you dumped those fairy tale creatures!'
	'FARQUAAD: Indeed. All right, ogre. I’ll make you a deal. Go on this quest for me, and I’ll give you your swamp back.'
	'SHREK: Exactly the way it was?'
	'FARQUAAD: Down to the last slime-covered toadstool.'
	'SHREK: And the squatters?'
	'FARQUAAD: As good as gone.'
	'SHREK: What kind of quest?'
	'DONKEY: Let me get this straight. You’re gonna go fight a dragon and rescue a princess just so Farquaad will give you back a swamp which you only don’t have because he filled it full of freaks in the first place. Is that about right?'
	'SHREK: You know, maybe there’s a good reason donkeys shouldn’t talk.'
	'DONKEY: I don’t get it. Why don’t you just pull some of that ogre stuff on him? Throttle him, lay siege to his fortress, grinds his bones to make your bread, the whole ogre trip.'
	'SHREK: Oh, I know what. Maybe I could have decapitated an entire village and put their heads on a pike, gotten a knife, cut open their spleen and drink their fluids. Does that sound good to you?'
	'DONKEY: Uh, no, not really, no.'
	'SHREK: For your information, there’s a lot more to ogres than people think.'
	'DONKEY: Example?'
	'SHREK: Example? Okay, um, ogres are like onions. (he holds out his onion)'
	'DONKEY: (sniffs the onion) They stink?'
	'SHREK: Yes - - No!'
	'DONKEY: They make you cry?'
	'SHREK: No!'
	'DONKEY: Oh, you leave them in the sun, they get all brown, start sproutin’ little white hairs.'
	'SHREK: No! Layers! Onions have layers. Ogres have layers! Onions have layers. You get it? We both have layers. (he throws away the onion and walks off)'
	'DONKEY: (trailing after Shrek) Oh, you both have layers. Oh. {Sniffs} You know, not everybody likes onions. Cake! Everybody loves cakes! Cakes have layers.'
	'SHREK: I don’t care... what everyone likes. Ogres are not like cakes. (Walks passed Donkey)'
	'DONKEY: You know what else everybody likes? Parfaits. Have you ever met a person, you say, "Let’s get some parfait," they say, "Hell no, I don’t like no parfait"? Parfaits are delicious.'
	'SHREK: (Yelling) No! You dense, irritating, miniature beast of burden! Ogres are like onions! End of story. Bye-bye. See ya later.'
	'DONKEY: Parfaits may be the most delicious thing on the whole damn planet.'
	'SHREK: You know, I think I preferred your humming.'
	'DONKEY: Do you have a tissue or something? I’m making a mess. Just the word parfait makes me start slobbering.'
	'DONKEY: (sniffs) Ohh! Shrek! Did you do that? You gotta warn somebody before you just crack one off. My mouth was open and everything.'
	'SHREK: Believe me, Donkey, if it was me, you’d be dead. (sniffs) It’s brimstone. We must be getting close.'
	'DONKEY: Yeah, right, brimstone. Don’t be talking about it’s the brimstone. I know what I smell. It wasn’t no brimstone. It didn’t come off no stone neither.'
	'SHREK: Sure, it’s big enough, but look at the location. (laughs)'
	'DONKEY: (chuckes along nervously) Uh, Shrek? Uh, remember when you said that ogres have layers?'
	'SHREK: Oh, aye.'
	'DONKEY: Well, I have a bit of a confession to make (Gasps, seeing the skeleton of a horse). Donkeys don’t have layers. We wear our fear right out there on our sleeves.'
	'SHREK: Wait a second. Donkeys don’t have sleeves.'
	'DONKEY: You know what I mean.'
	'SHREK: Oh you can’t tell me you’re afraid of heights.'
	'DONKEY: No, I’m just a little uncomfortable about being on a rickety bridge over a boiling lake of lava!'
	'SHREK: Come on, Donkey. I’m right here beside ya, okay? For emotional support. We’ll just tackle this thing together one little baby step at a time. DONKEY: Really?'
	'SHREK: Really, really.'
	'DONKEY: Okay, that makes me feel so much better.'
	'SHREK: Just keep moving. And don’t look down.'
	'DONKEY: (Nervously to himself) Okay, don’t look down. Don’t look down. Don’t look down. Keep on moving. Don’t look down.'
	'DONKEY: Shrek! I’m lookin’ down! Oh, God, I can’t do this! Just let me off, please!'
	'SHREK: But you’re already halfway.'
	'DONKEY: But I know that half is safe!'
	'SHREK: Okay, fine. I don’t have time for this. You go back.'
	'DONKEY: Shrek, no! Wait!'
	'SHREK: Just, Donkey - - Let’s have a dance then, shall me? (bounces and sways the bridge)'
	'DONKEY: Don’t do that!'
	'SHREK: Oh, I’m sorry. Do what? Oh, this? (bounces the bridge again)'
	'DONKEY: Yes, that!'
	'SHREK: Yes? Yes, do it. Okay. (continues to bounce and sway as he backs Donkey across the bridge)'
	'DONKEY: No, Shrek! No! Stop it!'
	'SHREK: You said do it! I’m doin’ it.'
	'DONKEY: I’m gonna die. I’m gonna die. Shrek, I’m gonna die. (steps onto solid ground) Oh!'
	'SHREK: That’ll do, Donkey. That’ll do. (walks towards the castle)'
	'DONKEY: Cool. So where is this fire-breathing pain-in-the-neck anyway?'
	'SHREK: Inside, waiting for us to rescue her. (chuckles)'
	'DONKEY: I was talkin’ about the dragon, Shrek.'
	'DONKEY: You afraid?'
	'SHREK: No. But...SHHHHHH. (Shushes Donkey)'
	'DONKEY: Oh, good. Me neither. (Get spooked and gasps) ’Cause there’s nothin’ wrong with bein’ afraid. Fear’s a sensible response to an unfamiliar situation. Unfamiliar dangerous situation, I might add. With a dragon that breathes fire and eats knights and breathes fire. It sure doesn’t mean you’re a coward if you’re a little scared. I sure as heck ain’t no coward. I know that.'
	'SHREK: Donkey, two things, okay? Shut. Up. Now go over there and see if you can find any stairs (Grabs the helmet and puts it on).'
	'DONKEY: Stairs? I thought we was lookin’ for the princess.'
	'SHREK: (Picking up pieces of armor) The princess will be up the stairs in the highest room in the tallest tower.'
	'DONKEY: What makes you think she’ll be there?'
	'SHREK: I read it in a book once. (walks off)'
	'DONKEY: Cool. You handle the dragon. I’ll handle the stairs. I’ll find those stairs. I’ll whip their butt too. Those stairs won’t know which way they’re goin’.'
	'DONKEY: I’m gonna take drastic steps. Kick it to the curb. Don’t mess with me. I’m the stair master. I’ve mastered the stairs. I wish I had a step right here. I’d step all over it.'
	'SHREK: Oh! At least we know where the princess is, but where’s the...'
	'DONKEY: Dragon! Ahhhhhh!'
	'SHREK: Donkey, look out!'
	'SHREK: Got ya!'
	'DONKEY: No. Oh, no, No! (the dragon growls) Oh, what large teeth you have! (the dragon roars) I mean white, sparkling teeth!'
	'DONKEY: I know you probably hear this all time from your food, but you must bleach, ’cause that is one dazzling smile you got there. Do I detect a hint of minty freshness?'
	'DONKEY: And you know what else? You know what else? You’re-- You’re--'
	'DONKEY: --a girl dragon! Oh, sure! I mean, of course you’re a girl dragon. You’re just reeking of feminine beauty.'
	'DONKEY: What’s the matter with you? You got something in your eye?'
	'DONKEY: Ohh. Oh. Oh. Man, I’d really love to stay, but you know, I’m, uh...(coughs) I’m an asthmatic, and I don’t know if it’d work out if you’re gonna blow smoke rings and stuff. Shrek!'
	'DONKEY: No! Shrek! Shrek! Shrek!'
	'FIONA: Wha...Wha...'
	'SHREK: Wake up!'
	'FIONA: What?!'
	'SHREK: Are you Princess Fiona?'
	'FIONA: I am... (smiling) awaiting a knight so bold as to rescue me.'
	'SHREK: Ah, that’s nice. Now let’s go!'
	'FIONA: But wait, Sir Knight! This be-ith our first meeting. Should it not be a wonderful, romantic moment?'
	'SHREK: Yeah, sorry, lady. There’s no time.'
	'FIONA: Hey, wait. What are you doing? You know, you should sweep me off my feet out yonder window and down a rope onto your valiant steed.'
	'SHREK: You’ve had a lot of time to plan this, haven’t you?'
	'FIONA: Mm-hmm.'
	'FIONA: But we have to savor this moment! You could recite an epic poem for me.'
	'FIONA: A ballad? A sonnet! A limerick? Or something!'
	'SHREK: I don’t think so.'
	'FIONA: Well, can I at least know the name of my champion?'
	'SHREK: Uh, Shrek.'
	'FIONA: Sir Shrek.'
	'FIONA: I pray that you take this favour as a token of my gratitude.'
	'SHREK: Thanks!'
	'FIONA: You didn’t slay the dragon?!'
	'SHREK: It’s on my to-do list, now come on!'
	'FIONA: But this isn’t right! You were meant to charge in, sword drawn, banner flying. That’s what all the other knights did!'
	'SHREK: Yeah, right before they burst into flames!'
	'FIONA: That’s not the point! Ugh!'
	'FIONA: Wait--where are you going? The exit’s over there!'
	'SHREK: Well, I have to save my ass.'
	'FIONA: Ugh. What kind of knight are you?'
	'SHREK: One of a kind.'
	'DONKEY: Slow down. Slow down, baby, please. Look I believe it’s healthy to get to know someone over a long period of time. Just, just call me old-fashioned. I don’t want to rush into a... a physical relationship. I’m not...not emotionally ready for a commitment of, uh, this, uh - - "magnitude" really is the word I’m looking for. Magnitude.'
	'DONKEY: Hey, that is unwanted physical contact. Hey, what are you doing?'
	'DONKEY: Okay, okay. Let’s just back up a little and take this one step at a time. I mean we really should get to know each other first, you know, as friends or maybe even pen pals. Y’know cause I’m on the road a lot, but I just love receiving cards to read --'
	'DONKEY: Oh y’know I’d, I’d really love to stay, but -- (Dragon tugs at Donkey’s tail with her mouth)'
	'DONKEY: Hey. hey don’t do that! That’s my tail! That’s my personal tail. You’re gonna tear it off. I don’t give permission to-- hey! What are you gonna do with that?'
	'DONKEY: Hey, now. No way. No! No! No, no! No. No, no, no. No! Oh!'
	'DONKEY: Hi, Princess!'
	'FIONA: It talks!'
	'SHREK: Yeah, it’s getting him to shut up that’s the trick.'
	'SHREK: Okay, you two, head for the exit! (setting down Donkey and Fiona) I’ll take care of the dragon.'
	'SHREK: Run!'
	'FIONA: You did it! You rescued me! You’re amazing.'
	'FIONA: You’re -- you’re wonderful. You’re...'
	'FIONA: A little unorthodox I’ll admit. But...thy deed is great, and thine heart is pure. I am eternally in your debt.'
	'DONKEY: Ahem...'
	'FIONA: And where would a brave knight be without his noble steed?'
	'DONKEY: I hope you heard that. She called me a noble steed. She thinks I’m a steed.'
	'FIONA: The battle is won. You may remove your helmet, good Sir Knight.'
	'SHREK: Uh, no.'
	'FIONA: Why not?'
	'SHREK: I...I have helmet hair.'
	'FIONA: Please. I would’st look upon the face of my rescuer.'
	'SHREK: Oh, no, you wouldn’t -- st.'
	'FIONA: But, how will you kiss me?'
	'SHREK: What?'
	'SHREK: (to Donkey) That wasn’t in the job description.'
	'DONKEY: Maybe it’s a perk! (Suggestively raises his eyebrows)'
	'FIONA: No, it’s destiny. Oh, you must know how it goes: A princess locked in a tower and beset by a dragon is rescued by a brave knight, and then they share true love’s first kiss.'
	'DONKEY: Hmm? With Shrek? You think --who, whoa, wait a sec. You think that Shrek is your true love?'
	'FIONA: Well...yes.'
	'DONKEY: You think Shrek is your true love!'
	'FIONA: (Annoyed) What is so funny?'
	'SHREK: Let’s just say I’m not your type, okay?'
	'FIONA: Of course, you are. You’re my rescuer. Now -- now remove your helmet.'
	'SHREK: Look. I really don’t think this is a good idea.'
	'FIONA: Just take off the helmet.'
	'SHREK: I’m not going to.'
	'FIONA: Take it off.'
	'SHREK: No!'
	'FIONA: NOW!'
	'SHREK: Okay! Easy! As you command,,,your Highness.'
	'FIONA: You’re...an ogre.'
	'SHREK: Oh, you were expecting Prince Charming?'
	'FIONA: Well --yes, actually! Oh, no. This is all wrong. You’re not supposed to be an ogre! (walks off)'
	'SHREK: Princess, I was sent to rescue you by Lord Farquaad, okay? He’s the one who wants to marry you.'
	'FIONA: Well then why didn’t he come rescue me?'
	'SHREK: Good question. You should ask him that when we get there.'
	'FIONA: But I have to be rescued by my true love! Not by some ogre and hi...hi...his pet.'
	'DONKEY: Well, so much for noble steed.'
	'SHREK: Look princess you’re not making my job any easier.'
	'FIONA: I’m sorry, but your job is not my problem. You can tell Lord Farquaad that if he wants to rescue me properly, I’ll be waiting for him right here.'
	'SHREK: Hey! I’m no one’s messenger boy, all right? (Advancing toward her) I’m a delivery boy.'
	'FIONA: You wouldn’t dare.'
	'FIONA: Agghh! Put me down! Aggghh!'
	'SHREK: You comin’, Donkey?'
	'DONKEY: Oh, yep! I’m right behind ya.'
	'FIONA: Put me down, or you will suffer the consequences! This is not dignified! Put me down!'
	'DONKEY: Okay, so here’s another question. Say there’s a woman that digs you, right, but you don’t really like her that way. How do you let her down real easy so her feelings aren’t hurt, but you don’t get burned to a crisp and eaten? How do you do that?'
	'FIONA: You just tell her she’s not your true love. Everyone knows what happens when you find your...'
	'FIONA: Hey! The sooner we get to Duloc the better.'
	'DONKEY: Oh you’re gonna love it there, Princess. It’s beautiful!'
	'FIONA: And what of my groom-to-be? Lord Farquaad? What’s he like?'
	'SHREK: Let me put it this way, princess.'
	'SHREK: Men of Farquaad’s stature are in...short supply.'
	'DONKEY: I dunno, Shrek. There are those who think...little of him.'
	'FIONA: Stop it. Stop it, both of you. You’re just jealous that you can never measure up to a great ruler like Lord Farquaad.'
	'SHREK: Yeah, well, maybe you’re right, princess. But I’ll let you do the...measuring...when you see him tomorrow.'
	'FIONA: Tomorrow?'
	'FIONA: It’ll take that long? Shouldn’t we stop to make camp?'
	'SHREK: No, that’ll take longer. We can keep going.'
	'FIONA: But there’s....robbers in the woods.'
	'DONKEY: Whoa! Time out, Shrek! Camp is definitely starting to sound good.'
	'SHREK: Hey, come on. I’m scarier than anything we’re going to see in this forest.'
	'FIONA: I need to find somewhere to camp-now!'
	'SHREK: Hey! Over here!'
	'DONKEY: Shrek, we can do better than that. I don’t think this is fit for a princess.'
	'FIONA: No, no, it’s perfect. It just needs a few homey touches.'
	'SHREK: Homey touches? Like what?'
	'FIONA: A door. Well, gentlemen, I bid thee good night.'
	'DONKEY: You want me to read you a bedtime story? Cause I will.'
	'FIONA: I said good night!'
	'DONKEY: Shrek, what are you doing?!'
	'SHREK: (laughs) I just--you know - - Oh, come on. I was just kidding.'
	'SHREK: And, uh, that one, that’s Throwback, the only ogre to ever spit over three wheat fields.'
	'DONKEY: Right. Yeah. Hey, can you tell my future from these stars?'
	'SHREK: The stars don’t tell the future, Donkey. They tell stories. Look, there’s Bloodnut the Flatulent. You can guess what he’s famous for. (chuckles)'
	'DONKEY: Alright now I know you’re making this up.'
	'SHREK: No, look.'
	'SHREK: There he is, and there’s the group of hunters running away from his stench.'
	'DONKEY: Man that ain’t nothin’ but a bunch of little dots.'
	'SHREK: You know, Donkey, sometimes things are more than they appear. Hmm?'
	'SHREK: Forget it.'
	'DONKEY: Hey, Shrek, what we gonna do when we get our swamp anyway?'
	'SHREK: Our swamp?'
	'DONKEY: You know, when we’re through rescuing the princess and all that stuff.'
	'SHREK: We? Donkey, there’s no we. There’s no our. There’s just me and my swamp. And the first thing I’m gonna do is build a ten-foot wall around my land.'
	'DONKEY: You cut me deep, Shrek. You cut me real deep just now.'
	'DONKEY: You know what I think? I think this whole wall thing is just a way to keep somebody out.'
	'SHREK: No, do ya think?'
	'DONKEY: Are you hidin’ something?'
	'SHREK: Never mind, Donkey.'
	'DONKEY: Oh, this is another one of those onion things, isn’t it?'
	'SHREK: No, this is one of those "drop it and leave it alone" things!'
	'DONKEY: Why don’t you want to talk about it?'
	'SHREK: Why do you want to talk about it? (turns)'
	'DONKEY: Why are you blocking?'
	'SHREK: I’m not blocking! (turns)'
	'DONKEY: Oh, yes, you are.'
	'SHREK: Donkey, I’m warning you...'
	'DONKEY: Who you trying to keep out?'
	'SHREK: Everyone! Okay?!'
	'DONKEY: Oh, now we’re gettin’ somewhere.'
	'SHREK: Oh! For the love of Pete!'
	'DONKEY: Hey what’s your problem Shrek? What you got against the whole world anyway, huh?'
	'SHREK: Look, I’m not the one with the problem, okay? It’s the world that seems to have a problem with me. People take one look at me and go "Aah! Help! Run! A big, stupid, ugly ogre!" They judge me before they even know me. That’s why I’m better off alone.'
	'DONKEY: You know what? When we met, I didn’t think you was just a big, stupid, ugly ogre.'
	'SHREK: Yeah, I know.'
	'DONKEY: So, uh, are there any donkeys up there?'
	'SHREK: Well, there’s, um, Gabby...the Small...and Annoying.'
	'DONKEY: Okay, okay, I see it now. The big shiny one, right there. That one there?'
	'SHREK: That’s the moon.'
	'DONKEY: Oh, okay.'
	'FARQUAAD: Again, show me again.'
	'FIONA: Mirror, mirror, show her to me. Show me the princess.'
	'MIRROR: Hmph.'
	'FARQUAAD: Ah...perfect.'
	'DONKEY: Mmm, yeah, you know I like it like that. Come on, baby. I said I like it...'
	'SHREK: Donkey, wake up. (shakes him)'
	'DONKEY: Huh? What?'
	'SHREK: Wake up.'
	'DONKEY: What? (stretches and yawns)'
	'FIONA: Good morning. Uhmm... how do you like your eggs?'
	'DONKEY: Oh, good morning, Princess!'
	'SHREK: What’s all this about?'
	'FIONA: You know, we kind of got off to a bad start yesterday and I wanted to make it up to you. I mean, after all, you did rescue me.'
	'SHREK: Uh, thanks.'
	'FIONA: Well, eat up. We’ve got a big day ahead of us.'
	'DONKEY: Shrek!'
	'SHREK: What? It’s a compliment. Better out than in, I always say. (laughs)'
	'DONKEY: Well, it’s no way to behave in front of a princess!'
	'FIONA: Thanks.'
	'DONKEY: She’s as nasty as you are.'
	'SHREK: (chuckles) You know, you’re not exactly what I expected.'
	'FIONA: Well, maybe you shouldn’t judge people before you get to know them.'
	'UNKNOWN: La liberte! Hey!'
	'SHREK: Princess!'
	'FIONA: Oh! Wait wait--what are you doing?!'
	'MONSIEUR HOOD: Be still, mon cherie, for I am you savior! And I am rescuing you from this green...'
	'MONSIEUR HOOD: ...beast.'
	'SHREK: Hey! That’s my princess! Go find you own!'
	'MONSIEUR HOOD: Please, monster! Can’t you see I’m a little busy here?'
	'FIONA: Look, pal, I don’t know who you think you are!'
	'MONSIEUR HOOD: Oh! Of course! Oh, how rude. Please let me introduce myself.'
	'MONSIEUR HOOD: Oh, Merry Men! (laughs)'
	'MERRYMEN: Ta, dah, dah, dah, whoo.'
	'MONSIEUR HOOD: I steal from the rich and give to the needy.'
	'MERRYMAN: He takes a wee percentage,'
	'MONSIEUR HOOD: But I’m not greedy. I rescue pretty damsels, man, I’m good.'
	'MERRYMEN: What a guy, Monsieur Hood.'
	'MONSIEUR HOOD: Break it down. I like an honest fight and a saucy little maid...'
	'MERRYMEN: What he’s basically saying is he likes to get...'
	'MONSIEUR HOOD: Paid! So...When an ogre in the bush grabs a lady by the tush. That’s bad.'
	'MERRYMEN: That’s bad. That’s bad. That’s bad!'
	'MONSIEUR HOOD: When a beauty’s with a beast it makes me awfully mad!'
	'MERRYMEN: He’s mad, he’s really, really mad!'
	'Fiona, still up in the tree, looks down. Her expression changes from confusion to horror as Monsieur Hood sings the last line:'
	'MONSIEUR HOOD: I’ll take my blade and ram it through your heart, keep your eyes on me, boys ’cause I’m about to start...'
	'FIONA: Man, that was annoying!'
	'MERRYMAN: Why, you little--'
	'FIONA: Uh, shall we?'
	'SHREK: Hold the phone.'
	'SHREK: Oh! Whoa, whoa, whoa. Hold on now. Where did that come from?'
	'FIONA: What?'
	'SHREK: That! Back there. That was amazing! Where did you learn that?'
	'FIONA: Well...(laughs) when one lives alone, uh, one has to learn these things in case there’s a...there’s an arrow in your butt!'
	'SHREK: What? Oh, would you look at that?'
	'FIONA: Oh, no. This is all my fault. I’m so sorry.'
	'DONKEY: Why? What’s wrong?'
	'FIONA: Shrek’s hurt.'
	'DONKEY: Shrek’s hurt! Shrek’s hurt?!'
	'DONKEY: Oh, no, Shrek’s gonna die!'
	'SHREK: Donkey, I’m okay.'
	'DONKEY: You can’t do this to me, Shrek. I’m too young for you to die! Keep your legs elevated! Turn your head and cough! Does anyone know the Heimlich?!'
	'FIONA: Donkey! Calm down! If you want to help Shrek, run into the woods and find me a blue flower with red thorns.'
	'DONKEY: Blue flower, red thorns. Okay, I’m on it. Blue flower, red thorns. Don’t die Shrek.'
	'DONKEY: And if you see a long tunnel, stay away from the light!'
	'SHREK & FIONA: Donkey!'
	'DONKEY: Oh, yeah. Right. Blue flower, red thorns. Blue flower, red thorns...'
	'SHREK: What are the flowers for?'
	'FIONA: For getting rid of Donkey.'
	'SHREK: Ah...'
	'FIONA: Now you hold still, and I’ll yank this thing out.'
	'SHREK: Ow! Hey! Easy with the yankin’!'
	'FIONA: I’m sorry, but it has to come out.'
	'SHREK: No, it’s tender.'
	'FIONA: Now, hold on.'
	'SHREK: What you’re doing is the opposite of help.'
	'FIONA: Don’t move.'
	'SHREK: Look, time out.'
	'FIONA: Would you...'
	'FIONA: Okay. What do you propose we do?'
	'DONKEY: Blue flower, red thorns. Blue flower, red thorns. Blue flower, red thorns. This would be so much easier if I wasn’t color-blind! Blue flower, red thorns.'
	'SHREK: Owww!'
	'DONKEY: Hold on, Shrek! I’m comin’!'
	'SHREK: Ow! Not good.'
	'FIONA: Okay. N--Okay. I can nearly see it...It’s just about...'
	'SHREK: Ow! Ohh!'
	'DONKEY: Ahem.'
	'SHREK: Nothing happened.'
	'SHREK: We were just, uh...'
	'DONKEY: Look, if you wanted to be alone, all you had to do was ask, okay?'
	'SHREK: Uggghhh!'
	'SHREK: Ow!'
	'DONKEY: Hey, what’s that? (chuckling) That’s...is that blood?'
	'SHREK: There it is, princess. Your future awaits you.'
	'FIONA: That’s Duloc?'
	'DONKEY: Yeah, I know. You know, Shrek thinks Lord Farquaad’s compensating for something, which I think means he has a really...'
	'DONKEY: Oww!'
	'SHREK: Um, I, uh-- I guess we better move on.'
	'FIONA: Sure. But, Shrek? I’m-- I’m worried about Donkey.'
	'SHREK: What?'
	'FIONA: I mean, look at him. He, he doesn’t look so good.'
	'DONKEY: What are you talking about? I’m fine.'
	'FIONA: Well that’s what they always say and then...then...then the next thing you know, you’re on your back. Dead.'
	'SHREK: You know, she’s right. You look awful. Do you want to sit down?'
	'FIONA: Uh, you know, I’ll make you some tea.'
	'DONKEY: I didn’t want to say nothin’, but I got this twinge in my neck, and when I turn my head like this, look.'
	'DONKEY: Ow! See?'
	'SHREK: Who’s hungry? I’ll find us some dinner.'
	'FIONA: I’ll get the firewood.'
	'DONKEY: Hey, where you goin’? Oh, man, I can’t feel my toes! (looks down and yelps) I don’t have any toes! I think I need a hug.'
	'FIONA: Mmm. This is good. This is really good. What is this?'
	'SHREK: Uh, weed rat. Rotisserie style.'
	'FIONA: No kidding. Well, this is delicious.'
	'SHREK: Well, they’re also great in stews. Now, I don’t mean to brag, but I make a mean weed rat stew.'
	'FIONA: I guess I’ll be dining a little differently tomorrow night.'
	'SHREK: Maybe you can come visit me in the swamp sometime. I’ll cook all kind of stuff for you. Swamp toad soup, fish eye tartare -- you name it.'
	'FIONA: Hmmm, I’d like that.'
	'SHREK: Um...princess?'
	'FIONA: Yes...Shrek?'
	'SHREK: I, um, I was wondering...are you...(sighs) Are you gonna eat that?'
	'DONKEY: Man, isn’t this romantic? Just look at that sunset.'
	'FIONA: Sunset?! Oh, no! I mean, it’s late. I-It’s very late.'
	'SHREK: What?'
	'DONKEY: Wait a minute. I see what’s goin’ on here.'
	'DONKEY: You’re afraid of the dark, aren’t you?'
	'FIONA: Yes! Yes, that’s it. I’m terrified. You know, I’d better go inside.'
	'DONKEY: Don’t feel bad, Princess. I used to be afraid of the dark, too, until -- Hey, no, wait. I’m still afraid of the dark.'
	'FIONA: Good night.'
	'SHREK: Good night.'
	'DONKEY: Ohh! Now I really see what’s goin’ on here.'
	'SHREK: Oh, what are you talkin’ about?’'
	'DONKEY: I don’t even wanna hear it. Look, I’m an animal, and I got instincts. And I know you two were diggin’ on each other. I could feel it.'
	'SHREK: You’re crazy. I’m just bringing her back to Farquaad.'
	'DONKEY: Oh, come on, Shrek. Wake up and smell the pheromones. Just go on in and tell her how you feel.'
	'SHREK: I--there’s nothing to tell. Besides, even if I did tell her that, well, you know-- and I’m not sayin’ I do, ’cause I don’t -- she’s a princess, and I’m...'
	'DONKEY: An ogre?'
	'SHREK: Yeah. An ogre.'
	'DONKEY: Hey, where you goin’?'
	'SHREK: To get...more firewood.'
	'DONKEY: Princess? Princess Fiona? Princess, where are you?'
	'DONKEY: Princess?'
	'DONKEY: It’s very spooky in here. I ain’t playing no games.'
	'DONKEY: Aah!'
	'FIONA: No, no!'
	'DONKEY: No, help!'
	'FIONA: Shh!'
	'DONKEY: Shrek! Shrek! Shrek!'
	'FIONA: No, it’s okay! It’s okay!'
	'DONKEY: What did you do with the princess?!'
	'FIONA: Donkey, shh! I’m the princess.'
	'DONKEY: Aah!'
	'FIONA: It’s me, in this body.'
	'DONKEY: Oh, my God! You ate the princess. (to her stomach) Can you hear me?'
	'FIONA: Donkey!'
	'DONKEY: (still aimed at her stomach) Listen, keep breathing! I’ll get you out of there!'
	'FIONA: No!'
	'DONKEY: Shrek! Shrek! Shrek!'
	'FIONA: (Covering Donkey’s mouth) Shh.'
	'DONKEY: (Muffled) Shrek!'
	'FIONA: This is me.'
	'DONKEY: Princess...? What happened to you? You’re, uh...uh...eh...different.'
	'FIONA: I’m ugly, okay?'
	'DONKEY: Well, yeah! Well was it something you ate? ’Cause I told Shrek those rats was a bad idea. You are what you eat, I said. Now--'
	'FIONA: No! I -- I’ve been this way as long as I can remember.'
	'DONKEY: What do you mean? Look, I ain’t never seen you like this before.'
	'FIONA: It only happens when sun goes down.'
	'FIONA: "By night one way, by day another. This shall be the norm... until you find true love’s first kiss... and then take love’s true form."'
	'DONKEY: Aww, that’s beautiful. I didn’t know you wrote poetry.'
	'FIONA: It’s a spell. (sigh) When I was a little girl, a witch cast a spell on me. Every night I become this. This horrible, ugly beast!'
	'FIONA: I was placed in a tower to await the day my true love would rescue me. That’s why I have to marry Lord Farquaad tomorrow before the sun sets and he sees me...like this.'
	'DONKEY: All right, all right. Calm down. Look, it’s not that bad. You’re not that ugly. Well, ok, I ain’t gonna lie. You are ugly. But you only look like this at night. Shrek’s ugly 24/7.'
	'FIONA: But Donkey, I’m a princess, and this is not how a princess is meant to look.'
	'DONKEY: Princess, how ’bout if you don’t marry Farquaad?'
	'FIONA: I have to. Only my true love’s kiss can break the spell.'
	'DONKEY: But, you know, um...you’re kind of an ogre. And Shrek...well...you got a lot in common.'
	'FIONA: Shrek?'
	'SHREK: Princess, I-- Uh, how’s it going, first of all? Good? Um, good for me too. I’m okay. I saw this flower and thought of you because it’s pretty and-- well, I don’t really like it but I thought you might like it ’cause you’re pretty. But I like you anyway. I’d-- uh, uh...(sighs) I’m in trouble. Okay, here we go.'
	'FIONA: I can’t just marry whoever I want. Take a good look at me, Donkey. I mean, really, who can ever love a beast so hideous and ugly? "Princess" and "ugly" don’t go together. That’s why I can’t stay here with Shrek.'
	'FIONA: My only chance to live happily ever after is to marry my true love. Don’t you see, Donkey? That’s just how it has to be.'
	'FIONA: It’s the only way to break the spell.'
	'DONKEY: Well you at least gotta tell Shrek the truth.'
	'FIONA: No! You can’t breathe a word. No one must ever know.'
	'DONKEY: What’s the point of being able to talk if you gotta keep secrets?'
	'FIONA: Promise you won’t tell. Promise!'
	'DONKEY: All right, all right. I won’t tell him. But you should.'
	'DONKEY: I just know before this is over, I’m gonna need a whole lot of serious therapy. Look at my eye twitchin’.'
	'FIONA: I tell him, I tell him not. I tell him, I tell him not...'
	'FIONA: I tell him! Shrek!'
	'FIONA: Shrek! There’s something I want...'
	'FIONA: Shrek! Are you all right?'
	'SHREK: Perfect! Never been better.'
	'FIONA: I...I don’t...there’s something I have to tell you.'
	'SHREK: You don’t have to tell me anything, princess. I heard enough last night.'
	'FIONA: You heard what I said?'
	'SHREK: Every word.'
	'FIONA: I thought you’d understand.'
	'SHREK: Oh, I understand. Like you said, "Who could love a hideous, ugly beast?"'
	'FIONA: But I thought that wouldn’t matter to you.'
	'SHREK: Yeah? Well, it does.'
	'SHREK: Ah, right on time. Princess, I’ve brought you a little something.'
	'DONKEY: What’d I miss? What’d I miss?'
	'DONKEY: Who said that? Couldn’t have been the donkey.'
	'FARQUAAD: Princess Fiona.'
	'SHREK: As promised. Now hand it over.'
	'FARQUAAD: Very well, ogre. The deed to your swamp, cleared out, as agreed. Take it and go before I change my mind.'
	'FARQUAAD: Forgive me, Princess, for startling you, but you startled me--for I have never seen such a radiant beauty before. I am Lord Farquaad.'
	'FIONA: Lord Farquaad? Oh, no, no. Forgive me, my lord, for I was just saying a short...'
	'FIONA: ...farewell.'
	'FARQUAAD: Oh, that is so sweet. You don’t have to waste good manners on the ogre. It’s not like it has feelings.'
	'FIONA: No, you’re right. It doesn’t.'
	'FARQUAAD: Princess Fiona, beautiful, fair, flawless Fiona. I ask your hand in marriage.'
	'FARQUAAD: Will you be the perfect bride for the perfect groom?'
	'FIONA: Lord Farquaad, I accept. Nothing would make--'
	'FARQUAAD: Excellent! I’ll start the plans, for tomorrow we wed!'
	'FIONA: No!'
	'FIONA: I mean--ah, why wait? Let’s get married today. Before sunset.'
	'FARQUAAD: Oh, anxious, are we? You’re right. The sooner, the better. There’s so much to do!'
	'FARQUAAD: There’s the caterer, the cake, the band, the guest list. Captain, round up some guests!'
	'FIONA: Fare thee well, ogre.'
	'DONKEY: Shrek, what are you doing? You’re letting her get away!'
	'SHREK: Yeah? So what?'
	'DONKEY: Shrek there’s something about her you don’t know. Look, I-- I talked to her last night... She’s --'
	'SHREK: Yeah I know you talked to her last night. You’re great pals, aren’t ya? Now, if you two are such good friends, why don’t you follow her home?!'
	'DONKEY: But Shrek, I-- I wanna go with you.'
	'SHREK: Hey I told you, didn’t I? You’re not coming home with me. I live alone! My swamp! Me! Nobody else! Understand? Nobody! Especially useless, pathetic, annoying, talking donkeys!'
	'DONKEY: But, I thought...'
	'SHREK: Yeah. You know what? You thought wrong! (stomps off)'
	'DONKEY: Shrek.'
	'SHREK: Donkey? What are you doing?'
	'DONKEY: I would think, of all people, you would recognize a wall when you see one.'
	'SHREK: Well, yeah. But the wall’s supposed to go around my swamp, not through it.'
	'DONKEY: It is, around your half. See that’s your half, and this is my half.'
	'SHREK: Oh! Your half? Hmm.'
	'DONKEY: Yes, my half. I helped rescue the princess. I did half the work. I get half the booty. Now hand me that big old rock, the one that looks like your head.'
	'SHREK: Back off!'
	'DONKEY: No, you back off.'
	'SHREK: This is my swamp!'
	'DONKEY: Our swamp!'
	'SHREK: Let go, Donkey!'
	'DONKEY: You let go!'
	'SHREK: Stubborn jackass!'
	'DONKEY: Smelly ogre.'
	'SHREK: Fine!'
	'DONKEY: Hey, hey, come back here. I’m not through with you yet.'
	'SHREK: Well, I’m through with you!'
	'DONKEY: Uh-uh! You know, with you it’s always "me, me, me!" Well, guess what! Now it’s my turn! So you just shut up and pay attention!'
	'DONKEY: You are mean to me! You insult me and you don’t appreciate anything that I do! You’re always pushing me around or pushing me away.'
	'SHREK: Oh, yeah? Well, if I treated you so bad, how come you came back?'
	'DONKEY: Because that’s what friends do! They forgive each other!'
	'SHREK: Oh, yeah. You’re right, Donkey. I forgive you...for stabbing me in the back!'
	'DONKEY: Uhhhh! You’re so wrapped up in layers, onion boy, you’re afraid of your own feelings.'
	'SHREK: Go away!'
	'DONKEY: See! There you are, doing it again just like you did to Fiona. And all she ever do was like you, maybe even love you.'
	'SHREK: Love me? She said I was ugly! A hideous creature! I heard the two of you talking.'
	'DONKEY: She wasn’t talkin’ about you. She was talkin’ about...uh...somebody else.'
	'SHREK: She wasn’t talking about me? Well then who was she talking about?'
	'DONKEY: Uh-uh, no way. I ain’t saying anything. You don’t wanna listen to me. Right? Right?'
	'SHREK: Donkey!'
	'DONKEY: No!'
	'SHREK: Okay, look. I’m sorry, all right?'
	'SHREK: I’m sorry. I guess I am just a big, stupid...ugly ogre. Can you forgive me?'
	'DONKEY: Hey, that’s what friends are for, right?'
	'SHREK: Right. Friends?'
	'DONKEY: Friends.'
	'SHREK: So, um, what did Fiona say about me?'
	'DONKEY: What are you asking me for? Why don’t you just go ask her?'
	'SHREK: The wedding! We’ll never make it in time.'
	'DONKEY: Ha-ha-ha! Never fear, for where, there’s a will, there’s a way and I have a way.'
	'SHREK: Donkey?!'
	'DONKEY: I guess it’s just my animal magnetism.'
	'SHREK: (laughs) Aw, come here, you.'
	'DONKEY: All right, all right. Don’t get all slobbery. No one likes a kiss ass. All right, hop on and hold on tight. I haven’t had a chance to install the seat belts yet.'
	'BISHOP: People of Duloc, we gather here today to bear witness to the union....'
	'FIONA: Um-'
	'BISHOP: ...of our new king...'
	'FIONA: Excuse me. Could we just skip ahead to the "I do’s"?'
	'FARQUAAD: Go on.'
	'DONKEY: Go ahead, have some fun. If we need you, I’ll whistle. How about that?'
	'DONKEY: Shrek, wait, wait! Wait a minute! You wanna do this right, don’t you?'
	'SHREK: What are you talking about?'
	'DONKEY: There’s a line, there’s a line you gotta wait for. The priest is gonna say, "Speak now or forever hold your peace." And that’s when you say, "I object!"'
	'SHREK: Oh, I don’t have time for this!'
	'DONKEY: Hey, wait. What are you doing? Listen to me!'
	'DONKEY: Look, you love this woman, don’t you?'
	'SHREK: Yes.'
	'DONKEY: You wanna hold her?'
	'SHREK: Yes.'
	'DONKEY: Please her?'
	'SHREK: Yes!'
	'DONKEY: (singing) "Then you got to, got to try a little tenderness". (talking) The chicks love that romantic crap!'
	'SHREK: All right! Cut it out! When does this guy say the line?'
	'DONKEY: We gotta check it out.'
	'BISHOP: And so, by the power vested in me...'
	'SHREK: What do you see?!'
	'DONKEY: The whole town’s in there.'
	'BISHOP: I now pronounce you husband and wife...'
	'DONKEY: They’re at the altar!'
	'PRIEST: ...king and queen.'
	'DONKEY: Mother Fletcher! He already said it.'
	'SHREK: Oh, for the love of Pete!'
	'SHREK: I object!'
	'FIONA: Shrek?'
	'FARQUAAD: Oh, now what does he want?'
	'SHREK: Hi, everyone. Havin’ a good time, are ya? I love Duloc, first of all. Very clean.'
	'FIONA: What are you doing here?'
	'FARQUAAD: Really, it’s rude enough being alive when no one wants you, but showing up uninvited to a wedding...'
	'SHREK: Fiona! I need to talk to you.'
	'FIONA: Oh, now you wanna talk? It’s a little late for that, so if you’ll excuse me--'
	'SHREK: But you can’t marry him!'
	'FIONA: And why not?'
	'SHREK: Because--because he’s just marrying you so he can be king!'
	'FARQUAAD: Outrageous! Fiona, don’t listen to him--'
	'SHREK: He’s not your true love.'
	'FIONA: And what do you know about true love?!'
	'SHREK: Well, I--uh--I mean...'
	'FARQUAAD: Oh, this is precious. (laughs) The ogre has fallen in love with the princess! Oh, good Lord. (laughs)'
	'FARQUAAD: An ogre and a princess! (laughs)'
	'FIONA: Shrek, is this true?'
	'FARQUAAD: Who cares?! It’s preposterous! Fiona, my love, we’re but a kiss away from our "happily ever after." Now kiss me!'
	'FIONA: "By night one way, by day another." I wanted to show you before.'
	'SHREK: Well, uh, that explains a lot!'
	'FARQUAAD: Ugh! It’s disgusting! Guards! Guards! I order you to get that out of my sight now!'
	'FARQUAAD: Get them! Get them both!'
	'FIONA: No, no! Shrek!'
	'FARQUAAD: This hocus-pocus alters nothing! This marriage is binding, and that makes me king! See?! See?!'
	'FIONA: No, let go of me! Shrek!'
	'SHREK: No!'
	'FARQUAAD: Don’t just stand there, you morons!'
	'SHREK: Get out of my way! Fiona!'
	'FARQUAAD: Kill him if you have to--but get him!'
	'FARQUAAD: Beast, I’ll make you regret the day we met! I’ll see you drawn and quartered! You’ll beg for death to save you!'
	'FIONA: No, Shrek!'
	'FARQUAAD: And as for you, my wife!'
	'SHREK: Fiona!'
	'FARQUAAD: I’ll have you locked back in that tower for the rest of your days! I’m king!'
	'FARQUAAD: I will have order! I will have perfection! I will have--'
	'FARQUAAD: Arrrggghhh!'
	'DONKEY: All right! Nobody move! I got a dragon here, and I’m not afraid to use it.'
	'DONKEY: I’m a donkey on the edge!'
	'DONKEY: Celebrity marriages. They never last, do they?'
	'DONKEY: Go ahead, Shrek.'
	'SHREK: Uh, Fiona?'
	'FIONA: Yes, Shrek?'
	'SHREK: I -- I love you.'
	'FIONA: Really?'
	'SHREK: Really, really.'
	'FIONA: I love you too.'
	'VOICE: "Until you find true love’s first kiss and then take love’s true form. Take love’s true form. Take love’s true form."'
	'SHREK: Fiona? Fiona. Are you all right?'
	'FIONA: Well, yes...but I don’t understand. I’m supposed to be beautiful.'
	'SHREK: But you are beautiful.'
	'DONKEY: I was hoping this would be a happy ending.'
	'GINGERBREAD MAN: God bless us, every one.'
	'DONKEY: (as he’s done singing and we fade to black) Oh, that’s funny. Oh. Oh. I can’t breathe. I can’t breathe.'
)
bindLoop "pgup" "shrek"
createConfigFile "shrek"

echo "Done!"

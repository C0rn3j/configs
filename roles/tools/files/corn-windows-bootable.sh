#!/usr/bin/env bash
set -euo pipefail
Red='\033[0;31m'
Green='\033[0;32m'
Yellow='\033[0;33m'
NoColor='\033[0m'

Help() {
	echo -e ${Yellow}
	cat << EOF
This tool has three uses:
  * Loop option is meant for creating bootable Windows image files that you can dd to a flash drive or boot via other mass-storage means
    * If you need to work with the bootable image after it's been created and you don't see partitions on mounting the loopback device, do 'sudo partprobe loopdevice' or use -P flag when using losetup
  * Device option is for creating a bootable flashdrive with Windows
  * Repack option is to add the ei.cfg file to a Windows ISO image

Hacks applied to all options:
  * ./sources/ei.cfg                         - Added to show all possible editions and disable autoselecting edition that is possibly registered on the motherboard
  * https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-7/dd744321(v=ws.10)
    ./boot/bootfix.bin                       - Removed               to skip "Press any key to boot from the CD/DVD" prompt in BIOS
    ./efi/microsoft/boot/efisys_noprompt.bin - Renamed to efisys.bin to skip "Press any key to boot from the CD/DVD" prompt in UEFI

Hacks applied to repack (only useful for W11):
  BypassTPMCheck, BypassRAMCheck, BypassSecureBootCheck

Options:
  --image                             Path to the Windows ISO
    (only)One of the following options is also required:
  --device [/dev/sdX]                 Set up a flash drive with the supplied image
  --loop                              Create a bootable image from the ISO via a loopdevices (BIOS boot is broken due to a GRUB bug)
  --repack                            Repacks the ISO with our hacks to the home folder
  -h | --help                         Show this help...

Usage example: ./corn-windows-bootable --image Win10_2004_English_x64.iso --device /dev/sde
EOF
	echo -e ${NoColor}
}

Cleanup() {
	# Unmount
	if mount | grep " /mnt/tempISO " > /dev/null; then
		sudo umount -A /mnt/tempISO
	fi
	if mount | grep " /mnt/tempImage " > /dev/null; then
		sudo umount -A /mnt/tempImage
	fi
	# Delete loopback (check if variable is defined as initial cleanup won't have it mounted)
	if [[ -n ${loopbackISO-} ]]; then
		sudo losetup -d ${loopbackISO}
	fi
	# And remove the temporary mounts
	if [[ -e /mnt/tempImage ]]; then
		sudo rmdir /mnt/tempImage
	fi
	if [[ -e /mnt/tempISO ]]; then
		sudo rmdir /mnt/tempISO
	fi
	rm -rf /tmp/bootableWin
}

SetupLoopback() {
	# Find a free loopback device for the resulting file (for example /dev/loop1)
	loopback=$(sudo losetup -f)
	# Make the final image as big as the ISO + 100MB to over-accomodate GRUB and splitting install.wim
	bytesISO=$(( $(du -b ${windowsISOPath} | awk '{print $1}') + 104857600))

	truncate -s${bytesISO} ${finalImage}
	sudo losetup ${loopback} ${finalImage}
	devicePath=${loopback}
	installImage
	echo -e "${Green}All went well! Resulting bootable image is in ${finalImage}${NoColor}"
}

SetupDevice() {
	sudo wipefs -a ${devicePath}
	installImage
	echo -e "${Yellow}Running 'sync', this might take a while...${NoColor}"
	sync
	echo -e "${Green}All went well! Flash drive ${devicePath} with image ${finalImage} should be both UEFI and BIOS bootable!${NoColor}"
}

installImage() {
	sudo parted -s ${devicePath} mklabel msdos
	sudo parted -s ${devicePath} mkpart primary fat32 1MiB 100%
	sudo parted -s ${devicePath} set 1 boot on
	if [[ -n ${loopback-} ]]; then
		sudo mkfs.fat -F32 -I -n ${FAT32Label} ${devicePath}p1
	else
		sudo mkfs.fat -F32 -I -n ${FAT32Label} ${devicePath}1
	fi
	sudo mkdir -p /mnt/tempImage
	if [[ -n ${loopback-} ]]; then
		sudo mount ${devicePath}p1 /mnt/tempImage
	else
		sudo mount ${devicePath}1 /mnt/tempImage
	fi
	# Rsync the files from the ISO to the image file except for install.wim as we need to split it first
	# Exclude bootfix.bin for BIOS skip hack
	# https://social.technet.microsoft.com/Forums/en-US/92246cf6-38ed-4568-835a-012447c649b4/vista-winpehow-to-remove-message-of-quotpress-any-key-to-boot-from-the-cd8221directly-boot-to
	sudo rsync -vr --progress --exclude install.wim --exclude bootfix.bin /mnt/tempISO/ /mnt/tempImage/
	# UEFI skip hack
	sudo mv /mnt/tempImage/efi/microsoft/boot/efisys_noprompt.bin /mnt/tempImage/efi/microsoft/boot/efisys.bin
	# Split install.wim into multiple 3GB install.swm files because ISOs since 1809 have an over 4GB install.wim, hitting FAT32 limitations
	sudo ${wimsplit} /mnt/tempISO/sources/install.wim /mnt/tempImage/sources/install.swm 3000
	# ei.cfg hack
	sudo tee /mnt/tempImage/sources/ei.cfg <<EOF
[Channel]
Retail
EOF
	# Install GRUB on MBR, enabling BIOS booting
	sudo ${grubInstall} --target=i386-pc --boot-directory=/mnt/tempImage/boot ${devicePath}
	sudo tee /mnt/tempImage/boot/grub/grub.cfg <<EOF
default=1
timeout=15
color_normal=light-cyan/dark-gray
menu_color_normal=black/light-cyan
menu_color_highlight=white/black
menuentry "Start ${windowsISOFilename} Installation" {
    insmod ntfs
    insmod search_label
    search --no-floppy --set=root --label ${FAT32Label} --hint hd0,msdos1
    ntldr /bootmgr
    boot
}
EOF
# Detach the loopback device
if [[ -n ${loopback-} ]]; then
	sudo losetup -d ${loopback}
fi
}

# ${1} is relative or absolute path to boot.wim
reqAvoid() {
	cat > /tmp/corn.reg << EOF
Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\SYSTEM\Setup\LabConfig]
"BypassTPMCheck"=dword:00000001
"BypassRAMCheck"=dword:00000001
"BypassSecureBootCheck"=dword:00000001
EOF
	# Get amount of images in the boot.wim, we only need to edit the PE one (I think), but we edit all (there's 2) anyway
	imageCount=$(wiminfo ${1} | grep 'Image Count' | sed 's/[^0-9]*//g')
	# Is this even needed? I am too tired to check.
	chmod 777 ${1}
	for i in $(seq 1 ${imageCount}); do
		echo "Extracting image ${1}"
		wimextract ${1} ${i} /Windows/System32/config/SYSTEM --dest-dir /tmp/
		echo "Editing image ${1}"
		# Because this throws a perms warning which in turn throws an error code
		set +e && ${reged} -I /tmp/SYSTEM 'HKEY_LOCAL_MACHINE\SYSTEM' /tmp/corn.reg -C && set -e
		echo "Updating image ${1}"
		wimupdate ${1} ${i} --command='add /tmp/SYSTEM /Windows/System32/config/SYSTEM'
	done
}

RepackISO() {
	finalImage="${HOME}/${strippedFilename}-repack.iso"
	mkdir -p /tmp/bootableWin/sources
	# ei.cfg hack
	sudo tee /tmp/bootableWin/sources/ei.cfg <<EOF
[Channel]
Retail
EOF
	# Copy boot.wim and do W11 req hack on it, requiring 0.5GB of space in /tmp (usually tmpfs, so RAM)
	cp /mnt/tempISO/sources/boot.wim /tmp/bootableWin/sources/boot.wim
	reqAvoid /tmp/bootableWin/sources/boot.wim
	# Supplying -m (exclude) for BIOS skip hack
	# Supplying -x (exclude) for UEFI skip hack
	# https://unix.stackexchange.com/a/556872/435522
	mkisofs \
	-iso-level 4 \
	-l \
	-R \
	-UDF \
	-D \
	-b boot/etfsboot.com \
	-no-emul-boot \
	-boot-load-size 8 \
	-hide boot.catalog \
	-eltorito-alt-boot \
	-eltorito-platform efi \
	-no-emul-boot \
	-b efi/microsoft/boot/efisys.bin \
	-m 'bootfix.bin' \
	-x '/mnt/tempISO/efi/microsoft/boot/efisys.bin' \
	-x '/mnt/tempISO/sources/boot.wim' \
	-graft-points \
	-o ${finalImage} \
	/mnt/tempISO/ /tmp/bootableWin/ 'efi/microsoft/boot/efisys.bin=/mnt/tempISO/efi/microsoft/boot/efisys_noprompt.bin'
	echo -e "${Green}Repacked ISO into ${finalImage}!"
}

if ! wimsplit=$(command -v wimsplit); then
	Help
	echo -e "${Red}You need to install wimlib as wimsplit command was not found!${NoColor}"
	exit 1
fi

if ! reged=$(command -v reged); then
	Help
	echo -e "${Red}You need to install chntpw as reged command was not found!${NoColor}"
	exit 1
fi

if ! grubInstall="$(command -v grub-install)"; then
	if ! grubInstall="$(command -v grub2-install)"; then
		echo -e "${Red}You need to install grub as grub-install nor grub2-install command was found!${NoColor}"
		exit 1
	fi
fi

windowsISOPath=""
devicePath=""
repack="no"
loop="no"
while [[ -n ${1-} ]]; do
	case ${1} in
		--image )
			shift
			windowsISOPath=${1};;
		--device )
			shift
			devicePath=${1};;
		--loop )
			loop="yes";;
		--repack )
			repack="yes";;
		-h | --help )
			Help
			exit 0;;
		* )
			echo -e "${Red}Wrong parameter(s)${NoColor}"
			Help
			exit 1;;
	esac
	shift
done
if [[ ${windowsISOPath} == "" ]]; then
	Help
	exit 1
fi
# The original Windows ISO file
windowsISOFilename=$(echo "${windowsISOPath##*/}")

# Remove all characters after and including . and remove path, leaving just the filename without extension
# Example stripped string: Win8.1_English_x64
strippedFilename=$(echo ${windowsISOFilename%.*})
# Final Image that files will be written to
finalImage="${HOME}/${strippedFilename}-bootable.iso"
# Generate a FAT32 compatible label from the filename
# Max 11 char limit, none of those symbols present "* ? . , ; : / \ | + = < > [ ]", but realistically I've only seen dots in the official filenames
FAT32Label=$(echo ${strippedFilename} | sed -e 's/Win/W/g' -e 's/\.//g' | cut -c-11)

if [[ -e ${finalImage} ]] && [[ ${devicePath} == "" ]]; then
	echo -e "${Red}This image already exists in ${finalImage} - exiting${NoColor}"
	exit 1
fi

# Cleanup if a previous run failed
Cleanup
# Find a free loopback device for the ISO file (for example /dev/loop0)
loopbackISO=$(sudo losetup -f)
sudo losetup ${loopbackISO} ${windowsISOPath}
sudo mkdir -p /mnt/tempISO
sudo mount -o ro ${loopbackISO} /mnt/tempISO
if [[ ${devicePath} != "" ]]; then
	SetupDevice
elif [[ ${loop} == "yes" ]]; then
	SetupLoopback
elif [[ ${repack} == "yes" ]]; then
	RepackISO
else
	echo -e "${Red}Missing parameter, unsure what to do${NoColor}"
	Help
	exit 1
fi
# Final cleanup
Cleanup

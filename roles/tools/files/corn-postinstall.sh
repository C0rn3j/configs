#!/usr/bin/env bash
set -euo pipefail

Red='\033[0;31m'
Yellow='\033[0;33m'
Purple='\033[0;35m'
NoColor='\033[0m'

# Enable NTP time syncing
sudo timedatectl set-ntp true
source <(sudo cat /root/answerfile)
if [[ ${answerEncrypt} == "yes" ]]; then
	# Rename VG. Has to be done in a post install script in case we're setting up encrypted Arch from within encrypted Arch, we can't have two VGs with the same name
	if [[ -e /dev/ArchVolInstall ]]; then
		sudo vgrename /dev/ArchVolInstall /dev/ArchVol
		if [[ -e /boot/loader/entries/arch.conf ]]; then # UEFI install
			sudo sed -i s/"ArchVolInstall"/"ArchVol"/ /boot/loader/entries/arch.conf
		else # BIOS install
			sudo sed -i s/"ArchVolInstall"/"ArchVol"/ /etc/default/grub
			sudo grub-mkconfig -o /boot/grub/grub.cfg
		fi
	fi
	echo -e "${Yellow}Setting up remote SSH unlocking${NoColor}"
	# https://wiki.archlinux.org/index.php/Dm-crypt/Specialties#Remote_unlocking_(hooks:_systemd,_systemd-tool)
	# Generate SSH ed25519 host key if it doesn't exist already (sshd.service creates it on first run)
	if [[ ! -e /etc/ssh/ssh_host_ed25519_key ]]; then
		sudo ssh-keygen -f /etc/ssh/ssh_host_ed25519_key -N '' -t ed25519
	fi
	if [[ ${username} != 'c0rn3j' ]]; then
		echo -e "${Purple}Make sure you put your own pubkeys to /etc/mkinitcpio-systemd-tool/config/authorized_keys if you want remote initramfs unlocking to work!${NoColor}"
	fi
	sudo systemctl enable initrd-tinysshd
fi
sudo mkinitcpio -P

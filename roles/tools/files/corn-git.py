#!/usr/bin/env python
"""Corn-git, get filesize diff of files in the current git directory."""

import subprocess
import logging
import os
from termcolor import colored
from dataclasses import dataclass

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

@dataclass
class FileInfo:
	filePath: str
	beforeSize: int
	afterSize: int
	diff: int

@dataclass
class CommandOutput:
	"""Class for shell command output."""

	def __init__(self, returnCode: int, stdOut: str, stdErr: str):
		self.returnCode = returnCode
		self.stdOut = stdOut
		self.stdErr = stdErr

class ShellExecError(Exception):
	pass

def shell_exec(*, args:list[str]|str, timeout: int = 300, textStdout: bool = True) -> CommandOutput:
	"""Execute a shell command with subprocess.

	Args:
	----
		args (list[str] or str): Command and arguments to execute.
		timeout (int): Maximum time in seconds to wait for the command to complete.
		debug (bool): If True, print debugging information.

	Returns:
	-------
	CommandOutput: An object containing the returncode, stdout, and stderr.

	Raises:
	------
	ShellExecError: If an error occurs during command execution.

	"""
	try:
		logging.debug(f"Executing command: {' '.join(args) if isinstance(args, list) else args}")
		process = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=textStdout)
		stdout, stderr = process.communicate(timeout=timeout)
		stdout = stdout.strip()
		stderr = str(stderr.strip())
		if textStdout and stderr != '':
			logging.warning(colored(stderr, 'red'))
		return CommandOutput(process.returncode, stdout, stderr)
	except subprocess.TimeoutExpired as e:
		raise ShellExecError(f"Command '{' '.join(args) if isinstance(args, list) else args}' timed out after {timeout} seconds.") from e
	except subprocess.SubprocessError as e:
		raise ShellExecError(f"An error occurred while executing command '{' '.join(args) if isinstance(args, list) else args}'.") from e

def pretty_size(*, bytes: int) -> str:
	if bytes < 1024:
		size = f"{bytes} bytes"
	elif bytes < 1048576:
		size = f"{bytes / 1024:.2f} KB"
	elif bytes < 1073741824:
		size = f"{bytes / 1048576:.2f} MB"
	else:
		size = f"{bytes / 1073741824:.2f} GB"
	return size


def get_file_size(*, filename:str) -> int:
	"""Get the current size of the file in bytes."""
	if os.path.exists(filename):
		return os.path.getsize(filename)
	return 0

untrackedFiles = shell_exec(args=['git', 'ls-files', '--others', '--exclude-standard'])
modifiedFiles  = shell_exec(args=['git', 'diff', '--name-only'])
stagedFiles    = shell_exec(args=['git', 'diff', '--name-only', '--cached'])

def getFileDiff(*, fileList:list[str]) -> list[FileInfo]:
	fileDB=[]
	for filename in fileList:
		# Skip deleted files
		if not os.path.exists(filename):
			continue
		logging.debug(f"Filename: {filename}")
		before = shell_exec(args=['git', 'show', f"HEAD:{filename}"], timeout=30, textStdout=False)
		before_size = len(before.stdOut)
		after_size = get_file_size(filename=filename)
#		diff = before_size - after_size
#		print(f"{diff} ({pretty_size(bytes=diff)} reduction): {pretty_size(bytes=before_size)} -> {pretty_size(bytes=after_size)} {filename}")
#		total_diff += diff
		fileDB.append(FileInfo(afterSize=after_size, beforeSize=before_size,filePath=filename, diff=before_size-after_size))
	return fileDB


def main():
	logging.info("START")
	untrackedFileInfo = getFileDiff(fileList=untrackedFiles.stdOut.splitlines())
	modifiedFileInfo = getFileDiff(fileList=modifiedFiles.stdOut.splitlines())
	stagedFileInfo = getFileDiff(fileList=stagedFiles.stdOut.splitlines())
	untrackedDiff = 0
	modifiedDiff = 0
	stagedDiff = 0

	for file in untrackedFileInfo:
#		diffType = 'Untracked'
		untrackedDiff += file.diff

	for file in modifiedFileInfo:
#		diffType = 'Modified'
		modifiedDiff += file.diff

	for file in stagedFileInfo:
#		diffType = 'Staged'
		stagedDiff += file.diff

	allFilesInfo = untrackedFileInfo + modifiedFileInfo + stagedFileInfo
	allFilesInfo.sort(key=lambda x: x.diff)
	for file in allFilesInfo:
		niceDiff = pretty_size(bytes=file.diff)
		niceBefore = pretty_size(bytes=file.beforeSize)
		niceAfter = pretty_size(bytes=file.afterSize)
		print(f"{colored(text=niceBefore,color='dark_grey')} reduced by {colored(text=niceDiff,color='green')} to {colored(text=niceAfter,color='light_blue')} - {file.filePath}")

	totalDiff = untrackedDiff + modifiedDiff + stagedDiff

	print(colored(text=f"Untracked diff: {pretty_size(bytes=untrackedDiff)}", color='cyan'))
	print(colored(text=f"Modified diff: {pretty_size(bytes=modifiedDiff)}", color='cyan'))
	print(colored(text=f"Staged diff: {pretty_size(bytes=stagedDiff)}", color='cyan'))
	print(colored(text=f"Total size change: -{pretty_size(bytes=totalDiff)}", color='light_cyan'))

if __name__ == "__main__":
	main()

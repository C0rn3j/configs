#!/usr/bin/env python

# This client is a drop-in replacement for `spd-say`.
# But this can also run as a Python module!

import sys
import requests
import subprocess
import io
import logging

# Update the location of your default speech server.
def say(text, base_url="http://localhost:59125/api/tts"):
    # params = { 'text': text, "lengthScale": "0.6" }
    params = { 'text': text, "voice": "en_US/vctk_low" }
    try:
        response = requests.get(base_url, params=params)
        if response.status_code == 200:
            audio_data = io.BytesIO(response.content)
            audio_bytes = audio_data.read()
            # Pass the audio data to gstreamer-1.0 for playback
            # You could also use `aplay -` for this
            command = ['gst-launch-1.0', '-q', 'fdsrc', '!', 'wavparse', '!', 'autoaudiosink']
            subprocess.run(command, input=audio_bytes, check=True,
            stdout=subprocess.DEVNULL)
        else:
            logging.error(response.status_code)
    except Exception:
        logging.error("Cannot contact speech-dispatcherd service.")

if __name__ == '__main__':
    if len(sys.argv) > 1:
        say(sys.argv[1])
    else:
        logging.error("Import say from mimic3_client in Python. Or test from command line with `mimic3_client 'this is a test'`")

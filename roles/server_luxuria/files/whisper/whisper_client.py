#!/usr/bin/env python
import logging

#import pyautogui
import pyperclip
import os
import time
import queue
import sys
import re
import json
import webbrowser
import tempfile
import threading
import subprocess
import signal
import requests
import socket
from termcolor import colored
from lynxlynx import shellExec
#from mimic3_client import say
logging.basicConfig(
	level=logging.DEBUG,
	format="%(asctime)s [%(levelname)s] %(message)s",
	handlers=[
		logging.StreamHandler()
	]
)
# INFO to stderr
logging.getLogger().handlers[0].setLevel(logging.INFO)

# address of whisper.cpp server
cpp_url = "http://127.0.0.1:7777/inference"
# address of Fallback Chat Server.
fallback_chat_url = 'http://localhost:5000'
last_recording_logfile = '/tmp/whisper.txt'
computerHostname = socket.gethostname()

api_key = os.getenv("OPENAI_API_KEY")
if (api_key):
    import openai
    openai.api_key = api_key
else:
    logging.info("Export OPENAI_API_KEY if you want answers from ChatGPT.\n")

# commands and hotkeys for various platforms
commands = {
	"windows": {
		"file manager": "start explorer",
		"terminal":     "start cmd",
		"browser":      "start iexplore",
		"web browser":  "start iexplore",
	},

	"linux": {
		r"^file manager.?$":               "nohup dolphin 2>&1 </dev/null >/dev/null &",
		r"^terminal.?$":                   "nohup konsole 2>&1 </dev/null >/dev/null &",
		r"^(un)?(mute|toggle) audio.?$":   "pactl set-sink-mute @DEFAULT_SINK@ toggle",
		r"^(web )?browser.?$":             "nohup htmlview 2>&1 </dev/null >/dev/null &",
#		r"^steam.?$":                      "nohup flatpak run com.valvesoftware.Steam 2>&1 </dev/null >/dev/null &",
		r"^steam.?$":                      "flatpak run com.valvesoftware.Steam",
	},
}
hotkeys = {
    r"^new paragraph.?$": [['enter'],['enter']],
    r"^new line.?$":     [['enter']],
    r"^page up.?$":     	[['pageup']],
    r"^page down.?$":    [['pagedown']],
    r"^undo that.?$":    [['ctrl', 'z']],
    r"^copy that.?$":    [['ctrl', 'c']],
    r"^paste it.?$":     [['ctrl', 'v']],
    }
actions = {
#    r"^left click.?$": "pyautogui.click()",
#    r"^(click)( the)?( mouse).? ": "pyautogui.click()",
#    r"^middle click.?$": "pyautogui.middleClick()",
#    r"^right click.?$": "pyautogui.rightClick()",
    fr"^({computerHostname.lower()}|nix|computer).? (fucking )?(fucking|run|execute|open|start|launch)(up)?( a| the)?.? ": "execute_command(program=q)",
#    r"^(nix|computer).? closed? window": "pyautogui.hotkey('alt', 'F4')",
    fr"^({computerHostname.lower()}|nix|computer).? search( the)?( you| web| google| bing| online)?(.com)? for ": "browser_open(query=q)"
#    r"^(nix|computer).? (send|compose|write)( an| a) email to ": "os.popen('xdg-open \"mailto://' + q.replace(' at ', '@') + '\"')"
#    r"^(nix|computer).? (i need )?(let's )?(see |have |show )?(us |me )?(an? )?(image|picture|draw|create|imagine|paint)(ing| of)? ": "os.popen(f'./sdapi.py \"{q}\"')",
#    r"^(nix.? |computer.? )?(resume|zoom|continue|start|type) (typing|d.ctation|this)" : "exec('global chatting;global listening;chatting = False;listening = True')"
#    r"^(nix|computer).? ": "chatGPT(q)"
    }

def browser_open(*, query: str):
  webbrowser.open('https://you.com/search?q=' + re.sub(' ','%20',query))

def execute_command(*, program:str):
  for input_pattern, action in commands[sys.platform].items():
    if re.search(input_pattern, program):
      logging.info(f"Match found: {colored(input_pattern, 'cyan')} {colored(program,'yellow')} {colored(action,'green')}")
      try:
        shellExec(args=['bash', '-c', commands[sys.platform][input_pattern]], start_new_session=True, communicate=False)
#        os.system(commands[sys.platform][input_pattern])
      except Exception:
        logging.exception('Failed to execute action!')
      return
  logging.error(f"Command definition for '{colored(program,'red')}' was not found!")


  if program not in commands[sys.platform]:
    logging.error(f"Command definition for '{colored(program,'red')}' was not found!")
    return
  try:
    os.system(commands[sys.platform][program])
  except Exception:
    logging.exception("Error executing command")

def process_actions(multilineCommand:str) -> bool:
    splitCommands = multilineCommand.split("\n")
    processedAtLeastOne = False
    for command in splitCommands:
        logging.debug(command)
        for input_pattern, action in actions.items():
            if s:=re.search(input_pattern, command):
                q = command[s.end():].strip('.')
                logging.info(f"Match found: {colored(input_pattern, 'cyan')}{colored(q,'yellow')}")
                try:
                  eval(action)
                  processedAtLeastOne = True
                except Exception:
                  logging.exception('Failed to execute action!')
                break
        if chatting:
            chatGPT(multilineCommand)
            continue
    return processedAtLeastOne

# fix race conditions
audio_queue = queue.Queue()
listening = True
chatting = False

# search text for hotkeys
def process_hotkeys(txt):
    global start
    for key,val in hotkeys.items():
        # if hotkey command
        if re.search(key, txt):
            start = 0 # reset paragraph timer
            # unpack list of key combos such as ctrl-v
            for x in val:
                # press each key combo in turn
                # The * unpacks x to separate args
                logging.warning(f"TODO IMPLEMENT HOTKEY: {x}")
#                pyautogui.hotkey(*x)
            return True
    return False

def gettext(f:str) -> str:
    logging.debug("Getting text")
    result = ['']
    if f and os.path.isfile(f):
        files = {'file': (f, open(f, 'rb'))}
        # https://github.com/ggerganov/whisper.cpp/blob/master/examples/server/server.cpp#L381-L474
        data = {'temperature': '0.2', 'response_format': 'json', 'print_colors': 'true', 'language': 'auto'}
#        data = {'temperature': '0.2', 'response_format': 'verbose_json', 'beam_size': '5', 'entropy_thold': '2.8', 'max_context': 0}

        try:
            response = requests.post(cpp_url, files=files, data=data)
            response.raise_for_status()  # Check for errors

            # Parse the JSON response
            result = [response.json()]
            logging.debug(json.dumps(result))
            if 'segments' in result[0]: # verbose_json - but buggy, see https://github.com/ggerganov/whisper.cpp/issues/2381
              fullText = ''
              for segment in result[0]['segments']:
                fullText += segment['text']
              return fullText
            else:
              return result[0]['text']

        except requests.exceptions.RequestException as e:
            logging.error(e)
            return ''
    return ''

def copytext(t:str):
    # paste text in window
    pyperclip.copy(t) # weird that primary won't work the first time
#    logging.debug("TODO IMPLEMENT: CTRL+V")
#    if pyautogui.platform.system() == "Linux":
#        pyperclip.copy(t, primary=True) # now it works
#        pyautogui.middleClick()
#    else:
#        pyautogui.hotkey('ctrl', 'v')

print("Start speaking. Text should appear in the window you are working in.")
print("Say \"Stop listening.\" or press CTRL-C to stop.")
logging.warning("TODO IMPLEMENT VOICE: computer ready")
#say("Computer ready.")

messages = [{ "role": "system", "content": "In this conversation between `user:` and `assistant:`, play the role of assistant. Reply as a helpful assistant." },]

def chatGPT(prompt):
    global chatting, messages
    messages.append({"role": "user", "content": prompt})
    completion = ""
    # Call chatGPT
    if api_key:
        try:
            completion = openai.ChatCompletion.create(
              model="gpt-3.5-turbo",
              messages=messages
            )
            completion = completion.choices[0].message.content
        except Exception:
                logging.exception("ChatGPT had a problem. Here's the error message.")
    # Fallback to localhost
    if not completion:
        try:
            msg = {"messages": messages}
            response = requests.post(fallback_chat_url, json=msg)
            if response.status_code == 200:
                data = response.json()
                completion = data["content"]
        except Exception:
            logging.exception("Chat had a problem. Here's the error message.")
    # Read back the response completion
    if completion:
        if completion == "< nooutput >":
            completion = "No comment."
        print(completion)
        copytext(completion)
        logging.warning(f"TODO IMPLEMENT VOICE: {completion}")
#        say(completion)
        chatting = True
        # add to conversation
        messages.append({"role": "assistant", "content": completion})
        if len(messages) > 9:
            messages.remove(messages[1])
            messages.remove(messages[1])

def cleanupHallucinations(text:str) -> str:
    splitLines = text.strip('\n').split("\n")
    returnString = ''
    for line in splitLines:
        line = line.strip()
        if line in ('Subtitles by the Amara.org community', 'Thanks for watching!', 'Thanks for watching.', 'Thank you for watching!', 'Thank you for watching.', 'Thank you.', '- Thank you.', 'Mm-hmm.', 'Thanks.', '- Dude.', 'there.', 'the', 'Bye now.', '- Bye.', 'Bye.', 'bye', 'this.', 'Peace.', 'Amen.', '- Okay.', 'Okay.', 'Goodbye.', '- Good.', '- Cool', '- Yeah.', 'Yeah.', 'from', 'day.', 'you.', 'you', 'End.', 'End', 'And,', 'And', 'and', 'Whoa.', 'Wow.', 'wow', 'Ooh.', '- Oh.', 'Oh.', 'oh', 'Uh...', 'uh...', 'uh,', 'uh', 'up.', '- Mm.', 'me', '- No.', 'on.', '- Sure.', 'Ja.', 'No.', 'in.', 'um', 'So.', 'So,', 'so', 'it.', 'it', 'A.', '.', '-') or \
          line in ('Titulky vytvořil JohnyX.', 'Děkujeme.', 'Takže...') or \
          line in ('תודה.') or \
          line in ('Dziękuję.', 'Dzięki.') or \
          line in ('Hvala.', 'Hvala vam.') or \
          line in ('Grazie.') or \
          line in ('Gracias.') or \
          line in ('Obrigado.') or \
          line in ('ترجمة نانسي قنقر', 'شكراً لكم.', 'شكرا') or \
          line in ('ありがとうございました', 'ご視聴ありがとうございました。', 'ありがとうございました。') or \
          line in ('감사합니다.') or \
          line in ('Textning.nu') or \
          line in ('Să vă mulțumim!') or \
          line in ('Un paldies!', 'Paldies!') or \
          line in ('Terima kasih.') or\
          line in ('Субтитры создавал DimaTorzok', 'Продолжение следует...', 'Пока!', 'Спасибо.') or \
          line in ('MULTIPLAYER.COM') or \
          line in ('SILENT PRAYER', 'SILENT NIGHT', 'Kiitos.', 'Kiitos!') or \
          line in ('Vielen Dank.', 'Sous-titrage Société Radio-Canada') or \
          line in ('Takk for at du så med.', 'Takk for watching!', 'Undertekster av Amara.org-gemenskapen') or \
          line in ('Tack till elever och personal vid Värmlands universitet.', 'Tack till elever och personal vid Uppsala universitet.', 'Lysetil', 'Tack för mig.', 'Tack!'):
          logging.debug(f'Removing line "{line}"')
          continue
        returnString += f"{line}\n"
    return returnString.strip('\n')

def transcribe():
    global start
    global listening
    colorChange = False
    while True:
#        print("START TRANSCRIBE LOOP")
        try:
            # transcribe audio from queue
            if f := audio_queue.get():
                # delete temporary audio file
                t = cleanupHallucinations(gettext(f))
                try:
                    os.remove(f)
                except Exception:
                    logging.exception(f'Could not remove temporary audio file {f}')
                if t == '':
                  continue
                # So other apps can interact with this, optionally disable this
                with open(last_recording_logfile, "w") as file:
                  file.write(t)
                print("\r", end='')
                leftToDelete = os.get_terminal_size().columns - 2
                for x in range(leftToDelete):
                    print(' ' * (leftToDelete - x), x, end='\x1b[1K\r')

                if colorChange:
                  colorChange = False
                  print(f"\r{colored(t,'light_yellow')}")
                else:
                  colorChange = True
                  print(f"\r{colored(t,'light_green')}")
                if not t:
                    break

                # get lower-case spoken command string
                lower_case = t.lower().strip()
                if match := re.search(r"[^\w\s]$", lower_case):
                    lower_case = lower_case[:match.start()] # remove punctuation

                # see list of actions and hotkeys at top of file :)
                # Go to Website.
                if s:=re.search(r"^(peter|computer).? (go|open|browse|visit|navigate)( up| to| the| website)* [a-zA-Z0-9-]{1,63}(\.[a-zA-Z0-9-]{1,63})+$", lower_case):
                    q = lower_case[s.end():] # get q for command
                    webbrowser.open('https://' + q.strip())
                    continue
                # Stop dictation.
                elif re.search(r"^.?stop.(d.ctation|listening).?$", lower_case):
                    logging.warning("TODO IMPLEMENT VOICE: shutting down")
#                    say("Shutting down.")
                    break
                elif re.search(r"^.?(pause.d.ctation|positi.?i?cation).?$", lower_case):
                    listening = False
                    logging.warning("TODO IMPLEMENT VOICE: okay")
#                    say("okay")
                    #record_process.send_signal(signal.SIGINT)
                    #record_process.wait()
                    #say("Acknowledged.")
                    #input("Paused. Press Enter to continue...")
                    #audio_queue.get() # discard truncated sample
                    #listening = True
                elif process_actions(lower_case):
                    continue

                if not listening:
                    continue
                elif process_hotkeys(lower_case):
                    continue
                else:
                    now = time.time()
                    # Remove leading space from new paragraph
                    if now - start > 120:
                        t = t.strip()
                    start = now
                    copytext(t)
            # continue looping every second
            else:
                time.sleep(0.05)
        except KeyboardInterrupt:
            logging.warning("TODO IMPLEMENT VOICE: goodbye")
           # say("Goodbye.")
            break

def recorder():
    # If it wasn't for Gst conflict with pyperclip,
    # we could import record.py instead of os.system()
    # from record import Record
    # rec = Record()
    global record_process
    global running
    while running:
        # record some (more) audio to queue
        temp_name = tempfile.mktemp()+ '.wav'
        record_process = subprocess.Popen(["./record.py", temp_name])
        record_process.wait()
        audio_queue.put(temp_name)

def quit():
    logging.info("Stopping...")
    global running
    global listening
    listening = False
    running = False
    try:
        record_process.send_signal(signal.SIGINT)
        record_process.wait()
    except Exception:
        logging.exception('Unknown error')
    time.sleep(1)
    record_thread.join()
    # clean up
    try:
        while f := audio_queue.get_nowait():
            logging.info(f"Removing temporary file: {f}")
            if f[:5] == "/tmp/": # safety check
                os.remove(f)
    except Exception:
        logging.exception('Unknown error')
    logging.info("Freeing system resources.")

if __name__ == '__main__':
    record_process = None
    running = True
    record_thread = threading.Thread(target=recorder)
    record_thread.start()
    start = 0
    transcribe()
    quit()

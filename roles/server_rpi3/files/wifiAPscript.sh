#! /bin/bash
sleep 20
sysctl -p
iptables-restore /etc/iptables.rules
ip addr add 192.168.2.1/24 broadcast 192.168.2.255 dev wlan0
ip route add default via 192.168.1.1
sleep 20
systemctl restart dnsmasq
sleep 5
systemctl restart hostapd

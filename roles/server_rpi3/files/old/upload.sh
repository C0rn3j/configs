#!/bin/bash
# sudo certbot certonly --standalone -d "rpi3.rys.rs" --email spleefer90@gmail.com --rsa-key-size 4096 --agree-tos
set -euo pipefail
host="rpi3.rys.rs"
port="10090"
rsync -av -e "ssh -p $port" ./nginx.conf root@$host:/etc/nginx/nginx.conf
ssh -p $port root@$host "nginx -t && systemctl restart nginx"
rsync -av -e "ssh -p $port" ./backupscript.sh ./wifiAPscript.sh ./rollers.sh root@$host:/root/
echo "All OK!"

<?php
// https://www.psxhax.com/threads/playstation-updaters-xml-files-and-resources.372/
// https://github.com/im-alfa/ps4updates-php/blob/master/index.php
// PS3 - echo -n "BLES01133_00" | openssl dgst -sha1 -mac hmac -macopt hexkey:F5DE66D2680E255B2DF79E74F890EBF349262F618BCAE2A9ACCDEE5156CE8DF2CDF2D48C71173CDC2594465B87405D197CF1AED3B7E9671EEB56CA6753C2E6B0
// https://www.psdevwiki.com/ps3/Keys
// https://www.psdevwiki.com/ps4/Keys#HMAC-SHA256_Patch_Pkg_URL_Key
// https://www.psdevwiki.com/ps4/PKG_files
// https://github.com/bucanero/apollo-ps4/issues/69


function formatBytes(float $size, $precision = 2) {
	$base = log($size, 1024);
	$suffixes = array('', 'K', 'M', 'G', 'T');
	return round(pow(1024, $base - floor($base)), $precision) .''. $suffixes[floor($base)];
}

function About() {
	echo("Licensed under AGPLv3 - <a href=https://gitlab.com/C0rn3j/configs/tree/master/roles/server_bree/files/html/ps3>https://gitlab.com/C0rn3j/configs/tree/master/roles/server_bree/files/html/ps3</a><br>
By Martin Rys - <a href=https://rys.rs>https://rys.rs</a><br>
Inspired by <a href=http://www.eurasia.nu/psn_update_finder.php>http://www.eurasia.nu/psn_update_finder.php</a>
Check out https://www.prosperopatches.com/ or https://orbispatches.com/ for a nicer version
Supply GET parameters ps3=gameID and or ps4=gameID to search specific games");
}

function unhexlify($str) {
	return pack("H*", $str);
}

function GetURLPS3($GameID) {
	// np (Retail), prod-qa (Debugish), sp-int (Debug), q-np (Unknown)
	return "https://a0.ww.np.dl.playstation.net/tpl/np/$GameID/$GameID-ver.xml";
}

function GetURLPS4($GameID) {
	$byte_key = unhexlify("AD62E37F905E06BC19593142281C112CEC0E7EC3E97EFDCAEFCDBAAFA6378D84");
	$hash = hash_hmac("sha256" , "np_$GameID", $byte_key);
//	echo "Game ID: " . $GameID . "<br>";
//	echo "Hash: " . $hash . "<br>";
	return "https://gs-sec.ww.np.dl.playstation.net/plo/np/$GameID/$hash/$GameID-ver.xml";
}

// Create context options that disable certificate verification because SONY can"t be arsed to keep their TLS certs up to date
$arrContextOptions=array(
	"ssl"=>array(
		"verify_peer"=>false,
		"verify_peer_name"=>false,
	),
);

$scrapePS3=false;
$scrapePS4=false;
$urlToScape="";
$PS3TitleID="BCES01118"; // Resistance (4), BCES00647 for Buzz
$PS4TitleID="CUSA00411"; // GTA V
if ($_GET["ps3"]) {
	$PS3TitleID=$_GET["ps3"];
	$scrapePS3=true;
}
if ($_GET["ps4"]) {
	$PS4TitleID=$_GET["ps4"];
	$scrapePS4=true;
}
// Fallback to at least do something
if (! $scrapePS3 and ! $scrapePS4) {
	$scrapePS3=true;
}
$PS3URL=GetURLPS3($PS3TitleID);
$PS4URL=GetURLPS4($PS4TitleID);

if ($scrapePS3) {
	$rawxmlPS3 = file_get_contents($PS3URL, false, stream_context_create($arrContextOptions));
	// https://www.psdevwiki.com/ps3/Online_Connections#Game_Updating_Procedure
	if (! $xmlPS3=simplexml_load_string($rawxmlPS3)) {
		echo("PS3 Error - No result for <b>".$PS3TitleID."</b><br><br>");
		About();
		exit(1);
	}
}
if ($scrapePS4) {
	$rawxmlPS4 = file_get_contents($PS4URL, false, stream_context_create($arrContextOptions));
	if (! $xmlPS4=simplexml_load_string($rawxmlPS4)) {
		echo("PS4 Error - No result for <b>".$PS4TitleID."</b><br><br>");
		About();
		exit(1);
	}
}

echo("<!DOCTYPE html>
<html>
	<head>
		<title>PS3/PS4 Update Checker</title>
		<style>
		table, th, td {
			border: 1px solid black;
		}
		th, td { padding: 4px; }
		</style>
	</head>
<body>");

//print_r($xml);
//echo($xml->asXML());
// Always 'alive'?
//echo("Status: ".$xml->attributes()->status."<br>\n");
// TitleID + _something
//echo("TagName: ".$xml->tag->attributes()->name."<br>\n");
// May be related to whether update pops up when booting game
//echo("TagPopup: ".$xml->tag->attributes()->popup."<br>\n");
// May be related to being logged to psn or not
//echo("TagSignoff: ".$xml->tag->attributes()->signoff."<br>\n");
if ($scrapePS3) {
	$gameTitle="";
	echo("<table>
		<thead>
			<tr>
				<th>Version</th>
				<th>Size</th>
				<th>PS3 Min Version</th>
				<th>Checksum (SHA1)</th>
			</tr>
		</thead>
		<tbody>");
	foreach ($xmlPS3->tag->package as $package) {
	//	echo("<br>\n");
		echo("<tr>");
		// Version + Download link
		echo("<td style=\"text-align:center;\"><a href=".$package->attributes()->url."><b>".$package->attributes()->version."</b></a></td>");
		// File size
		echo("<td style=\"text-align:center;\">".formatBytes(floatval($package->attributes()->size))."</td>");
		// Minimum PS3 version
		echo("<td style=\"text-align:center;\">".$package->attributes()->ps3_system_ver."</td>");
		// SHA1
		echo("<td style=\"text-align:center;\">".$package->attributes()->sha1sum."</td>");
		// Could add showing localized version of the name but meh
		// https://www.psdevwiki.com/ps3/PARAM.SFO#TITLE_xx_.28for_localized_languages.29
		echo("</tr>");
		$gameTitle=$package->paramsfo->TITLE;
	}
	echo("	</tbody>
</table>\n");
	echo("<br>\n");
	echo("<b>Game:</b> ".$gameTitle."<br>\n");
	echo("<b><a href=\"$PS3URL\">TitleID</a>:</b> ".$xmlPS3->attributes()->titleid."<br>\n");
	echo("<br>\n");
	// https://a0.ww.np.dl.playstation.net/tpl/np/{productId}/{productId}-ver.xml
	// Buzz - 1 package: https://a0.ww.np.dl.playstation.net/tpl/np/BCES00647/BCES00647-ver.xml
	// Resistance 3 - 4 packages: https://a0.ww.np.dl.playstation.net/tpl/np/BCES01118/BCES01118-ver.xml
	echo("To check the PS3 checksum, you have to cut the last 32 bytes from the PKG file as that is the checksum. On Linux - 'head -c -32 file.pkg | sha1sum'<br>\n");
	echo("(Someone pointed out there can be packages where the end is also padded by zeroes, if that's the case the above command needs to be improved)<br>\n");
	echo("<br>");
}
if ($scrapePS4) {
	$gameTitle="";
	echo("<table>
		<thead>
			<tr>
				<th>Version</th>
				<th>Size</th>
				<th>PS4 min version</th>
				<th>digest</th>
				<th>Content ID</th>
				<th>Manifest URL</th>
			</tr>
		</thead>
		<tbody>");
	foreach ($xmlPS4->tag->package as $package) {
	//	echo("<br>\n");
		echo("<tr>");
		// Version + Download link
		echo("<td style=\"text-align:center;\"><a href=".$package->attributes()->url."><b>".$package->attributes()->version."</b></a></td>");
		// File size
		echo("<td style=\"text-align:center;\">".formatBytes(floatval($package->attributes()->size))."</td>");
		// Minimum PS4 version
		echo("<td style=\"text-align:center;\">".dechex((int)$package->attributes()->system_ver)."</td>");
		// ??? checksum
		echo("<td style=\"text-align:center;\">".$package->attributes()->digest."</td>");
		// Content ID
		echo("<td style=\"text-align:center;\">".$package->attributes()->content_id."</td>");
		// Manifest URL
		echo("<td style=\"text-align:center;\">".$package->attributes()->manifest_url."</td>");
		echo("</tr>");
		$gameTitle=$package->paramsfo->title;
	}
	echo("	</tbody>
</table>\n");
	echo("<br>\n");
	echo("<b>Game:</b> ".$gameTitle."<br>\n");
	echo("<b><a href=\"$PS4URL\">TitleID</a>:</b> ".$xmlPS4->attributes()->titleid."<br>\n");
	echo("<b><a href=\"https://orbispatches.com/$PS4TitleID\">OrbisPatches.com entry</a></b><br>\n");
	echo("<br>\n");
//	echo("To check the PS4 checksum, you have to cut the last 32 bytes from the PKG file as that is the checksum. On Linux - 'head -c -32 file.pkg | sha1sum'<br>\n");
//	echo("(Someone pointed out there can be packages where the end is also padded by zeroes, if that's the case the above command needs to be improved)<br>\n");
	echo("<br>");
}

About();
echo("</body>
</html>
");
?>

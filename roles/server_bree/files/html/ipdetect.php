<?php
// This script is AGPLv3 licensed.
function ipVersion($txt) {
	return strpos($txt, ":") === false ? 4 : 6;
}
function getOS() {
    global $user_agent;
    $os_platform    =   "Unknown OS Platform";
    $os_array       =   array(
                            '/windows nt 10/i'     =>  'Windows 10',
                            '/windows nt 6.3/i'     =>  'Windows 8.1',
                            '/windows nt 6.2/i'     =>  'Windows 8',
                            '/windows nt 6.1/i'     =>  'Windows 7',
                            '/windows nt 6.0/i'     =>  'Windows Vista',
                            '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
                            '/windows nt 5.1/i'     =>  'Windows XP',
                            '/windows xp/i'         =>  'Windows XP',
                            '/windows nt 5.0/i'     =>  'Windows 2000',
                            '/windows me/i'         =>  'Windows ME',
                            '/win98/i'              =>  'Windows 98',
                            '/win95/i'              =>  'Windows 95',
                            '/win16/i'              =>  'Windows 3.11',
                            '/macintosh|mac os x/i' =>  'Mac OS X',
                            '/mac_powerpc/i'        =>  'Mac OS 9',
                            '/linux/i'              =>  'Linux',
                            '/ubuntu/i'             =>  'Ubuntu',
                            '/iphone/i'             =>  'iPhone',
                            '/ipod/i'               =>  'iPod',
                            '/ipad/i'               =>  'iPad',
                            '/android/i'            =>  'Android',
                            '/blackberry/i'         =>  'BlackBerry',
                            '/webos/i'              =>  'Mobile'
                        );
    foreach ($os_array as $regex => $value) {
        if (preg_match($regex, $user_agent)) {
            $os_platform    =   $value;
        }
    }
    return $os_platform;
}

function getBrowser() {
	global $user_agent;
	$browser = "Unknown Browser";
	$browser_array = array(
		'/msie/i'       =>  'Internet Explorer',
		'/trident/i'    =>  'Internet Explorer(trident)',
		'/firefox/i'    =>  'Firefox',
		'/safari/i'     =>  'Safari',
		'/chrome/i'     =>  'Chrome',
		'/edge/i'       =>  'Edge',
		'/opr/i'        =>  'Opera',
		'/netscape/i'   =>  'Netscape',
		'/maxthon/i'    =>  'Maxthon',
		'/konqueror/i'  =>  'Konqueror',
		'/mobile/i'     =>  'Handheld Browser',
		'/vivaldi/i'    =>  'Vivaldi'
	);
	foreach ($browser_array as $regex => $value) {
		if (preg_match($regex, $user_agent)) {
			$browser = $value;
		}
	}
	return $browser;
}

//If we're on the ip subdomain and not the ipv4 or ipv6 one, do everything
if($_SERVER['HTTP_HOST'] == "ip.rys.rs") {
	printf("<!doctype html>\n");
	printf("<strong>Your IPv4:</strong> <span id=\"main\"></span><br>");
	printf("<strong>Your IPv6:</strong> <span id=\"subby\"></span><br>");
	printf("<strong>Your local IPs are:</strong><br> <span id=list>-</span><br>");
	$user_agent = $_SERVER['HTTP_USER_AGENT'];
	$user_os = getOS();
	$user_browser = getBrowser();
	$device_details = "<strong>Operating System: </strong>".$user_os."<br><strong>Browser: </strong>".$user_browser;
	printf($device_details);
	echo("<br><strong>User agent: </strong>".$_SERVER['HTTP_USER_AGENT']."<br>");
	$serverProtocol = $_SERVER['SERVER_PROTOCOL'];
	printf("<strong>You are connected via: </strong>".$serverProtocol);
	echo("<br><br>You can also access or curl IPv4 only accessible domain <a href=\"https://ipv4.rys.rs\">ipv4.rys.rs</a> or IPv6 only accessible domain <a href=\"https://ipv6.rys.rs\">ipv6.rys.rs</a>, the response is only an IP.");
	echo("<br><br><a href=\"https://gitlab.com/C0rn3j/configs/tree/master/bree\">Hey, this script is free and open source under the AGPLv3 license!</a>");
	// Get IPv4 and IPv6 via AJAX
	echo("
<script crossorigin=\"anonymous\" src=\"https://code.jquery.com/jquery-3.7.0.min.js\"></script>
<script>
	function getIP(subdomain) {
		return $.get(\"https://\"+ subdomain +\".rys.rs\");
	}
	getIP(\"ipv4\").then(res => $(\"#main\").text(res));
	getIP(\"ipv6\").then(res => $(\"#subby\").text(res));
</script>
");
	// Get all local adresses on both IPv4 and IPv6 via WebRTC
	// Why/how this works https://www.whatismybrowser.com/detect/what-is-my-local-ip-address
	echo('<script type="text/javascript" src="/ipdetect.js"></script>');
	// If you're copying my tool remove this section, it's my analytics.
	echo("
<!-- Matomo -->
<script type=\"text/javascript\">
	var _paq = _paq || [];
	_paq.push(['trackPageView']);
	_paq.push(['enableLinkTracking']);
	(function() {
		var u=\"//analytics.rys.pw/\";
		_paq.push(['setTrackerUrl', u+'js/']);
		_paq.push(['setSiteId', '2']);
		var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
		g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'js/'; s.parentNode.insertBefore(g,s);
	})();
</script>
<!-- End Matomo Code -->
");
}
// Else if current site is not on the ip subdomain it is ipv4 or ipv6, so just show the IP. This is also very curl friendly.
else {
	printf($_SERVER['REMOTE_ADDR']);
}
?>

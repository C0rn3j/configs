'use strict';

document.getElementById('list').innerHTML = "[You need a modern browser that supports WebRTC and has it enabled!]";

var addrs = [];

var list = document.getElementById('list');

function grepSDP(sdp) {
	sdp.split('\r\n').forEach(function (line) {
		var parts = line.split(' ');
		var addressIndex = null;
		var type = null;
		if (line.indexOf('a=candidate') !== -1) {
			addressIndex = 4;
			type = 'host';
		} else if (line.indexOf('c=') !== -1) {
			addressIndex = 2;
		} else {
			return;
		}
		var addr = parts[addressIndex];
		if (type && type !== parts[7]) {
			return;
		}

		// formerly updateDisplay:
		if (addrs.indexOf(addr) !== -1 || addr === '0.0.0.0') {
			return;
		}
		addrs.push(addr);
		list.innerHTML =
			addrs.join('<br />') || 'n/a';
	});
}

// NOTE: window.RTCPeerConnection is "not a constructor" in FF22/23
var RTCPeerConnection =
	/* window.RTCPeerConnection || */
	window.webkitRTCPeerConnection ||
	window.mozRTCPeerConnection;

if (RTCPeerConnection) {
	var rtc = new RTCPeerConnection({
		iceServers: []
	});
	rtc.createDataChannel('', {
		reliable: false
	});
	rtc.addEventListener('icecandidate', function (e) {
		if (e.candidate) {
			grepSDP('a=' + e.candidate.candidate);
		}
	});
	rtc.createOffer(function (desc) {
		grepSDP(desc.sdp);
		rtc.setLocalDescription(desc);
	}, function (err) {
		console.warn('offer failed', err);
	});
} else {
	list.innerHTML = '<code>' +
		'ifconfig | grep inet | grep -v inet6 | cut -d" " -f2 | tail -n1' +
		'</code>';
	list.nextSibling.textContent =
		'In Chrome and Firefox your IP should display automatically,' +
		'by the power of WebRTCskull.';
}

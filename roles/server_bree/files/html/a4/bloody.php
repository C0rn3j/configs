<?php
//protocolDecode - takes a protocol (atm just supports 'A4tech bloody') and a protocol string like:
//                 "0701 782c 5060 0000 0124 0826 4b28 0926 0240 0125 00c1 0298 00b8 56e0 0826 1428 0124 0826 5028 0926 ffff ffff ffff ffff ffff ffff ffff ffff ffff ffff ffff ffff";
//                 If it matches a predefined rule, it returns a name of the function and a description
function protocolDecode($protocol, $protocolString, $protocolType) {
	if($protocolType != "Query" && $protocolType != "Response") {
		return array ("N/A", "N/A");
	}
	if ($protocol = "A4Tech bloody") {
		switch ($protocolString) {
			case (preg_match('/^0705 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000/', $protocolString) ? true : false) :
		$decodedTitle = "Get some info";
		$decodedText = "Query:
$protocolString
Example response:
0705 0000 0000 0000 0100 e803 002a 002c 003e 5512 f03f 8b12 0000 0401 759f 2fe1 ffff 094a 0000 5aff ff07 8a5c feff 0000 0000 0000 0000 0000 0000 0000 0000 0000
                         BBCC                                         AAAA AAAA  DDD JJJJ           FFGG HHII  E
AAAA AAAA - Reversed Mouse ID. In the example: E12F9F75
BBCC - Number of trial clicks left (for unlicensed mice), BB goes down first and it underflows into CC. Starts at e8 03 which is a 1000.
DDD, E - DDD is fff during trial, 019 when licensed and 0ff when preactivated. E is e during trial, c when licensed and f when preactivated.

The mouse communicates some secret bytes to the activation server when requesting the activation key. The following is an example in dec - 0, 6, 254, 26, 18953, 138, 92. First value is always zero and second value seems to be hardcoded Bloody version.
3rd, 4th, 6th and 7th are different for every firmware (even across different kernels (Cores). 5th seems to always be 18953.
FF - 3rd secret value
GG - 4th secret value
HH - 6th secret value
II - 7th secret value
JJJJ - 5th secret value (static so far) - reversed, just like mouse ID


Mouse ID is generated 'randomly' on first connect to bloody software. Mouse PID is then set based on Mouse ID.

What other information is contained is unknown.";
		break;
			case (preg_match('/^0705 .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... ..../', $protocolString) ? true : false) :
		$decodedTitle = "Get some info";
		$mouseID=substr($protocolString,77, 2).substr($protocolString,75, 2).substr($protocolString,72, 2).substr($protocolString,70, 2);
		$decodedText = "Response:
$protocolString
                         BBCC                                         AAAA AAAA  DDD JJJJ           FFGG HHII  E
AAAA AAAA - Reversed Mouse ID: $mouseID
BBCC - Number of trial clicks left (for unlicensed mice), BB goes down first and it underflows into CC. Starts at e8 03 which is a 1000.
DDD, E - DDD is fff during trial, 019 when licensed and 0ff when preactivated. E is e during trial, c when licensed and f when preactivated.

The mouse communicates some secret bytes to the activation server when requesting the activation key. The following is an example in dec - 0, 6, 254, 26, 18953, 138, 92. First value is always zero and second value seems to be hardcoded Bloody version.
3rd, 4th, 6th and 7th are different for every firmware (even across different kernels (Cores). 5th seems to always be 18953.
FF - 3rd secret value
GG - 4th secret value
HH - 6th secret value
II - 7th secret value
JJJJ - 5th secret value (static so far) - reversed, just like mouse ID

Mouse ID is generated 'randomly' on first connect to bloody software. Mouse PID is then set based on Mouse ID.

What other information is contained is unknown.";
		break;
			case (preg_match('/^0711 .... 8... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... ..../', $protocolString) ? true : false) :
		$decodedTitle = "Set backlight intensity";
		$backlightIntensity=substr($protocolString,22, 1);
		$decodedText = "$protocolType:
$protocolString
          A         BB
A  - get/set bit. 0 means get, 8 means set.
BB - From 0 to 3, 0 is off, 3 is the brightest. Anything else means off.

Backlight intensity: $backlightIntensity";
		break;
			case (preg_match('/^0711 .... 0... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... ..../', $protocolString) ? true : false) :
		$decodedTitle = "Get backlight intensity";
		$backlightIntensity=substr($protocolString,22, 1);
		$decodedText = "$protocolType:
$protocolString
          A         BB
A  - get/set bit. 0 means get, 8 means set.
BB - From 0 to 3, 0 is off, 3 is the brightest. Anything else means off.

Backlight intensity: $backlightIntensity";
		break;
			case (preg_match('/^071F .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... ..../', $protocolString) ? true : false) :
		$decodedTitle = "Get firmware info (Bloody7+)";
		$decodedText = "$protocolType:
$protocolString
                      AA BBCC DD   EE   FF
DD - Firmware version - as seen in the FW filename and what bcdDevice should report as(since Bloody7?)";
		break;
			case (preg_match('/^0703 0600 .... .... .1.. .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... ..../', $protocolString) ? true : false) :
		$decodedTitle = "Enter RGB edit mode";
		$decodedText = "$protocolType:
$protocolString
  AA AA             BB
AA AA - 03 06 - protocol
BB - set to 01 for edit mode(can control individual LEDs through commands) or to 00 to have the mouse modes running instead";
break;
			case (preg_match('/^0703 0600 .... .... .0.. .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... ..../', $protocolString) ? true : false) :
		$decodedTitle = "Exit RGB edit mode";
		$decodedText = "$protocolType:
$protocolString
AA AA             BB
AA AA - 03 06 - protocol
BB - set to 01 for edit mode(can control individual LEDs through commands) or to 00 to have the mouse modes running instead";
		break;
			case (preg_match('/^0703 0601 .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... ..../', $protocolString) ? true : false) :
		$decodedTitle = "Set constant/breathing light in RGB edit mode";
		$decodedText = "$protocolType:
$protocolString
  AA AAAA      BBCC
AA AAAA - 03 0601 - protocol
BB - 01 is breathing, 00 is static
CC - how fast is the breathing, from 01 to 04
     00 when BB is 00.";
		break;
			case (preg_match('/^0703 0602 .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... ..../', $protocolString) ? true : false) :
		$decodedTitle = "Set RGB LEDs";
		$decodedText = "$protocolType:
$protocolString
  AA AAAA           1111 1122 2222 3333 3344 4444 5555 5566 6666 7777 7788 8888
                    Every LED has ff:ff:ff which controls R:G:B intensity
AA AAAA - 03 0602 - protocol";
		break;
			case (preg_match('/^0703 0605 .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... ..../', $protocolString) ? true : false) :
		$decodedTitle = "Get mouse mode";
		$decodedText = "$protocolType:
$protocolString
                    AABB CC

AA - Brightness? 00 off, 03 max?
BB - Profile show mode byte - is 02 when in it, 00 when not in it.
CC - Which animation is the mouse set to if in RGB mode or RGB edit mode";
		break;
			case (preg_match('/^0710 .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... ..../', $protocolString) ? true : false) :
		$decodedTitle = "Set/Get RNG bytes";
		$decodedText = "$protocolType:
$protocolString
                    AAAA
AAAA - RNG bytes. Calling this sets them and they persist until firmware restart. The resulting bytes are based on time since the mouse firmware started running.
       They are used for various DRM functions, including generating Mouse ID and license activation.";
		break;
			case (preg_match('/^0700 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... ..../', $protocolString) ? true : false) :
		$decodedTitle = "Restart firmware";
		$decodedText = "$protocolType:
$protocolString

Restarts firmware. Seems to be used when flashing kernel.";
		break;
		case (preg_match('/^0702 002A 0060 .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... ..../', $protocolString) ? true : false) :
		$decodedTitle = "Read data off an address - P93 Sensitivity (Bloody6)";
		$decodedText = "$protocolType:
$protocolString
       AA BBDD      CCCC EEEE EEEE HHHH GGHH IIHH JJJJ JJJJ HHHH KKKK KKKK HHHH LLLL LLLL HHHH MMMM MMMM HHHH NNNN NNNN HHHH HHHH HHHH HHHH HHHH HHHH HHHH HHHH
AA BB - Address to read data from. BB overflows into AA.
DD - Always 60?
CCCC - always a4a4
EEEE EEEE - Magic 8 sensitivity bytes
GG - Number of enabled modes. 04 max for 5 modes, 00 for 1 mode.
HH - Padding? Or X/Y axes?
II - Selected mode. 00 for 1st, 04 for 5th
JJJJ JJJJ - X and Y sensitivity for the 1st mode
KKKK KKKK - X and Y sensitivity for the 2nd mode
LLLL LLLL - X and Y sensitivity for the 3rd mode
MMMM MMMM - X and Y sensitivity for the 4th mode
NNNN NNNN - X and Y sensitivity for the 5th mode

There's 112 chars per line, and 1 address position jumps 4 chars.
Meaning address X will give you data Y and X+1 is going to give you data Y +- 4 characters.
The data repeats itself after whole memory is read. The cutoff point is different for different chips (0x4000 for P93)";
		break;
		case (preg_match('/^0702 00.. ..60 .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... ..../', $protocolString) ? true : false) :
		$decodedTitle = "Read data off an address (Bloody6)";
		$decodedText = "$protocolType:
$protocolString
       AA BBDD      CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC
AA BB - Address to read data from. BB overflows into AA.
CCCC CCCC C... - data
DD - Always 60?

There's 112 chars per line, and 1 address position jumps 4 chars.
Meaning address X will give you data Y and X+1 is going to give you data Y +- 4 characters.
The data repeats itself after whole memory is read. The cutoff point is different for different chips (0x4000 for P93)";
		break;
			case (preg_match('/^0701 183F F460 .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... ..../', $protocolString) ? true : false) :
		$decodedTitle = "Set mouse ID";
		$decodedText = "$protocolType:
$protocolString
                    AAAA AAAA
AAAA AAAA - Mouse ID to set, reversed.

This function takes a lot more than just the mouse ID to work, but so far it is not known how exactly to get it working.";
		break;
			case (preg_match('/^0701 78.. .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... ..../', $protocolString) ? true : false) :
		$decodedTitle = "Write data";
		$decodedText = "$protocolType:
$protocolString
     EEAA BBDD      CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC CCCC
AA BB - Address to write data to. BB overflows into AA.
CCCC CCCC C... - Data to write
DD - This is e0 when BB is 00, 60 otherwise. It's also e0 when BB is 80 but has no effect and seems to work just as fine when set to 60.
EE - Kind of a mode? 78 for writing Sensitivity(only?) data, 18 for setting mouse ID, 00 for entering flashing mode.... These addresses differ across different models of mice.

When DD is e8 (first write command?) the whole memory block that is going to be written to is going to get formated to 'ff ff ..' from AA 10 to AA f0, which is 1024 characters.

WRITING DATA TO THE MOUSE CAN PERMANENTLY BRICK IT. DO NOT PLAY AROUND WITH THIS.";
		break;
			case (preg_match('/^0701 0001 0060 .... 0000 .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... ..../', $protocolString) ? true : false) :
		$decodedTitle = "Write data - Set mouse to flashing mode (Bloody 6)";
		$decodedText = "$protocolType:
$protocolString
       AA BBDD      CCCC
AA BB - Address to write data to. BB overflows into AA.
CCCC - Data to write

This writes zeroes instead of a4a4 and makes mouse switch to flashing mode on firmware restart.

WRITING DATA TO THE MOUSE CAN PERMANENTLY BRICK IT. DO NOT PLAY AROUND WITH THIS.";
		break;
			case (preg_match('/^0704 .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... ..../', $protocolString) ? true : false) :
		$decodedTitle = "??? Freezes colors";
		$decodedText = "$protocolType:
$protocolString
                          A
A - Setting this to 2 freezes RGB animation colors. Anything else unfreezes them. Software sets 1 for unfreeze.

Actual purpose and effect of this function is unknown.
Seems to be called for freezing before writing data and unfreezing after data was written (0701)";
		break;
		case (preg_match('/^0712 .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... ..../', $protocolString) ? true : false) :
		$decodedTitle = "Something to do with calibration";
		$decodedText = "$protocolType:
$protocolString
                    A
A - 8 start?, 0 end?.

It has something to do with getting 0713. Think it's used for the self-calibration when moving in the zig-zag pattern.";
		break;
		case (preg_match('/^0713 .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... ..../', $protocolString) ? true : false) :
		$decodedTitle = "Get/Set mouse calibration";
		$decodedText = "$protocolType:
$protocolString
          A         BCDF
A - 8 set, 0 get.
B - 8 preset, c calibration. Can also be 4 and b???
C - Calibration to get/set, from 0 to f, f being 1, 0 being 16.
D - Only present while getting. Always 8?
F - Only present while getting. F is what the mouse thinks the calibration should be? Bloody has a calibration function where you move the mouse in zigzag pattern. It sets it to this value after it's done.

It seems this function is not reliable for getting calibration at all times, magic bytes should be used for that, but it is reliable for setting it.";
		break;

		case (preg_match('/^0720 0000 0000 000. 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000/', $protocolString) ? true : false) :
			$decodedTitle = "Enable/Disable writing settings(probably)";
			$decodedText = "$protocolType:
$protocolString
  AA              B
AA - 20 - protocol
B - 1 = enable, 0 = disable

Seems to happen right before and after settings write calls";
		break;
		case (preg_match('/^070A .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... ..../', $protocolString) ? true : false) :
		$decodedTitle = "??? Happens after setting sensor calibration in bloody v6, also when resetting settings to default right before firmware reset command";
		$decodedText = "$protocolType:
$protocolString
????";
		break;
		case (preg_match('/^070C .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... ..../', $protocolString) ? true : false) :
		$decodedTitle = "Set used and selected profiles and key response(sensitivity)";
		$decodedText = "$protocolType:
$protocolString
                     ABC DD
A - Number of profiles enabled
B - ???
C - Profile selected. 0 is 1st, 6 is 7th.
DD - 01 to 10 - Key response 1 to 16 (Sensitivity)";
		break;
		case (preg_match('/^070D .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... .... ..../', $protocolString) ? true : false) :
		$decodedTitle = "Set DPI";
		$decodedText = "$protocolType:
$protocolString
                    AABB CCCC   DD
AA   - How many more modes are enabled, from 00 to 04 (00 = 1 mode, 04 = 5 modes total)
BB   - mode selected, goes from 00 to 04 - this sets what mode is going to be next when you press the 'Next DPI mode' button.
CCCC - Value of Hz selected. It doesn't actually use the last 2 bytes on P series
       the value is calculated by the retarded bitmask algo and is different on various mice???
DD   - Response rate. 00 for 3Hz, 01 for 1000hz, 02 for 500Hz, 04 for 250Hz, 08 for 125Hz.
       Can be set to values like 03 for 666Hz[average] (1x 1000Hz, 1x 333Hz), which is something the v6 doesn't do (cause why would it)";
		break;
			default:
		$decodedTitle = "Unknown";
		$decodedText = "Function unknown, could not decrypt";
		break;
		}
		return array ($protocolType.": ".$decodedTitle, $decodedText);
	}
}

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
		<!--We want monospaced text to see characters where they should be, right under each other.
		We also want no text wrapping.
-->
<style>
			html {
				font-family: monospace;
				white-space: nowrap;
			}
			.pointer {
				cursor: pointer;
			}
			.monospace {
				font-family: monospace;
				white-space: nowrap;
			}
			.popover {
				max-width: none;
			}
			.popover-body {
				white-space: pre-wrap;
				font-family: monospace;
			}
		</style>
		<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
		<!-- // If you're copying my tool remove this section, it's my analytics. -->
		<!-- Matomo -->
		<script type="text/javascript">
			var _paq = _paq || [];
			_paq.push(['trackPageView']);
			_paq.push(['enableLinkTracking']);
			(function() {
				var u="//analytics.rys.pw/";
				_paq.push(['setTrackerUrl', u+'js/']);
				_paq.push(['setSiteId', '3']);
				var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
				g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'js/'; s.parentNode.insertBefore(g,s);
			})();
		</script>
		<!-- End Matomo Code -->
	</head>

	<body>
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<p>This script is available under the AGPL version 3 license on: <a href="https://gitlab.com/C0rn3j/configs/tree/master/ansible/serverPlaybooks/roles/bree/files/html/a4">https://gitlab.com/C0rn3j/configs/tree/master/ansible/serverPlaybooks/roles/bree/files/html/a4</a>
					<br>Made for <a href="https://gitlab.com/C0rn3j/a4tech_bloody_p85_driver">this</a> project.
					<br>To process the pcapng files into text this form understands, you'll need the following script: <a href="https://gitlab.com/C0rn3j/configs/blob/master/ansible/serverPlaybooks/roles/bree/files/html/a4/a4.sh">https://gitlab.com/C0rn3j/configs/blob/master/ansible/serverPlaybooks/roles/bree/files/html/a4/a4.sh</a>
					<br>
					<br>Queries are marked blue (host>device), responses are marked green (device>host).
					<br>Predefined rules in the index.php files are applied to the protocol and known/decrypted info is in a box that appears when you hover over the line.
					<br>
					<br><a href="https://gitlab.com/C0rn3j/configs/raw/master/ansible/serverPlaybooks/roles/bree/files/html/a4/parsedexample.txt">Example a4.sh output</a>
					<br>Enter output from the a4.sh parser:</p>
					<form action="" method="post" >
						<textarea rows="5" cols="80" name="cmd"> </textarea><br>
						<input class="btn btn-default" type="submit" value="Submit"/>
					</form><br>
		<?php
		ini_set('display_startup_errors', 1);
		ini_set('display_errors', 1);
		error_reporting(-1);

		if(!isset($_POST['cmd']))
			exit();
		// Load the Bloody 7 protocol funtcion file
		require_once('bloody.php');
		$input=$_POST['cmd'];
		$inputarray = preg_split("/\\r\\n|\\r|\\n/", $input);
		$color="#000000";
		$protocolType="N/A";
		foreach($inputarray as $row) {
			$row = strtoupper($row);
			// If this matches HOST it can still be a response, the latter check will override it if so
			if (strpos($row, 'SUBMIT') !== false || strpos($row, 'HOST') !== false) {
				$color="#000080";
				$protocolType="Query";
			}
			if (strpos($row, 'COMPLETE') !== false || strpos($row, 'HOST	USB') !== false) {
				$color="#006400";
				$protocolType="Response";
			}
			if (strpos($row, 'CONTROL') !== false || strpos($row, 'INTERRUPT') !== false || strpos($row, 'BULK') !== false) {
				continue; // Line has a string we're ignoring, therefore do not echo it out
			}
			if (strpos($row, 'HOST') !== false) {
				$row = substr(strstr($row,'	07'), 1);
			}
			list($tooltipTitle,$tooltipText) = protocolDecode("A4Tech bloody", $row, $protocolType);
			// Echo the final line with tooltips and everything
			echo('<a class="pointer popoverData monospace" data-content="'.$tooltipText.'" data-placement="top" data-original-title="'.$tooltipTitle.'" data-trigger="hover" style="color:'.$color.';">'.$row.'</a><br/>');
		}
		?>
		<script>
			$(document).ready(function() {
				$('.popoverData').popover({ trigger: "hover" });
			});
		</script>
				</div>
		</div>
	</div>
	</body>
</html>

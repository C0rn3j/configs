<?php
//curl -X POST -A "User-Agent: Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)" -d @curlmeb64 http://snu.lge.com/CheckSWAutoUpdate.laf
function formatBytes(float $size, $precision = 2) {
	$base = log($size, 1024);
	$suffixes = array('', 'K', 'M', 'G', 'T');
	return round(pow(1024, $base - floor($base)), $precision) .''. $suffixes[floor($base)];
}
$url = 'http://snu.lge.com/CheckSWAutoUpdate.laf';
//$tvModel="HE_DTV_W15B_AFADATAA"; # Ruskyi
$tvModel="HE_DTV_W18A_AFADABAA"; # Mine
$productName="webOSTV 4.0";
$majorVer="04";
$country="GB";
$languageCode="en-GB";
//$country="BR8";
//$languageCode="pt-BR";

// The API can be slow as hell to the point of timing out.
// DEVICE_ID -> MAC address - not used, so we throw in an obviously fake one
// Minor ver -> Set to 00.00 to always get the update link if the rest of the parameters are OK.
// Major ver -> You'll get the latest minor update for this major version.
//              If you set a version too low or too high you will get an empty response.
//              The latest version +1 can still work and give the link for latest version.
// PRODUCT_NM -> Is used
$data = <<<EOF
<REQUEST>
<PRODUCT_NM>$productName</PRODUCT_NM>
<MODEL_NM>$tvModel</MODEL_NM>
<SW_TYPE>FIRMWARE</SW_TYPE>
<MAJOR_VER>$majorVer</MAJOR_VER>
<MINOR_VER>00.00</MINOR_VER>
<COUNTRY>$country</COUNTRY>
<COUNTRY_GROUP>EU</COUNTRY_GROUP>
<DEVICE_ID>de:ad:be:ef:de:ad</DEVICE_ID>
<AUTH_FLAG>N</AUTH_FLAG>
<IGNORE_DISABLE>N</IGNORE_DISABLE>
<ECO_INFO>01</ECO_INFO>
<CONFIG_KEY>00</CONFIG_KEY>
<LANGUAGE_CODE>$languageCode</LANGUAGE_CODE></REQUEST>

EOF;

$cURL = curl_init();
curl_setopt($cURL, CURLOPT_URL, $url);
curl_setopt($cURL, CURLOPT_CUSTOMREQUEST, 'POST');
curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
curl_setopt($cURL,CURLOPT_USERAGENT,'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)');

$payload = base64_encode($data);

curl_setopt($cURL, CURLOPT_POSTFIELDS, $payload);
curl_setopt($cURL, CURLOPT_HTTPHEADER, array(
	'Content-Type: application/x-www-form-urlencoded',
	'Content-Length: ' . strlen($payload)
));
$result = curl_exec($cURL);
curl_close($cURL);

// Parse xml request
$p = xml_parser_create();
xml_parse_into_struct($p, base64_decode($result), $vals, $index);
xml_parser_free($p);

foreach($vals as $valArray) {
	switch ($valArray['tag']) {
		case "RESPONSE":
			// Nothing useful, throw away
			break;
		case "IMAGE_URL":
			if(! isset($valArray['value'])) {
				echo("Empty response from server!".PHP_EOL);
				echo("Raw 'zagotovki' response: ".PHP_EOL);
				echo(base64_decode($result));
				exit(1);
			}
			$imageName = $valArray['value'];
			break;
		case "CDN_URL":
			$downloadURL = $valArray['value'];
			break;
		case "UPDATE_MAJOR_VER":
			$updateMajorVer = $valArray['value'];
			break;
		case "UPDATE_MINOR_VER":
			$updateMinorVer = $valArray['value'];
			break;
		case "IMAGE_NAME":
			$imageName = $valArray['value'];
			break;
		case "IMAGE_SIZE":
			$imageSize = $valArray['value'];
		case "MSG":
			$msgCode = $valArray['value'];
			break;
		default:
			echo("Not processing ".$valArray['tag']).PHP_EOL;
	}
}
echo("Found image with version $updateMajorVer.$updateMinorVer with size ".formatBytes($imageSize).".".PHP_EOL."Download at $downloadURL".PHP_EOL);
echo("Raw 'zagotovki' response: ".PHP_EOL);
echo(base64_decode($result));
?>

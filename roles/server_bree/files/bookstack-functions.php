<?php
// Plugin by ssddanbrown that let's me use rys.rs/page if said page exists in a book somewhere, instead of having to always use the full long URLs

use BookStack\Entities\Models\Page;
use BookStack\Theming\ThemeEvents;
use BookStack\Facades\Theme;
use Illuminate\Http\Request;

// This will be the content of the functions.php file you'll have
// after following the getting started instructions here:
// https://github.com/BookStackApp/BookStack/blob/release/dev/docs/logical-theme-system.md

Theme::listen(ThemeEvents::WEB_MIDDLEWARE_AFTER, function (Request $request, $response) {
	// Ignore if not 404 or a GET response.
	if ($response->getStatusCode() !== 404 && $request->getMethod() !== 'GET') {
		return $response;
	}

	// Get a path and ignore if more than one path segment.
	$path = $request->decodedPath();
	if (str_contains('/', $path)) {
		return $response;
	}

	// DokuWiki replaces spaces with _ and BookStack with -, so replace them for this search
	$replaces = array('_', '-');
	$name = str_replace($replaces, ' ', $path);
	// Find a matching page and redirect to that if existing
	// otherwise return the default response.
	$page = Page::visible()->where('name', '=', $name)->first();
	return $page ? redirect($page->getUrl()) : $response;
});

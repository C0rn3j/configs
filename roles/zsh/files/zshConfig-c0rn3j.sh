export EDITOR=nano
export VISUAL=nano
export GOPATH=/home/${USERNAME}/Golang
export GRC_ALIASES=true # otherwise /etc/profile.d/grc.sh refuses to execute
# Colored man pages as per https://github.com/sharkdp/bat#man
export MANPAGER="sh -c 'col -bx | bat -l man -p'"
export MANROFFOPT="-c"

# source /etc/profile.d/grc.sh - this should already be done for login shells, but somehow doesn't seem to apply in KDE and lxc exec(probably because it's not a login shell?)
# We do this first as it otherwise overrides our following aliases
# Note: For ip we use grc instead of 'ip -c', as the -c flag is not as featureful: https://github.com/garabik/grc/issues/180
source /etc/profile.d/grc.sh

# https://stackoverflow.com/a/68570391/8962143
function urldecode() {
	echo "${1}" | python -c "import sys; from urllib.parse import unquote; print(unquote(sys.stdin.read()));"
}
function urlencode() {
	echo "${1}" | python -c "import sys; from urllib.parse import quote; print(quote(sys.stdin.read()));"
}

# ls, but with colors!
alias ls="lsd"
# Bat is a prettier cat with syntax highlighting and more
alias cat="bat"
# Make sure dd always has a progress bar
alias dd="dd status=progress"
# Duf is a df replacement that has usage bars and colors
alias df="duf -avail-threshold '0,0' -usage-threshold '0.90,0.95' -only local -output 'type, mountpoint, usage, used, avail, filesystem, inodes_usage'"
# Drop caches and sync
alias clearcaches="sudo sync && echo 3 | sudo tee /proc/sys/vm/drop_caches"
# Screenfetch, neofetch, fastfetch... just alias the one that's currently hip to fetch so I don't have to keep relearning it
alias fetch="fastfetch"
# Update OS, AUR, Flatpak
alias update="paru && flatpak update -y && flatpak remove --unused"
# Set default g++ flags to be strict
alias g++="colourify g++ -pedantic -Wall -Wextra -Wcast-align -Wcast-qual -Wctor-dtor-privacy -Wdisabled-optimization -Wformat=2 -Winit-self -Wlogical-op -Wmissing-declarations -Wmissing-include-dirs -Wnoexcept -Wold-style-cast -Woverloaded-virtual -Wredundant-decls -Wshadow -Wsign-conversion -Wsign-promo -Wstrict-null-sentinel -Wstrict-overflow=5 -Wswitch-default -Wundef -Werror -Wno-unused -Og"
# More verbose lsblk
alias lsblk='colourify lsblk -o +fstype,label,uuid,model'
alias dmesg='sudo dmesg'
alias blkid='sudo grc -se blkid'
alias corn-info='sudo corn-info'
alias makePKGBUILD="corn-makepkg"
# Duf/dusk can show tmpfs, maybe retire this
alias dutmpfs="duf -only-fs tmpfs -avail-threshold '0,0'" # for i in $(mount | grep ^tmpfs | awk {"print \$3"} ); do sudo du -sh ${i} 2>/dev/null; done'
alias reboot-to-firmware='systemctl reboot --firmware-setup'
alias fdupenames='find -type f -print0 | awk -F/ '\''BEGIN { RS="\0" } { n=$NF } k[n]==1 { print p[n]; } k[n] { print $0 } { p[n]=$0; k[n]++ }'\'''

# Execute corn-info on new terminals - sudo is not needed on root but eh
sudo /usr/local/bin/corn-info | tee /dev/null

# Show used alias for the command (if applies) and append it as a comment to the end of the line, this will show in history.
function execute-with-alias-comment() {
	# Exit if the buffer is empty
	if [[ -z $BUFFER ]]; then
		zle accept-line
		return
	fi

	local newBuffer=''
	local parts=("${(@s/|/)BUFFER}")

	for part in "${parts[@]}"; do
		local tempBuffer=''
		# Remove leading and trailing whitespace from each part
		part=${part## }
		part=${part%% }

		local cmd=${part%% *}
		local rest=${part#* }
		local isAlias=''
		isAlias=$(alias "$cmd" 2> /dev/null)

		if [[ -n $isAlias ]]; then
			local aliasCMD=''
			# Extract the alias command, using xargs to remove escapes
			aliasCMD=$(echo "$isAlias" | xargs | cut -d= -f2)
			# Check if the comment is already present in the part
			tempBuffer="<$cmd> aliased to «$aliasCMD»"
			if [[ -n $newBuffer ]]; then
				newBuffer="$newBuffer ‖ $tempBuffer"
			else
				newBuffer=" # $tempBuffer"
			fi
		fi
	done

	if [[ -n $newBuffer ]]; then
		zle end-of-line
		# Print the extra stuff
		print -Pn "%F{yellow}%B$newBuffer%b%f"
	fi

	# Execute the command
	zle accept-line
}

# Register the plugin
zle -N execute-with-alias-comment

# Bind the plugin to ENTER
bindkey '^M' execute-with-alias-comment

# Autosuggest previously used commands - TODO this clashes if there is both an unfinished autosuggestion and an alias comment, their order is wrong
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
# Syntax highlighting for zsh, line needs to be at the very end of file
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

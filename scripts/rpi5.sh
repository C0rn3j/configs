#!/usr/bin/env bash

# Things may be presuming installing from a RPi5 official OS booted on SD card
# https://archlinuxarm.org/platforms/armv8/generic
# https://archlinuxarm.org/forum/viewtopic.php?f=30&t=16659
# https://archlinuxarm.org/forum/viewtopic.php?f=60&t=16627&sid=0a3aa7973f4398240823237f3160368e&start=10#p71972
# https://forums.raspberrypi.com/viewtopic.php?t=344892
# https://kiljan.org/2023/11/24/arch-linux-arm-on-a-raspberry-pi-5-model-b/

# https://archlinuxarm.org/about/mirrors

drive='sda'         # DEFINE THIS
WIFISSID='MyAP'     # DEFINE THIS
WIFIPASSWORD='1234' # DEFINE THIS

# using fat and btrfs, so tools necessary for them, and arch scripts and pacman for pacstrap
apt-get install -y arch-install-scripts btrfs-progs dosfstools pacman-package-manager

wipefs --all "/dev/${drive}"
parted -s "/dev/${drive}" mklabel gpt
parted -s --align optimal "/dev/${drive}" mkpart ESP fat32 1MiB 1025MiB
parted -s "/dev/${drive}" set 1 boot on
parted -s --align optimal "/dev/${drive}" mkpart primary btrfs 1025MiB 100%
mkfs.fat -F32 /dev/${drive}1
mkfs.btrfs /dev/${drive}2

mount /dev/${drive}2 /mnt
mount --mkdir /dev/${drive}1 /mnt/boot
wget -O /tmp/ArchLinuxARM-aarch64-latest.tar.gz https://os.archlinuxarm.org/os/ArchLinuxARM-aarch64-latest.tar.gz
bsdtar -xpf ArchLinuxARM-aarch64-latest.tar.gz -C /mnt

# no HTTPS (bad cert address)
# Repo names gotten from http://mirror.archlinuxarm.org/aarch64/
# https://archlinuxarm.org/about/package-signing
cat >> /etc/pacman.conf << 'EOF'
[alarm]
SigLevel = Required DatabaseOptional
Server = http://mirror.archlinuxarm.org/$arch/$repo

[core]
SigLevel = Required DatabaseOptional
Server = http://mirror.archlinuxarm.org/$arch/$repo

[extra]
SigLevel = Required DatabaseOptional
Server = http://mirror.archlinuxarm.org/$arch/$repo
EOF

pacman-key --init
wget -O /usr/share/keyrings/archlinuxarm.gpg https://raw.githubusercontent.com/archlinuxarm/archlinuxarm-keyring/master/archlinuxarm.gpg
pacman-key --populate archlinuxarm

pacstrap -K /mnt base base-devel rpi5-eeprom linux-rpi-16k linux-firmware raspberrypi-bootloader firmware-raspberrypi archlinuxarm-keyring btrfs-progs git iwd openssh tmux wget
# https://nj.us.mirror.archlinuxarm.org/aarch64/core/
# https://nj.us.mirror.archlinuxarm.org/aarch64/core/linux-rpi-16k-6.6.18-1-aarch64.pkg.tar.xz

# Add to /etc/fstab
#UUID=8AAB-CC51  /boot  vfat    defaults          0       2

# In /boot/cmdline.txt add quriks if necessary  and also root uuid
#usb-storage.quirks=0bda:9210:u root=UUID=9f074357-4845-4e8b-acc5-dc7729c13247 rw rootwait console=serial0,115200 console=tty1

# Set root passwd
passwd root

# Add keys to /root/.ssh/authorized_keys

systemctl enable sshd iwd systemd-networkd systemd-resolved

cat > /var/lib/iwd/${WIFISSID}.psk << EOF
[Security]
Passphrase=${WIFIPASSWORD}

/etc/systemd/network/25-wireless.network
[Match]
Name=wl*

[Network]
DHCP=yes
IgnoreCarrierLoss=3s
EOF

# In chroot
pacman-key --init
pacman-key --populate archlinuxarm

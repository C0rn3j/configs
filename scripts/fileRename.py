#!/usr/bin/env python
import os
import sys

def main():
	if len(sys.argv) != 3:
		print(f"Usage: {sys.argv[0]} stringToReplace replaceWith")
		sys.exit(1)

	string_to_replace = sys.argv[1]
	new_string = sys.argv[2]

	current_dir = os.getcwd()
	for file in os.listdir(current_dir):
		if os.path.isfile(file):
			new_base = file.replace(string_to_replace, new_string)
			destination = new_base
#			destination = os.path.join(current_dir, new_base)
			if file == destination:
				print(f"No rename needed for '{file}'")
				continue
			if os.path.exists(destination):
				print(f"Tried to move file '{file}' to an already existing path '{destination}'")
				sys.exit(33)
			print(f"Moving file \n'{file}' to path \n'{destination}'")
			os.rename(file, destination)

if __name__ == "__main__":
	main()

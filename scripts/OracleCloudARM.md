Refs:
1. http://os.archlinuxarm.org/os/ArchLinuxARM-aarch64-latest.tar.gz
2. https://dl-cdn.alpinelinux.org/alpine/v3.13/releases/aarch64/alpine-virt-3.13.5-aarch64.iso
3. https://wiki.alpinelinux.org/wiki/Replacing_non-Alpine_Linux_with_Alpine_remotely
4. https://wiki.archlinux.org/index.php/installation_guide#Configure_the_system
5. https://archlinuxarm.org/platforms/armv8/generic
6. https://gist.github.com/zengxinhui/01afb43b8d663a4232a42ee9858be45e

Requirements:
  * Console access.

## In Oracle Linux 8.0/Ubuntu 22.04
```bash
# Switch to root
sudo -i
cd /tmp
wget https://dl-cdn.alpinelinux.org/alpine/v3.13/releases/aarch64/alpine-virt-3.13.5-aarch64.iso
dd if=alpine-virt-3.13.5-aarch64.iso of=/dev/sda
sync
reboot
```
## In Alpine through Oracle Console access, login is 'root':
### [Bring up networking]
```bash
vi /etc/network/interfaces # Add the following two lines:
  auto eth0
  iface eth0 inet dhcp
ifup eth0
```
### [Per Ref #3]
```bash
mkdir /media/setup
cp -a /media/sda/* /media/setup
mkdir /lib/setup
cp -a /.modloop/* /lib/setup
/etc/init.d/modloop stop
umount /dev/sda
mv /media/setup/* /media/sda/
mv /lib/setup/* /.modloop/
```
### [Setup apk and bring in pacman]
```bash
setup-apkrepos
vi /etc/apk/repositories # enable community repository
apk update
apk add dosfstools e2fsprogs libarchive-tools pacman arch-install-scripts
```
### [Disk partitioning & mounting]
```bash
fdisk /dev/sda # (use gpt table (g), set esp partition 15 size 512M (n)(15)()(+512M), set root partition 1 size remaining (n)(1)()(), write (w))
mkfs.vfat /dev/sda15
mkfs.ext4 /dev/sda1
mount /dev/sda1 /mnt
mkdir -p /mnt/boot/efi
mount /dev/sda15 /mnt/boot/efi
cd /mnt
wget http://os.archlinuxarm.org/os/ArchLinuxARM-aarch64-latest.tar.gz
bsdtar -xpf /mnt/ArchLinuxARM-aarch64-latest.tar.gz -C /mnt
genfstab -U /mnt >> /mnt/etc/fstab
arch-chroot /mnt/
pacman-key --init
pacman-key --populate archlinuxarm
```
### [EFI boot]
```bash
pacman -Syu grub efibootmgr --noconfirm
grub-install --efi-directory=/boot/efi --bootloader-id=GRUB
```
### [grub config]
```bash
grub-mkconfig -o /boot/grub/grub.cfg
```
### [workaround: grub of alarm doesn't detect linux]
```bash
echo "linux (hd0,gpt1)/boot/Image.gz root=/dev/sda1" >> /boot/grub/grub.cfg
echo "initrd (hd0,gpt1)/boot/initramfs-linux.img"    >> /boot/grub/grub.cfg
echo boot                                            >> /boot/grub/grub.cfg
```
### [Enable SSH login]
```bash
nano /etc/ssh/sshd_config # change to "PasswordAuthentication no"
su alarm
mkdir ~/.ssh
nano ~/.ssh/authorized_keys # add your pubkey
exit
exit
reboot
```

### Reboot to Arch Linux ARM

After the reboot you should be able to use ssh keyless login to remote into new alarm system to the `alarm` user, afterwards login with `su -` and `root` as a password.

### ISCSI drives
If you have ISCSI block volumes configured, add them after configuring the following:
```bash
pacman -S open-iscsi --noconfirm --needed
nano /etc/iscsi/iscsid.conf # set node.startup = automatic
systemctl enable --now iscsid iscsi
```

#!/bin/bash
set -euo pipefail
# This script exists to convert Scaleway's Ubuntu image into an Arch one.
# You'll have to follow and execute the commands by hand.

# Stolen from https://gist.github.com/m-ou-se/863ad01a0928e184b2b8
# and https://wiki.archlinux.org/index.php/Install_from_existing_Linux

# Make sure you'll have enough entropy for pacman-key later with haveged, and zstd for unpacking the bootstrap
apt-get install -y haveged zstd

# WTF
#umount /boot/efi
rm /etc/apt/apt.conf.d/20snapd.conf
apt-get remove -y lxd snapd apparmor && apt-get autoremove -y

systemctl disable --now unattended-upgrades irqbalance networkd-dispatcher

cd /root
wget 'ftp://ftp.nluug.nl/pub/os/Linux/distr/archlinux/iso/latest/archlinux-bootstrap-*-x86_64.tar.zst'

mkdir /frankenstein
# Do 2g if you have the RAM for it, this was done on a 2GB VPS
mount -t tmpfs -o size=1g tmpfs /frankenstein
cd /frankenstein
tar xvf ~/archlinux-bootstrap-*-x86_64.tar.zst --strip-components=1

# Copy SSH keys and whatnot
cp -r ~/.ssh root

# Uncomment mirrors.
sed -i s/#Server/Server/g etc/pacman.d/mirrorlist
# Swap / and /frankenstein
mount --make-rprivate /
for i in run proc sys dev; do mount --move /$i /frankenstein/$i; done
pivot_root . mnt
exec chroot .

# other chroot method on arch wiki, doesnt seem to work proper
#/frankenstein/bin/arch-chroot /frankenstein/

# Get systemd-resolved working
ln -sf ../run/systemd/resolve/stub-resolv.conf /etc/resolv.conf
#echo "nameserver 1.1.1.1" > /etc/resolv.conf

# Setup pacman and install the tools we need.
pacman-key --init
pacman-key --populate archlinux
pacman -Sy psmisc openssh lsof htop wget iputils nano --noconfirm --needed
# For good measure or if you run into issues you can do full -Syu in case -Sy does break things this time around
#pacman -Syu
# Remove cache, just in case
pacman -Scc


# Restart sshd in the new root.
cp /mnt/etc/ssh/* /etc/ssh
killall -HUP sshd
# You'll have to reconnect at this point. Let's hope it works.

# Make sure nothing is using the old filesystem anymore.
killall atd dhclient haveged agetty cron dbus-daemon polkitd rsyslogd systemd-udevd systemd-logind lvmetad ModemManager udisksd
# kill systemd-logind, systemd-timesyncd, systemd-resolved, systemd-journald, systemd --user (SIGKILL) and finally dbus-daemon
systemctl daemon-reexec


umount /mnt
# If umount complains that /mnt is still busy, install and use
# lsof to check which other processes you need to kill:
lsof +f -- /mnt
#   kill ...
#   umount /mnt

# If there are no processes left and you still get an error, try forcesmashing it, restarting services again, smashing it again
# installing lvm2/mdadm and following https://serverfault.com/a/36047/1075061
partprobe /dev/sdX
blockdev --rereadpt /dev/sdX
hdparm -z /dev/sdX
wipefs -af /dev/sdX

# If you still get an error message when umounting and you ran out of ideas, just force the umount and pray it'll work
# this will VERY LIKELY not work as you'll find out, you can keep trying to kill the thing that's using it later too
umount -l /mnt

# Install Arch as normal
wget https://gitlab.com/C0rn3j/configs/raw/master/scripts/archinstall.sh && chmod +x archinstall.sh && ./archinstall.sh

arch-chroot /mnt

# Scaleway scripts
git clone https://github.com/scaleway/image-tools/
rsync -av image-tools/bases/overlay-common/usr/local/sbin/ /usr/local/sbin/
rm -rf image-tools

# Get Scaleway Console working
systemctl enable serial-getty@ttyS0
# Add the following params to kernel cmdline:
#console=tty1 console=ttyS0

# Finally verify that the UUID of your ESP matches the one in your fstab,
# as genfstab for example will get confused in this setup

# If you want, manually configure network using systemd-networkd instead of relying on network manager, i.e.
systemctl disable NetworkManager
systemctl enable systemd-networkd

nano /etc/systemd/network/Hopium.network

[Match]
Name=en*

[Network]
Address=152.89.168.179/24
Gateway=152.89.168.1

Address=2a05:4140:3::22b8/48
Gateway=2a05:4140:3::1

DNS=1.1.1.1
DNS=8.8.8.8
DNS=2606:4700:4700::1111
DNS=2001:4860:4860::8888

# Finally sync for good luck and reboot
sync
reboot

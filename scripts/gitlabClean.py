#!/usr/bin/env python
import requests

PRIVATE_TOKEN = '1234'
GITLAB_API_URL = 'https://gitlab.com/api/v4'
PROJECT_PATH = 'Arch/Linux'

def get_project_id(project_path):
    """Retrieve a project's ID using its path."""
    url = f"{GITLAB_API_URL}/projects/{requests.utils.quote(project_path, safe='')}"
    headers = {'Authorization': f'Bearer {PRIVATE_TOKEN}'}
    response = requests.get(url, headers=headers)
    if response.status_code == 200:
        return response.json().get('id')
    else:
        print(f"Failed to fetch project ID for {project_path}")
        return None

def get_parent_pipelines(project_id, page=1):
    """Fetch all parent pipelines for a given project with pagination."""
    pipelines = []
    while True:
        # https://docs.gitlab.com/ee/api/pipelines.html#list-project-pipelines
        pipelines_url = f"{GITLAB_API_URL}/projects/{project_id}/pipelines?per_page=100&page={page}"
        headers = {'Authorization': f'Bearer {PRIVATE_TOKEN}'}
        response = requests.get(pipelines_url, headers=headers)
#     print(response.json())
# {'id': 185841,
# 'iid': 2760,
# 'project_id': 879,
# 'sha': '546f4fbf44e130fbc5a6822b762aeff0e9093d3e',
# 'ref': 'master',
# 'status': 'success',
# 'source': 'push',
# 'created_at': '2023-08-06T00:27:47.867+02:00',
# 'updated_at': '2023-08-06T00:28:59.657+02:00',
# 'web_url': 'https://gitlab.com/Arch/Linux/-/pipelines/185841',
# 'name': None}
        if response.status_code == 200:
            page_pipelines = response.json()
            if not page_pipelines:
                break  # No more pipelines, exit loop
            pipelines.extend(page_pipelines)
            page += 1  # Increment page number for next iteration
        else:
            print("Failed to fetch pipelines")
            break
    print(response.json())
    return pipelines

def delete_jobs(project_id, pipeline_id):
    """Delete all jobs associated with a given pipeline."""
    jobs_url = f"{GITLAB_API_URL}/projects/{project_id}/pipelines/{pipeline_id}/jobs"
    headers = {'Authorization': f'Bearer {PRIVATE_TOKEN}'}
    response = requests.get(jobs_url, headers=headers)
    if response.status_code == 200:
        jobs = response.json()
        for job in jobs:
            delete_job_url = f"{GITLAB_API_URL}/projects/{project_id}/jobs/{job['id']}"
            delete_response = requests.delete(delete_job_url, headers=headers)
            if delete_response.status_code == 204:
                print(f"Deleted job {job['web_url']} from pipeline {pipeline_id}")
                return True
            else:
                print(f"Failed to delete job {job['web_url']} from pipeline {pipeline_id} Response: {delete_response.json()} {job}")
                exit(1)
    else:
        print(f"Failed to fetch jobs for pipeline {pipeline_id}")
        return False

def delete_pipeline(project_id, pipeline):
    """Delete a specific pipeline by ID, does not delete its child pipelines!"""
    # delete_jobs(project_id, pipeline['id'])  # Delete associated jobs first
    pipeline_id = pipeline['id']
    delete_url = f"{GITLAB_API_URL}/projects/{project_id}/pipelines/{pipeline_id}"
    headers = {'Authorization': f'Bearer {PRIVATE_TOKEN}'}
    response = requests.delete(delete_url, headers=headers)
    if response.status_code == 204:
        print(f"Deleted pipeline[{pipeline['created_at'][:10]}|{pipeline['ref']}|{pipeline['status'][:4]}] {pipeline['web_url']}")
        return True
    else:
        print(f"Failed to delete pipeline[{pipeline['created_at'][:10]}|{pipeline['ref']}|{pipeline['status'][:4]}] {pipeline['web_url']}")
        return False

def delete_all_jobs(project_id):
    """Delete all jobs for a given project."""
    print(f"Deleting all jobs for {project_id}")
    page = 1
    deleted_pipelines = []
    while True:
        # https://docs.gitlab.com/ee/api/jobs.html#list-pipeline-jobs
        jobs_url = f"{GITLAB_API_URL}/projects/{project_id}/jobs?per_page=100&page={page}"
        headers = {'Authorization': f'Bearer {PRIVATE_TOKEN}'}
        response = requests.get(jobs_url, headers=headers)
        if response.status_code == 200:
            jobs = response.json()
            if not jobs:
                break  # No more jobs, exit loop
            for job in jobs:
                if job['pipeline']['id'] in deleted_pipelines:
                    continue
                if job['pipeline']['id'] in deleted_pipelines:
                    continue
                delete_job_url = f"{GITLAB_API_URL}/projects/{project_id}/jobs/{job['id']}/erase"
                delete_response = requests.post(delete_job_url, headers=headers)
                if delete_response.status_code in [200, 204]:
                    print(f"Deleted job {job['id']}")
                else:
                    if job['pipeline']['source'] == 'parent_pipeline' and job['pipeline']['project_id'] == project_id:
                        if delete_pipeline(project_id, job['pipeline']):
                            deleted_pipelines.append(job['pipeline']['id'])
                            print(f"Deleted job {job['id']} via means of deleting its (child) pipeline")
                    else:
                        print(f"Failed to delete job {job['web_url']} {delete_response.json()}\n{job}")
                        exit(1)
            page += 1  # Increment page number for next iteration
        else:
            print("Failed to fetch jobs")
            break

def main():
    project_id = get_project_id(PROJECT_PATH)
    if project_id:
        pipelines = get_parent_pipelines(project_id)
        if pipelines:
            print(f"Found {len(pipelines)} pipelines. Deleting...")
            for pipeline in pipelines:
                delete_pipeline(project_id, pipeline)
        else:
            print("No pipelines found or failed to fetch pipelines.")
        delete_all_jobs(project_id)
    else:
        print("Could not retrieve project ID.")

if __name__ == "__main__":
    main()

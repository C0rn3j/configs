This repo contains my Ansible playbooks, roles and also networking setup for my domain on the side.

This is sort of my post-install script that's meant to run after https://gitlab.com/C0rn3j/arch/blob/master/install.sh

If you intend to use it, make sure to install the dependencies:

```bash
ansible-galaxy collection install -r playbooks/requirements.yaml
ansible-galaxy role install -r playbooks/requirements.yaml --force
# Yes, --force is necessary.
#   See https://docs.ansible.com/ansible/latest/network/getting_started/network_roles.html#update-an-installed-role
#   and https://github.com/ansible/proposals/issues/23
```

There are some symlinks that were necessary to get this running in AWX.

`playbooks/site.yaml` is the main Ansible playbook, it runs roles from `roles/*`, each role's main file is in `roles/*/main.yaml`.

`ansible-lint` is employed to keep things neat.

All rights reserved unless otherwise mentioned. I am open to relicensing parts to AGPLv3 if necessary, feel free to contact me about it.

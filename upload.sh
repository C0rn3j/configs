#!/usr/bin/env bash
set -euo pipefail

host="ansible.rys.rs"
port="10250"

echo "Syncing playbooks, unit files and hosts"
rsync -avzz -e "ssh -p ${port}" ansible.cfg hosts root@${host}:/etc/ansible/
rsync -avzz -e "ssh -p ${port}" --delete playbooks collections roles root@${host}:/root/
rsync -avzz -e "ssh -p ${port}" ./unit_files/* root@${host}:/etc/systemd/system/

echo "Reloading systemd and enabling/starting unit files"
ssh -p ${port} root@${host} "systemctl daemon-reload && systemctl enable --now update.timer"

echo "Updating Ansible Galaxy collections and roles"
ssh -p ${port} root@${host} "ansible-galaxy collection install -r /root/playbooks/requirements.yaml --upgrade && ansible-galaxy role install -r /root/playbooks/requirements.yaml --force"

echo "All OK!"

#!/usr/bin/env php
<?php
// https://api.cloudflare.com
class DNSRecord {
	public $id = "";        // [id] Cloudflare ID
	public $name = "";      // [name] => paste
	public $type = "A";     // [type] => A
	public $content = "";   // [content] => 123.123.123.123
	public $priority = 0;   // [priority] => 0
	public $ttl = 300;      // [ttl] => 300
}

function CFRequest($url, $method, $data = NULL) {
	global $bearerToken;
	$cURL = curl_init();
	curl_setopt($cURL, CURLOPT_URL, $url);
	curl_setopt($cURL, CURLOPT_CUSTOMREQUEST, $method);
	curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
	if($method == 'POST') {
		$payload = json_encode($data);
		curl_setopt($cURL, CURLOPT_POSTFIELDS, $payload);
		curl_setopt($cURL, CURLOPT_HTTPHEADER, array(
			"Authorization: Bearer $bearerToken",
			'Content-Type: application/json',
			'Content-Length: ' . strlen($payload)
		));
	} else {
		curl_setopt($cURL, CURLOPT_HTTPHEADER, array(
			"Authorization: Bearer $bearerToken",
			'Content-Type: application/json'
		));
	}
	$result = curl_exec($cURL);
	curl_close($cURL);
	return json_decode($result, true);
}

function loadCloudflare(&$CFrecordArray, $zoneID, &$currentPage) {
	$json = CFRequest("https://api.cloudflare.com/client/v4/zones/$zoneID/dns_records?per_page=100&page=$currentPage", 'GET', null);
	if (! isset($json["result"])) {
		echo("Something went wrong with processing $zoneID");
		print_r($json);
		exit(1);
	}
	$records = $json["result"];
	foreach($records as $key=>$value) {
		$newRecord = new DNSRecord();
		foreach($records[$key] as $keyname =>$value){
			$newRecord->$keyname = $value;
		}
		array_push($CFrecordArray, $newRecord);
	}
	if($json["result_info"]["page"] < $json["result_info"]["total_pages"]) {
		$currentPage+=1;
		echo("Loading next page($currentPage)...".PHP_EOL);
		loadCloudflare($CFrecordArray, $zoneID, $currentPage);
	}
}

function loadCSV(&$CSVrecordArray) {
	global $zoneName;
	$row = 1;
	if (($handle = fopen("$zoneName.csv", "r")) !== FALSE) {
		while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
			if($row == 1) {
				$row++;
				continue;
			}
			$newRecord = new DNSRecord();
			$num = count($data);
			$row++;
			for ($c=0; $c < $num; $c++) {
				if($c==0) {
					if($data[$c] == "@") {
						$newRecord->name = $zoneName;
					} else {
						$newRecord->name = $data[$c].'.'.$zoneName;
					}
				}
				if($c==1) { $newRecord->type = $data[$c]; }
				if($c==2) { $newRecord->content = $data[$c]; }
				if($c==3) { $newRecord->ttl = (int)$data[$c]; }
				if($c==4) { $newRecord->priority = (int)$data[$c]; }
			}
			array_push($CSVrecordArray, $newRecord);
		}
		fclose($handle);
	}
}

function deleteFromCF($record, $zoneID) {
	echo("Deleting ".$record->name."[".$record->type."][".$record->ttl."][".$record->content."]".PHP_EOL);
	$json = CFRequest("https://api.cloudflare.com/client/v4/zones/$zoneID/dns_records/$record->id", 'DELETE', null);
	if (! isset($json["result"])) {
		echo("Something went wrong with deleting a record in $zoneID");
		var_dump($record);
		print_r($json);
		exit(1);
	}
}

function addToCF($record, $zoneID) {
	echo("Adding ".$record->name."[".$record->type."][".$record->ttl."][".$record->content."]".PHP_EOL);
	$data = array(
		'type' => $record->type,
		'name' => $record->name,
		'content' => $record->content,
		'ttl' => (int)$record->ttl,
		'priority' => (int)$record->priority
	);
	$json = CFRequest("https://api.cloudflare.com/client/v4/zones/$zoneID/dns_records", 'POST', $data);
	if (! isset($json["result"])) {
		echo("Something went wrong with adding a record in {$zoneID}");
		var_dump($record);
		print_r($json);
		exit(1);
	}
}

// DNS records File is always $zoneName.csv
$zones = [
	[
		'zoneID' => '8c5339c75cb56a0fc70ffa2c9baca26d',
		'zoneName' => 'rys.pw',
		'accountToken' => 'cloudflare_token'
	],
	[
		'zoneID' => '62931ce2ea378e5f654f9db36edeb5a0',
		'zoneName' => 'rys.rs',
		'accountToken' => 'cloudflare_token'
	]
];

foreach ($zones as $zone) {
	$zoneID = $zone['zoneID'];
	$zoneName = $zone['zoneName'];
	$accountToken = $zone['accountToken'];
	// Load bearer token from a secret file
	$stream = fopen($accountToken,"r");
	$bearerToken = rtrim(stream_get_contents($stream));
	fclose($stream);

	$currentPage = 1;
	$CFrecordArray = array();
	$CSVrecordArray = array();

	loadCloudflare($CFrecordArray, $zoneID, $currentPage);
	loadCSV($CSVrecordArray);

	// Check if records exist on CF that aren't present in the CSV - if it does not exist or it differs, delete it
	foreach ($CFrecordArray as $CFrecord) {
		foreach ($CSVrecordArray as $CSVrecord) {
			if ($CFrecord->name == $CSVrecord->name && $CFrecord->type == $CSVrecord->type && $CFrecord->content == $CSVrecord->content && $CFrecord->ttl == $CSVrecord->ttl) {
				// Name and type matches, record exists in CSV so just jump to the next CF record
				continue 2;
			}
		}
		deleteFromCF($CFrecord, $zoneID);
	}
	foreach ($CSVrecordArray as $CSVrecord) {
		foreach ($CFrecordArray as $CFrecord) {
			if ($CFrecord->name == $CSVrecord->name && $CFrecord->type == $CSVrecord->type && $CFrecord->content == $CSVrecord->content && $CFrecord->ttl == $CSVrecord->ttl) {
				// Name and type matches, record exists in CF so just jump to the next CSV record
				continue 2;
			}
		}
		addToCF($CSVrecord, $zoneID);
	}

	//var_dump($CSVrecordArray);
	//var_dump($CFrecordArray);
}
?>

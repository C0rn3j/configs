#!rsc
# This script assumes that network is 192.168.1.0/24

#/ip firewall nat remove [find comment="C0rn3j"]
# Remove all rules that have 'C0rn3j' in them
:foreach i in [/ip firewall nat find] do={
	:if ("." . [:find [/ip firewall nat get $i comment] "C0rn3j"] . "."!="..") do={
		/ip firewall nat remove $i
	}
}

# Add a masquerading rule so forwarding works in the first place
/ip firewall nat add chain=srcnat action=masquerade out-interface=wlan1 log=no log-prefix="" comment="masquerade rule C0rn3j"

:global portForward do={
	# $name
	# $port
	# $ip
	# $globalip
	/ip firewall nat add chain=dstnat action=dst-nat to-addresses=$ip protocol=tcp dst-address=$globalip dst-port=$port log=no log-prefix="" comment="$name C0rn3j"
	/ip firewall nat add chain=dstnat action=dst-nat to-addresses=$ip protocol=udp dst-address=$globalip dst-port=$port log=no log-prefix="" comment="$name C0rn3j"
	/ip firewall nat add chain=srcnat action=masquerade protocol=tcp src-address=192.168.1.0/24 dst-address=$ip out-interface=ether1 dst-port=$port log=no log-prefix="" comment="$name C0rn3j"
	/ip firewall nat add chain=srcnat action=masquerade protocol=udp src-address=192.168.1.0/24 dst-address=$ip out-interface=ether1 dst-port=$port log=no log-prefix="" comment="$name C0rn3j"
}

# Proxy
$portForward ip="192.168.1.20" globalip="89.248.245.158" port="80" name="HTTP"
$portForward ip="192.168.1.20" globalip="89.248.245.158" port="443" name="HTTPS"
$portForward ip="192.168.1.20" globalip="89.248.245.158" port="51820" name="Wireguard"

# Games
$portForward ip="192.168.1.29" globalip="89.248.245.158" port="25565" name="Minecraft"
$portForward ip="192.168.1.29" globalip="89.248.245.158" port="25565" name="Minecraft"

# Games Luxuria
$portForward ip="192.168.1.10" globalip="89.248.245.158" port="34197" name="Factorio"
$portForward ip="192.168.1.10" globalip="89.248.245.158" port="20100" name="Torrent"
$portForward ip="192.168.1.10" globalip="89.248.245.158" port="15000" name="Satisfactory"
$portForward ip="192.168.1.10" globalip="89.248.245.158" port="15777" name="Satisfactory"
# 7777 also needed by Satisfactory
$portForward ip="192.168.1.10" globalip="89.248.245.158" port="7777-7780" name="Terraria"
$portForward ip="192.168.1.10" globalip="89.248.245.158" port="47630" name="BaldursGate"
$portForward ip="192.168.1.10" globalip="89.248.245.158" port="2300-2400" name="BaldursGate"
$portForward ip="192.168.1.10" globalip="89.248.245.158" port="6073" name="BaldursGate"
$portForward ip="192.168.1.10" globalip="89.248.245.158" port="14242" name="PommelParty"
$portForward ip="192.168.1.10" globalip="89.248.245.158" port="14300" name="PommelParty"
$portForward ip="192.168.1.10" globalip="89.248.245.158" port="27015" name="CSGO"
$portForward ip="192.168.1.10" globalip="89.248.245.158" port="24642" name="StardewValley"
# Asterisk
$portForward ip="192.168.1.30" globalip="89.248.245.158" port="5061" name="Asterisk"

# Port forward 10 ports based on fourth octet of an internal IP *10 +10000
# So 192.168.1.20 gets 10200-10209, 192.168.1.231 gets 12310-12319 etc
# Don't need more than 99 at the moment
:for i from=1 to=99 step=1 do={
	:local portsFrom; :set portsFrom (10000+$i*10)
	:local portsTo; :set portsTo ($portsFrom+9)
	$portForward ip=("192.168.1.".$i) globalip="89.248.245.158" port=($portsFrom."-".$portsTo) name=$i
}

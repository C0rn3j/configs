#!/bin/sh
portForward() {
	# $1 = IP
	# $2 = source port(s)
	# $3 = destination port(s)
	# $4 = name
	echo "Adding FW rule ${i}"
	uci set firewall.custom_rule_$(printf %03d $((i++)))=redirect
	uci set firewall.custom_rule_$(printf %03d $((i++))).target="DNAT"
	uci set firewall.custom_rule_$(printf %03d $((i++))).src="wan"
	uci set firewall.custom_rule_$(printf %03d $((i++))).dest="lan"
	uci set firewall.custom_rule_$(printf %03d $((i++))).proto="tcp udp"
	uci set firewall.custom_rule_$(printf %03d $((i++))).src_dport="${2}"
	uci set firewall.custom_rule_$(printf %03d $((i++))).dest_ip="${1}"
	uci set firewall.custom_rule_$(printf %03d $((i++))).dest_port="${3}"
	uci set firewall.custom_rule_$(printf %03d $((i++))).name="${4}"
	i=$(($i+1))
}
staticIP() {
	# $1 = IP
	# $2 = MAC
	# $3 = name
	echo "Adding Static IP rule ${i}"
	uci set dhcp.custom_rule_$(printf %03d $((i++)))="host"
	uci set dhcp.custom_rule_$(printf %03d $((i++))).name="${3}"
	uci set dhcp.custom_rule_$(printf %03d $((i++))).dns="1"
	uci set dhcp.custom_rule_$(printf %03d $((i++))).mac="${2}"
	uci set dhcp.custom_rule_$(printf %03d $((i++))).ip="${1}"
	i=$(($i+1))
}
clearCustomPortForwards() {
	i=1
	while uci -q delete firewall.custom_rule_$(printf %03d $((i++))); do
		echo "Removing FW rule ${i}"
		i=$(($i+1))
	done
	echo "Done deleting old entries"
}
clearCustomStaticIPs() {
	i=1
	while uci -q delete dhcp.custom_rule_$(printf %03d $((i++))); do
		echo "Removing Static IP rule ${i}"
		i=$(($i+1))
	done
	echo "Done deleting old entries"
}
setCustomPortForwards() {
	i=1
	# Port forward 10 ports based on fourth octet of an internal IP *10 +10000
	# So 192.168.1.20 gets 10200-10209, 192.168.1.231 gets 12310-12319 etc
	while [ ${i} != 100 ]; do # Don't need more than 99 at the moment
		portsFrom=$((10000+$i*10))
		portsTo=$(($portsFrom+9))
		portForward "192.168.1.${i}" "${portsFrom}-${portsTo}" "${portsFrom}-${portsTo}" ${i}
	done
	# Proxy
	portForward "192.168.1.20" "80" "80" "HTTP"
	portForward "192.168.1.20" "443" "443" "HTTPS"
	portForward "192.168.1.20" "51820" "51820" "Wireguard"

	# Games
	portForward "192.168.1.29" "25565" "25565" "Minecraft"

	# Games Luxuria
	portForward "192.168.1.10" "34197" "34197" "Factorio"
	portForward "192.168.1.10" "20100" "20100" "Torrent"
	portForward "192.168.1.10" "15000" "15000" "Satisfactory"
	portForward "192.168.1.10" "15777" "15777" "Satisfactory"

	# 7777 also needed by Satisfactory
	portForward "192.168.1.10" "7777-7780" "7777-7780" "Terraria"
	portForward "192.168.1.10" "47630" "47630" "BaldursGate"
	portForward "192.168.1.10" "2300-2400" "2300-2400" "BaldursGate"
	portForward "192.168.1.10" "6073" "6073" "BaldursGate"
	portForward "192.168.1.10" "14242" "14242" "PommelParty"
	portForward "192.168.1.10" "14300" "14300" "PommelParty"
	portForward "192.168.1.10" "27015" "27015" "CSGO"
	portForward "192.168.1.10" "24642" "24642" "Stardew Valley"
	# Asterisk
	portForward "192.168.1.30" "5061" "5061" "Asterisk"
}
setCustomStaticIPs() {
	if [ ! -e ./DHCP.csv ]; then
		echo "Error: ./DHCP.csv does not exist! Use 'libreoffice --convert-to csv DHCP.ods' to create it!"
		exit 1
	fi
	i=1
	awk -F "," '{print $2,$4,$5}' DHCP.csv | grep ":" | grep "\." > /tmp/dhcp.txt
	file="/tmp/dhcp.txt"
	while IFS=" " read -r f1 f2 f3; do
		# IP, MAC, Name
		staticIP ${f1} ${f2} ${f3}
	done <"$file"
}
saveConfig() {
	echo "Changes:"
	uci changes
	echo "Committing and reloading config"
	uci commit
	reload_config
}
if [ "${1}" != "portForward" ] && [ "${1}" != "DHCP" ]; then
	echo "Use with either 'portForward' or 'DHCP' parameter!"
	exit 1
fi
if [ "${1}" = "portForward" ]; then
	clearCustomPortForwards
	setCustomPortForwards
	saveConfig
fi
if [ "${1}" = "DHCP" ]; then
	clearCustomStaticIPs
	setCustomStaticIPs
	saveConfig
fi

# Revert changes:
#uci revert [subsystem]
# Read system log (stored in RAM):
#logread
# Show current config of a subsystem:
#uci show firewall
